<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	protected $js_prefix = 'js_';
	protected $js_suffix = '';
	public $_user_id;
	public $_role_id;
	public $_now;
	public $_now_timestamp;
	public $_branch_code;

	public function __construct($securePage=false)
	{
		parent::__construct();
		
		// error reporting
		// error_reporting(-1);
		ini_set('display_errors', 'On');
		error_reporting(E_ALL);
		ini_set('display_errors', 1);


		$this->load->model('model_get');

		$this->output->cache(0);

		date_default_timezone_set('Asia/Jakarta');

		// set global variable
		$this->_user_id = $this->session->userdata('user_id');
		$this->_role_id = $this->session->userdata('role_id');
		$this->_now = date("Y-m-d");
		$this->_now_timestamp = date("Y-m-d H:i:s");
		$this->_branch_code = $this->session->userdata('branch_code');

		if($securePage==true) {
			if($this->session->userdata('is_logged_in')==false) {
				redirect('login');
			}
		} else {
			if($this->session->userdata('is_logged_in')==true) {
				redirect('home');
			}	
		}

		// $token = $this->session->userdata('token');
		// if (!$token) {
		// 	redirect('login','refresh');
		// }

		$url = $this->uri->segment(1); // get url by segment 1
		if($url==false) {
			redirect('login');
		}

		// $url = strtoupper($this->uri->segment(1).$this->uri->segment(2));
		// if ($url!="") {
		// 	$menu_is_exists = $this->model_get->cek_menu_is_exists($url);
		// 	if ($menu_is_exists==true) {
		// 		$menu_id = $this->model_get->get_menu_id_by_url($url);
		// 		$url_is_allowed = $this->model_get->cek_url_is_allowed($menu_id,$this->_role_id);
		// 		if ($menu_id!='210') { // if not dashboard
		// 			if ($url_is_allowed==false) {
		// 				show_404();
		// 				// redirect('notfound');
		// 			}
		// 		}
		// 	}
		// }

		// $role_id = $this->session->userdata('role_id');
		// $this->generate_menu($role_id);


        // foreach($_GET as $key => $val):
        // 	$this->js_suffix = '_'.$key;
        // endforeach;

		// /*pagescript*/
		// $urisegmen = $this->uri->segment(2);
		// if($urisegmen != ''){	       
    	// 	$dirmap = get_dir_file_info('./assets/_js/');
    	// 	if(isset($dirmap[$this->js_prefix.$urisegmen.$this->js_suffix.".js"]['server_path'])){
		// 		$js_template_name = $dirmap[$this->js_prefix.$urisegmen.$this->js_suffix.".js"]['name'];
		// 		$pagescript = base_url('assets/_js/'.$js_template_name);
        //     	$this->load->vars('pagescript',$pagescript);
    	// 	}
        // }

        // $this->breadcrumb();
	}

	public function get_usia($birthdate,$now)
	{
		$date = $birthdate;
		$data = $this->model_get->getUsiaAsuransi($date);
		return $data['usia'];
	}

	function generate_menu($role_id)
	{
		$this->load->model('model_core');
		$html = '';
		$menu = $this->model_core->get_menu($role_id,0);

		/*
		| BEGIN MENU PARENTS
		*/
		for ( $i = 0 ; $i < count($menu) ; $i++ ) {
			
			$childmenu = $this->model_core->get_menu($role_id,$menu[$i]['menu_id']);

			if ($menu[$i]['menu_site_url']==$this->uri->segment(1)) {
				$html .= '<li class="active">';
			} else {
				$html .= '<li>';
			}
			
			if($menu[$i]['menu_site_url']=='home' || $menu[$i]['menu_site_url']=='dashboard'){
				$html .= '<a href="'.site_url($menu[$i]['menu_site_url']).'">';
			}else{
				if (count($childmenu) > 0) {
					$html .= '<a href="'.site_url($menu[$i]['menu_site_url']).'" class="has-dropdown">';
				}else{
					$html .= '<a href="'.site_url($menu[$i]['menu_site_url']).'">';
				}
			}

			if($menu[$i]['menu_site_url']=='dashboard'){
				$menuIcon = '<i class="ion ion-speedometer"></i>';
			}else{
				$menuIcon = '<i class="fa fa-'.$menu[$i]['menu_icon'].'"></i>';
			}

			if ($menu[$i]['menu_icon']!="") { $html .= $menuIcon; }
			$html .= '&nbsp;&nbsp;<span class="title">'.$menu[$i]['menu_title'].'</span>';
			
			if ($menu[$i]['menu_site_url']==$this->uri->segment(1)) {
				$html .= '<span class="has-dropdown"></span>';
			}

			if (count($childmenu) > 0) {
				$html .= '<span class="has-dropdown"></span>';
			}

			$html .= '</a>';

			/*
			| BEGIN CHILD MENU
			*/
			if (count($childmenu) > 0) $html .= '<ul class="menu-dropdown" style="display: none;">';

			for ( $j = 0 ; $j < count($childmenu) ; $j++ ) {
			
				$grandchildmenu = $this->model_core->get_menu($role_id,$childmenu[$j]['menu_id']);

				if ($childmenu[$j]['menu_site_url']==$this->uri->segment(1).'/'.$this->uri->segment(2)) {
					$html .= '<li class="active">';
				} else {
					$html .= '<li>';
				}
				if (count($grandchildmenu) > 0) {
					$html .= '<a href="'.site_url($childmenu[$j]['menu_site_url']).'" class="has-dropdown">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}else{
					$html .= '<a href="'.site_url($childmenu[$j]['menu_site_url']).'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				if ($childmenu[$j]['menu_icon']!="") { $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; }
				// if ($childmenu[$j]['menu_icon']!="") { $html .= '<i class="fa-'.$childmenu[$j]['menu_icon'].'"></i>'; }
				
				if (count($grandchildmenu) > 0) {
					$html .= '<span class="title">'.$childmenu[$j]['menu_title'].'</span>';
				} else {
					$html .= $childmenu[$j]['menu_title'];
				}
					
				if ($childmenu[$j]['menu_site_url']==$this->uri->segment(1).'/'.$this->uri->segment(2)) {
					$html .= '<span class="selected"></span>';
				}

				if (count($grandchildmenu) > 0) {
					$html .= '<span class="arrow "></span>';
				}
				$html .= '</a>';

				/*
				| BEGIN GRAND CHILD MENU
				*/
				if (count($grandchildmenu) > 0) $html .= '<ul class="menu-dropdown">';

				for ($k = 0 ; $k < count($grandchildmenu) ; $k++ ) {
					
					if ($grandchildmenu[$k]['menu_site_url']==$this->uri->segment(1).'/'.$this->uri->segment(2)) {
						$html .= '<li class="start active open">';
					} else {
						$html .= '<li>';
					}
					$html .= '<a href="'.site_url($grandchildmenu[$k]['menu_site_url']).'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					if ($grandchildmenu[$k]['menu_icon']!="") { $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; }
					// if ($grandchildmenu[$k]['menu_icon']!="") { $html .= '<i class="fa-'.$grandchildmenu[$k]['menu_icon'].'"></i>'; }
					$html .= $grandchildmenu[$k]['menu_title'];
					if ($grandchildmenu[$k]['menu_site_url']==$this->uri->segment(1).'/'.$this->uri->segment(2)) {
						$html .= '<span class="selected"></span>';
					}
					$html .= '</a>';
					$html .= '</li>';

				}

				if (count($grandchildmenu) > 0) $html .= '</ul>';
				/*
				| END GRAND CHILD MENU
				*/

				$html .= '</li>';
			}

			if (count($childmenu) > 0) $html .= '</ul>';
			/*
			| END CHILD MENU
			*/

			$html .= '</li>';
		}

		/*
		| END MENU PARENTS
		*/

		$data['menu'] = $html;

		$this->load->vars($data);
	}
	

	public function convert_numeric($value)
	{
		$value = str_replace('.', '', $value);
		$result = str_replace(',', '.', $value);

		return $result;
	}
	
	
	public function convert_date($date='',$month_length='long',$lang='en_to_id')
	{
		if ( $date == '' )
			$date = date('Y-m-d');
	
		$e = explode ( '-' , $date );
	
		if ( $lang == 'en_to_id' )
		{
			if ( $month_length == 'short' )
			{
				$month = array('','Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nop','Des');
			}
			else
			{
				$month = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember');
			}
	
			return $e[2] . '-' . $month [ (int) $e[1] ] . '-' . $e[0];
		}
	
		else if ( $lang == 'id_to_en' )
		{
			if ( $month_length == 'short' )
			{
				$month = array('','Jan','Feb','Mar','Apr','Mei','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
			}
			else
			{
				$month = array('','January','February','March','April','Mei','June','July','August','September','October','November','December');
			}
	
			return $e[0] . '-' . $month [ (int) $e[1] ] . '-' . $e[2];
		}
	
		else 
		{
			return $date;
		}
	}

	public function current_date()
	{
		$now = date("d/m/Y");
		return $now;
	}

	public function datepicker_convert($has_separator=false,$datepicker,$separator='/')
	{
		if(trim($datepicker)==''){
			return '';
		}
		if($has_separator==true){
			$datepicker = str_replace($separator, '', $datepicker);
		}
        $date = substr($datepicker,4,4).'-'.substr($datepicker,2,2).'-'.substr($datepicker,0,2);

        return $date;
	}

	function konversi($x)
	{
	  	$x = abs($x);
	  	$angka = array ("","satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	  	$temp = "";
	   
	  	if($x < 12){
	   		$temp = " ".$angka[$x];
	  	}else if($x<20){
	   		$temp = $this->konversi($x - 10)." belas";
	  	}else if ($x<100){
	   		$temp = $this->konversi($x/10)." puluh". $this->konversi($x%10);
	  	}else if($x<200){
	   		$temp = " seratus".$this->konversi($x-100);
	  	}else if($x<1000){
	   		$temp = $this->konversi($x/100)." ratus".$this->konversi($x%100);   
	  	}else if($x<2000){
	   		$temp = " seribu".$this->konversi($x-1000);
	  	}else if($x<1000000){
	   		$temp = $this->konversi($x/1000)." ribu".$this->konversi($x%1000);   
	  	}else if($x<1000000000){
	   		$temp = $this->konversi($x/1000000)." juta".$this->konversi($x%1000000);
	  	}else if($x<1000000000000){
	   		$temp = $this->konversi($x/1000000000)." milyar".$this->konversi($x%1000000000);
	  	}
	   
	  	return $temp;
 	}
   
 	function tkoma($x)
 	{

  		$str = stristr($x,",");
  		$ex = explode(',',$x);
  		if (isset($ex[1])) {
	   	
		  	if(($ex[1]/10) >= 1){
		   		$a = abs($ex[1]);
		  	}
		  	
		  	$string = array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan",   "sembilan","sepuluh", "sebelas");
		  	$temp = "";
	  
	  		$a2 = $ex[1]/10;
	  		$pjg = strlen($str);
	  		$i =1;
	     
	   
	  		if($a>=1 && $a< 12){   
	   			$temp .= " ".$string[$a];
	  		}else if($a>12 && $a < 20){   
	   			$temp .= $this->konversi($a - 10)." belas";
	  		}else if ($a>20 && $a<100){   
	   			$temp .= $this->konversi($a / 10)." puluh". $this->konversi($a % 10);
	  		}else{
	   			if($a2<1){
			    	while ($i<$pjg){     
			    		$char = substr($str,$i,1);     
	     				$i++;
	     				$temp .= " ".$string[$char];
	    			}
	   			}
	  		}  
	  	} else {
	  		$temp = '';
	  	}
  		return $temp;
 	}
  
 	function terbilang($x=0)
 	{
		if($x<0){
		  $poin = trim($this->tkoma($x));
			 $hasil = "minus ".trim($this->konversi($x));
		}else{
			 $poin = trim($this->tkoma($x));
			 $hasil = trim($this->konversi($x));
		}
 
	  if($poin){
			 $hasil = $hasil." koma ".$poin;
		}else{
			 $hasil = $hasil;
		}
		
		return $hasil;  
	}

	public function format_date_detail($tanggal,$lang='id',$description=false,$separator='/')
	{
		
		if($tanggal!="0000-00-00" || $tanggal!="" || $tanggal!=NULL)
		{
			$exp = explode('-',$tanggal);
			$year = $exp[0];
			$month = $exp[1];
			$date = $exp[2];
	
			if($description==true)
			{
				$month = $this->get_month_description($month,$lang);
			}
	
			if($lang=='id' || $lang=='en' || $lang=='iden')
			{
				if($lang=="id")
				{
					$return = $date.$separator.$month.$separator.$year;
				}
				else if($lang=="en")
				{
					$return = $year.$separator.$month.$separator.$date;
				}
				else if($lang=="iden")
				{
					$return = ((int)$date).$separator.$month.$separator.$year;
				}
			}
			else
			{
				die("Bahasa pada bulan tidak ditemukan. lang:$lang <strong>function:format_date_detail()</strong>");
			}
		}
		else
		{
			$return = '';
		}
		return $return;
	}

	function IntervalDays($CheckIn,$CheckOut){
		$CheckInX = explode("/", $CheckIn);
		$CheckOutX =  explode("/", $CheckOut);
		$date1 =  mktime(0, 0, 0, $CheckInX[1],$CheckInX[2],$CheckInX[0]);
		$date2 =  mktime(0, 0, 0, $CheckOutX[1],$CheckOutX[2],$CheckOutX[0]);
		$interval =($date2 - $date1)/(3600*24);
		// returns numberofdays
		return  $interval ;
	}

	public function RupiahToNumeric($value) {
		$return = str_replace('.','',$value);
		$return = str_replace(',','.',$return);
		return $return;
	}

	public function DatepickerToEn($datepicker) {
		if($datepicker!=''){
			$datepicker = str_replace('/', '', $datepicker);
	        $date = substr($datepicker,4,4).'-'.substr($datepicker,2,2).'-'.substr($datepicker,0,2);
	        return $date;
	    }
	}

	public function DatepickerToIna($datepicker) {
		$datepicker = str_replace('-', '', $datepicker);
        $date = substr($datepicker,6,2).'-'.substr($datepicker,4,2).'-'.substr($datepicker,0,4);
        return $date;
	}

	public function ExtractPeriodDate($from_date,$thru_date,$type) {

		$from_date = new DateTime($from_date);
		$thru_date =new DateTime($thru_date);
		$interval = $thru_date->diff($from_date);

		switch ($type) {
			case 'FullYear':
			$time = $interval->format('%y');
			break;
			case 'FullDay':
			$time = $interval->format('%a');
			break;
			case 'MonthOfLastYear':
			$time = $interval->format('%m');
			break;
			case 'FullMonth':
			$time = $interval->format('%m') + ($interval->format('%y') * 12);
			break;
			case 'DayOfLastYear':
			$time = $interval->format('%d');
			break;
			default:
			die("error function ExtractPeriodDate() : The Parameter of Type \"".$type."\" Is Undefined.");
			break;
		}
		return $time;

	}

	function breadcrumb()
	{
		$url = $this->uri->segment(1).'/'.$this->uri->segment(2);
		// Main
		$url_main = $this->uri->segment(1);

		$bread = '';
		// get main menu 
		$check_main = $this->uri->segment(2);
		if (empty($check_main)) {

			$sql_main ="select menu_title,menu_url
				from m_menu
				where menu_url = ?
			";
			$query_main = $this->db->query($sql_main,array($url_main));
			$row_main = $query_main->row_array();

			if (count($row_main)>0) {
				$bread .= $row_main['menu_title'];
			}
		}

		// get parents menu (ada ato tidak)
		$sql ="select menu_title,menu_url
				from m_menu
				where menu_id in(select menu_parent
								from m_menu
								where menu_id in (select menu_parent
													from m_menu
													where menu_url = ?))
			";
		$query = $this->db->query($sql,array($url));
		$row = $query->row_array();

		if (count($row)>0) {
			$bread .= $row['menu_title'] . ' / ';
		}
		
		$sql2 ="select menu_title,menu_url
				from m_menu
				where menu_id in(select menu_parent
								from m_menu
								where menu_url = ?)
			";
		$query2 = $this->db->query($sql2,array($url));
		$row2 = $query2->row_array();
		
		if (count($row2)>0) {
			$bread .= $row2['menu_title']. ' / ';
		}

		$sql3 ="select menu_title,menu_url
				from m_menu
				where menu_url = ?
			";
		$query3 = $this->db->query($sql3,array($url));
		$row3 = $query3->row_array();

		if (count($row3)>0) {
			$bread .= $row3['menu_title'];
		}

		$this->load->vars('breadcrumb',$bread);

		// $url = $this->uri->segment(1).'/'.$this->uri->segment(2);

		// $bread = '';
		// // get parents menu (ada ato tidak)
		// $sql ="select menu_title,menu_url
		// 		from m_menu
		// 		where menu_id in(select menu_parent
		// 						from m_menu
		// 						where menu_id in (select menu_parent
		// 											from m_menu
		// 											where menu_url = ?))
		// 	";
		// $query = $this->db->query($sql,array($url));
		// $row = $query->row_array();

		// if (count($row)>0) {
		// 	$bread .= '<li> &nbsp;<a href="'.$row['menu_url'].'">'.$row['menu_title'].'</a><i class="fa fa-circle"></i></li>';
		// }
		
		// $sql2 ="select menu_title,menu_url
		// 		from m_menu
		// 		where menu_id in(select menu_parent
		// 						from m_menu
		// 						where menu_url = ?)
		// 	";
		// $query2 = $this->db->query($sql2,array($url));
		// $row2 = $query2->row_array();
		
		// if (count($row2)>0) {
		// 	$bread .= '<li> &nbsp;<a href="'.$row2['menu_url'].'">'.$row2['menu_title'].'</a><i class="fa fa-circle"></i></li>';
		// }

		// $sql3 ="select menu_title,menu_url
		// 		from m_menu
		// 		where menu_url = ?
		// 	";
		// $query3 = $this->db->query($sql3,array($url));
		// $row3 = $query3->row_array();

		// if (count($row3)>0) {
		// 	$bread .= '<li> &nbsp;<a href="'.$row3['menu_url'].'">'.$row3['menu_title'].'</a> </li>';
		// }

		// $this->load->vars('breadcrumb',$bread);

	}
	/*
	| BEGIN VALIDASI
	*/
	function validate_maxlength($val,$maxlength) {
		if (strlen($val)>$maxlength) {
			return false;
		} else {
			return true;
		}
	}
	function validate_minlength($val,$maxlength) {
		if (strlen($val)<=$maxlength) {
			return false;
		} else {
			return true;
		}
	}
	function validate_numeric($val) {
		return is_numeric($val);
	}
	function validate_required($val) {
		if (trim($val)=="") {
			return false;
		} else {
			return true;
		}
	}

	function pembulatan($premi=0,$pembulatan=0,$bataspembulatan=0)
	{
		// echo $pembulatan;
		// echo "<br/>";
		// echo $bataspembulatan;
		// die();
		$sisa_premi = $premi % $pembulatan;
		if ($sisa_premi > 0){
			$premi_dikurangi = $premi-$sisa_premi;
			if($sisa_premi>=$bataspembulatan){
				$premibayar = $premi_dikurangi+$pembulatan;
			}else{
				$premibayar = $premi_dikurangi;
			}
		}else{
			$premibayar = $premi;
		}
		return $premibayar;
	}

	function roundup($number=0, $digit=0) {
	  	if ($digit<0) {
	  		$digit = $digit*-1;
	  	}
	  	$sat = pow(10, $digit);
	  	$new_number = ceil($number/$sat)*$sat;

	  	return $new_number;
	}
	  // $val = 137001;
	  // $ret = roundup($val,-3);
	  // echo $val;
	  // echo "<br/>";
	  //Ganti val dengan hasil perhitungan val = manfaat*rate

	function pembulatan_premi($premi=0,$pembulatan=0)
	{
		$sisa = $premi%$pembulatan;
		if($sisa==0){
			$premi = $premi;
		}else if($pembulatan>0){
			$premi = $premi-$sisa+$pembulatan;
		}else if($pembulatan<0){
			$premi = $premi-$sisa;
		}else{
			$premi = $premi;
		}
		return $premi;
	}

	function makeInt($angka)
	{
	    if ($angka < -0.0000001){
	        return ceil($angka-0.0000001);
	    }else{
	        return floor($angka+0.0000001);
	    }
	}
	
	function konvhijriah($tanggal){ //yyyy-mm-dd

	    $array_bulan = array("Muharram"
	                        ,"Safar"
	                        ,"Rabiul Awwal"
	                        ,"Rabiul Akhir"
	                        ,"Jumadil Awwal"
	                        ,"Jumadil Akhir"
	                        ,"Rajab"
	                        ,"Sya’ban"
	                        ,"Ramadhan"
	                        ,"Syawwal"
	                        ,"Dzulqo'dah"
	                        ,"Dzulhijjah");

	    $date = $this->makeInt(substr($tanggal,8,2));
	    $month = $this->makeInt(substr($tanggal,5,2));
	    $year = $this->makeInt(substr($tanggal,0,4));

	    if (($year>1582)||(($year == "1582") && ($month > 10))||(($year == "1582") && ($month=="10")&&($date >14)))
	    {

	        $jd = $this->makeInt((1461*($year+4800+$this->makeInt(($month-14)/12)))/4)+

	        $this->makeInt((367*($month-2-12*($this->makeInt(($month-14)/12))))/12)-

	        $this->makeInt( (3*($this->makeInt(($year+4900+$this->makeInt(($month-14)/12))/100))) /4)+

	        $date-32075;

	    }
	    else
	    {
	        $jd = 367*$year-$this->makeInt((7*($year+5001+$this->makeInt(($month-9)/7)))/4)+

	        $this->makeInt((275*$month)/9)+$date+1729777;
	    }

	    $wd = $jd%7;

	    $l = $jd-1948440+10632;

	    $n=$this->makeInt(($l-1)/10631);

	    $l=$l-10631*$n+354;

	    $z=($this->makeInt((10985-$l)/5316))*($this->makeInt((50*$l)/17719))+($this->makeInt($l/5670))*($this->makeInt((43*$l)/15238));

	    $l=$l-($this->makeInt((30-$z)/15))*($this->makeInt((17719*$z)/50))-($this->makeInt($z/16))*($this->makeInt((15238*$z)/43))+29;

	    $m=$this->makeInt((24*$l)/709);

	    $d=$l-$this->makeInt((709*$m)/24);

	    $y=30*$n+$z-30;

	    $g = $m-1;

	    $final = "$d $array_bulan[$g] $y H";

	    return $final;

	}

	function getUserIP()
	{
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}

	public function postUrl($url,$data) 
	{
		$postdata = http_build_query($data);
	    $opts = array('http' =>
	        array(
	            'method'  => 'POST',
	            'header'  => 'Content-type: application/x-www-form-urlencoded',
	            'content' => $postdata
	        )
	    );
	    $context = stream_context_create($opts);
	    return file_get_contents($url, false, $context);
	}

	// public function action_send_sms($msisdn,$message,$no_polis)
	// {
	// 	$link4execute = 'http://brilife.co.id/node/serverIBOnline/services/brilifesms';
	// 	$dataPost = array('msisdn'=>$msisdn,'message'=>$message);
	// 	$res = $this->postUrl($link4execute,$dataPost);
	// 	echo $res;
	// }

	public function DatepickerSlashToEn($datepicker) {
		if($datepicker!=''){
			$datepicker = str_replace('-', '', $datepicker);
	        $date = substr($datepicker,4,4).'-'.substr($datepicker,2,2).'-'.substr($datepicker,0,2);
	        return $date;
	    }
	}

	public function DateTimepickerToIna($datepicker) {
		$datepicker = str_replace('-', '', $datepicker);
        $date = substr($datepicker,6,2).'-'.substr($datepicker,4,2).'-'.substr($datepicker,0,4).' '.substr($datepicker,9);
        return $date;
	}

	function paymode($carbay) 
	{
		switch ($carbay) {
			case '1':
				$faktor = 2;
				break;
			case '2':
				$faktor = 4;
				break;
			case '3':
				$faktor = 7;
				break;
			case '4':
				$faktor = 13;
				break;
			case '5':
				$faktor = 0;
				break;
			
			default:
				$faktor = 1;
				break;
		}
		return $faktor;
	}

	function getBiaya($masa_pembayaran,$masa_perjanjian) 
	{
	  	for ($i=1;$i<=$masa_perjanjian;$i++) {  		
	       if ($i<4) {
	            if ($i==1) {
	              $vBiaya = ($masa_pembayaran * 3.5);
				  if ($vBiaya>40) {
	               $vBiaya=40;
				  }
				  $biaya = $vBiaya+10;
	            } else {
	              $vBiaya = ($vBiaya/2);
				  $biaya = $vBiaya+2.5;
	            }
	            
	            $arrBiaya[]=$biaya;
	       } else 
	       if ($i<=$masa_pembayaran)
	       {
	            $biaya = 2.5;
	            $arrBiaya[]=$biaya;
	       } else {
	       	    $biaya = 0;
	            $arrBiaya[]=$biaya;
	       }
	    }
	    return $arrBiaya;
	}

	function getRate() 
	{
		$sql = "SELECT usia,ti,tpd,dd,pa from rate_purnadana";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function calc_purnadana($kontribusi,$masa_pembayaran,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dana_kebajikan_ti,$dana_kebajikan_pa,$flg_tpd=0,$flg_dd=0) 
  	{
	  	$thn = 1;
	  	$flg_thn = 0;
		$akum_kontribusi = 0;
		$akum_tot_biaya = 0;  
	  	$faktor = $this->paymode($carbay);
	  	$tot_kontribusi = $kontribusi*(12/($faktor-1))*$masa_pembayaran;
	  	$t_biaya = $this->getBiaya($masa_pembayaran,$masa_perjanjian);
	  	$ratearray = $this->getRate();

		foreach ($ratearray as $key ) {
			$rate[1][$key['usia']] = $key['ti'];
			$rate[2][$key['usia']] = $key['tpd'];
			$rate[3][$key['usia']] = $key['dd'];
			$rate[4][$key['usia']] = $key['pa'];
		}

	  	$rate_wakalah = 0.03;  	
	  	$biaya_admin = 15000;

	  	for ($i=1;$i<=$masa_perjanjian*12;$i++) {
		  	if (((($i-1) % 12)==0)  && ($i<>1)) {
		  		$usia++;
		  		$thn++;
		  		$flg_thn = 1;
		  	} else {$usia =$usia;$thn=$thn;$flg_thn=0;}
		  	$flag = 0;
		  	$vkontribusi = $kontribusi;
		  	if ($i==1) {
		  		$n = $i;  		
		  		$flag = 1;  		
		  	} 
		  	if ($i==$n)
		  	{
		  		$n = $n+($faktor-1);
		  		$flag = 1;  		
		  	} else {
		  		$flag = 0;
		  	}
		  	$vkon = ($vkontribusi*$flag);
		  	if ($i<=($masa_pembayaran*12)) {
		  		$vkon = $vkon;
		  	} else {
		  		$vkon=0;	
		  	}
		  	$akum_kontribusi = $akum_kontribusi + ($vkontribusi*$flag);
			if($carbay!='5'){
				$waiver = $tot_kontribusi-$akum_kontribusi;
			}else{
				$waiver = 0;
			}
		  	$tbr_ti =($rate[1][$usia]*$dana_kebajikan_ti)/1000;
		  	$tbr_pa = ($rate[4][$usia]*$dana_kebajikan_pa)/1000;  	  	

		  	if ($waiver<0) { $waiver=0;};
		  	if ($flg_tpd==1) {
		  		$tbr_tpd = ($rate[2][$usia]*$waiver)/1000;
		  	} else {$tbr_tpd=0;}
		  	if ($flg_dd==1) {
		  		$tbr_dd = ($rate[3][$usia]*$waiver)/1000;
		  	} else {$tbr_dd=0;}

		  	$tbr_tot = $tbr_ti+$tbr_pa+$tbr_tpd+$tbr_dd;
		  	$tbr_tot = $tbr_tot *10/100;
			if ($akum_kontribusi>$tot_kontribusi) {$akum_kontribusi=$tot_kontribusi;}
			if($carbay=='5'){

				$biaya_kontribusi = $kontribusi*13.5/100;
			}else{
				$biaya_kontribusi = $kontribusi*$t_biaya[$thn-1]*$flag/100;
			}
		  	if ($i==1) {
				if($carbay == '5'){
					$dana_awal = ($kontribusi-$tbr_tot-$biaya_kontribusi-$biaya_admin)*97/100;	
					$wakalah_fee = $dana_awal*$rate_wakalah;	
					$hasil_investasi = $dana_awal*(pow((1+$rate_investasi),(1/12))-1);
					$nilai_tunai = $dana_awal + $hasil_investasi;
					$total_biaya = $biaya_kontribusi+$biaya_admin+$wakalah_fee;
				}else{
					$dana_awal = $kontribusi-$tbr_tot-$biaya_kontribusi-$biaya_admin;	
					$wakalah_fee = $dana_awal*$rate_wakalah;	
					$hasil_investasi = ($dana_awal - $wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
					$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
					$total_biaya = $biaya_admin+$wakalah_fee;
				}
		  	} else {
				if($carbay =='5'){
					$dana_awal = $nilai_tunai-$tbr_tot-$biaya_admin;
					$wakalah_fee = $dana_awal*$rate_wakalah*$flg_thn;
					$hasil_investasi = ($dana_awal-$wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
					$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
					$total_biaya = $biaya_kontribusi+$biaya_admin+$wakalah_fee;
				}else{
					$dana_awal = $vkon-$tbr_tot-$biaya_kontribusi-$biaya_admin+$nilai_tunai;
					$wakalah_fee = $dana_awal*$rate_wakalah*$flg_thn;	
					$hasil_investasi = ($dana_awal - $wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
					$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
					$total_biaya = $biaya_kontribusi+$biaya_admin+$wakalah_fee;
				}
			}

			if($carbay=='5'){
				$akum_kontribusi = $kontribusi;
			}

			if($usia>60){
				$dana_kebajikan_pa = $dana_kebajikan_ti;
			}

			$akum_tot_biaya = $akum_tot_biaya + $total_biaya;

		  	$result[]=array(
		  		'bln'=>$i,
		  		'usia'=>$usia,
		  		'pembayaran'=>$vkon,
		  		'akum_kontribusi'=>$akum_kontribusi,
		  		'dana_kebajikan_ti'=>$dana_kebajikan_ti,
		  		'dana_kebajikan_pa'=>$dana_kebajikan_pa,
		  		'waiver'=>$waiver,
		  		'tbr_ti'=>$tbr_ti,
		  		'tbr_pa'=>$tbr_pa,
		  		'tbr_tpd'=>$tbr_tpd,
		  		'tbr_dd'=>$tbr_dd,
		  		'tbr_tot'=>$tbr_tot,
		  		'biaya_admin'=>$biaya_admin,
		  		'biaya_kontribusi'=>$biaya_kontribusi,
		  		'dana_awal'=>$dana_awal,
		  		'wakalah_fee'=>$wakalah_fee,
		  		'hasil_investasi'=>$hasil_investasi,
				'nilai_tunai'=>$nilai_tunai,
				'akum_total_biaya'=>$akum_tot_biaya
		  		);
	  	}
		 print_r($result);
	  	return $result;
	}

	public function uuid($hyphen = true) {

		// The field names refer to RFC 4122 section 4.1.2
		if($hyphen == false){
			return sprintf('%04x%04x%04x%03x4%04x%04x%04x%04x',
			mt_rand(0, 65535), mt_rand(0, 65535), // 32 bits for "time_low"
			mt_rand(0, 65535), // 16 bits for "time_mid"
			mt_rand(0, 4095),  // 12 bits before the 0100 of (version) 4 for "time_hi_and_version"
			bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
			// 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
			// (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
			// 8 bits for "clk_seq_low"
			mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) // 48 bits for "node"
			);
		}else{
			return sprintf('%04x%04x-%04x-%03x4-%04x-%04x%04x%04x',
			mt_rand(0, 65535), mt_rand(0, 65535), // 32 bits for "time_low"
			mt_rand(0, 65535), // 16 bits for "time_mid"
			mt_rand(0, 4095),  // 12 bits before the 0100 of (version) 4 for "time_hi_and_version"
			bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
			// 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
			// (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
			// 8 bits for "clk_seq_low"
			mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) // 48 bits for "node"
			);
		}
	} 

	function calc_danasiswa($kontribusi,$masa_pembayaran,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dana_kebajikan_ti,$dana_kebajikan_pa,$flg_tpd,$flg_ci,$dana_pendidikan,$usia_anak) 
  	{
	  	$thn = 1;
	  	$flg_thn = 0;
	  	$akum_kontribusi = 0;
	  	$faktor = $this->paymode($carbay);
	  	$tot_kontribusi = $kontribusi*(12/($faktor-1))*$masa_pembayaran;
	  	$t_biaya = $this->getBiaya($masa_pembayaran,$masa_perjanjian);
	  	$ratearray = $this->getRate_danasiswa();
	  	$arr_tahapan[4]=(object) array(
		     'jenjang'=>'TK-A',
		     'persen'=>5
		   );
	  	$arr_tahapan[5]=(object) array(
		     'jenjang'=>'TK-B',
		     'persen'=>10
		   );
	  	$arr_tahapan[6]=(object) array(
		     'jenjang'=>'SD',
		     'persen'=>15
		   );
	  	$arr_tahapan[12]=(object) array(
		     'jenjang'=>'SMP',
		     'persen'=>20
		   );
	  	$arr_tahapan[15]=(object) array(
		     'jenjang'=>'SMU',
		     'persen'=>25
		   );
	  	$arr_tahapan[18]=(object) array(
		     'jenjang'=>'PT-1',
		     'persen'=>30
		   );
	  	$arr_tahapan[19]=(object) array(
		     'jenjang'=>'PT-2',
		     'persen'=>35
		   );
	  	$arr_tahapan[20]=(object) array(
		     'jenjang'=>'PT-3',
		     'persen'=>40
		   );
	  	$arr_tahapan[21]=(object) array(
		     'jenjang'=>'PT-4',
		     'persen'=>50
		   );
	  	$arr_tahapan[22]=(object) array(
		     'jenjang'=>'PT-5',
		     'persen'=>100
		   );
		foreach ($ratearray as $key ) {
			$rate[1][$key['usia']] = $key['ti'];
			$rate[2][$key['usia']] = $key['tpd'];
			$rate[3][$key['usia']] = $key['ci'];
			$rate[4][$key['usia']] = $key['pa'];
			$rate[5][$key['usia']] = $key['ta'];
		}

	  	$rate_wakalah = 0.03;  	
	  	$biaya_admin = 15000;
	  	$manfaat_anak = 15000000;

	  	for ($i=1;$i<=(($masa_perjanjian*12)+1);$i++) {
		  	if (((($i-1) % 12)==0)  && ($i<>1)) {
		  		$usia++;
		  		$usia_anak++;		  		
		  		$thn++;
		  		$flg_thn = 1;
		  		if (isset($arr_tahapan[$usia_anak])) {		  					  				 
		  			if ($thn>2) { 			
			  			if ($usia_anak<18) {
			  				$tahapan = $arr_tahapan[$usia_anak]->persen*$dana_pendidikan/100;
			  			} else {
			  				$tahapan = $arr_tahapan[$usia_anak]->persen*$nilai_tunai/100;	
			  			}
			  			$jenjang = $arr_tahapan[$usia_anak]->jenjang;	
		  				$persen = $arr_tahapan[$usia_anak]->persen;
		  			}
		  		}
		  		 
		  	} else {$usia =$usia;$thn=$thn;$flg_thn=0;$tahapan=0;$jenjang='';$persen=0;}
		  	$flag = 0;
		  	$vkontribusi = $kontribusi;
		  	if ($i==1) {
		  		$n = $i;  		
		  		$flag = 1;  		
		  	} 
		  	if ($i==$n)
		  	{
		  		$n = $n+($faktor-1);
		  		$flag = 1;  		
		  	} else {
		  		$flag = 0;
		  	}
		  	$vkon = ($vkontribusi*$flag);
		  	if ($i<=($masa_pembayaran*12)) {
		  		$vkon = $vkon;
		  	} else {
		  		$vkon=0;	
		  	}
			$akum_kontribusi = $akum_kontribusi + ($vkontribusi*$flag);
			if($carbay!='5'){
				$waiver = $tot_kontribusi-$akum_kontribusi;
			}else{
				$waiver = 0;
			}
		  	$tbr_ti =($rate[1][$usia]*$dana_kebajikan_ti)/1000;
		  	$tbr_pa = ($rate[4][$usia]*$dana_kebajikan_pa)/1000; 
		  	$tbr_anak = ($rate[5][$usia_anak]*$manfaat_anak)/1000; 
			$tbr_ti_waiver = ($rate[1][$usia]*$waiver)/1000; 	

		  	// if ($waiver<0) { $waiver=0;};
		  	if ($flg_tpd==1) {
		  		$tbr_tpd = ($rate[2][$usia]*$waiver)/1000;
		  	} else {$tbr_tpd=0;}
		  	if ($flg_ci==1) {
		  		$tbr_ci = ($rate[3][$usia]*$waiver)/1000;
		  	} else {$tbr_ci=0;}
			
		  	$tbr_tot = $tbr_ti+$tbr_pa+$tbr_tpd+$tbr_ci+$tbr_anak+$tbr_ti_waiver;
		  	$tbr_tot = $tbr_tot *10/100;
			// echo"<pre>";print_r('TI '.$tbr_ti.' pa '.$tbr_pa.' tpd  '.$tbr_tpd.' ci '.$tbr_ci.' anaka '.$tbr_anak.' waover '.$tbr_ti_waiver);die();  
			if ($akum_kontribusi>$tot_kontribusi) {$akum_kontribusi=$tot_kontribusi;}

		  	if (isset($t_biaya[$thn-1])) {
			  	$biaya_kontribusi = $kontribusi*$t_biaya[$thn-1]*$flag/100;
		  	}else{
		  		$biaya_kontribusi = 0;
		  	}

		  	if ($i==1) {
		  		$dana_awal = $kontribusi-$tbr_tot-$biaya_kontribusi-$biaya_admin;	
		  		$wakalah_fee = $dana_awal*$rate_wakalah;	
		  		$hasil_investasi = ($dana_awal - $wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
		  		$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
		  		$nilai_tunai = $this->rounddown($nilai_tunai,-3);
		  	} else {
				$dana_awal = $vkon-$tbr_tot-$biaya_kontribusi-$biaya_admin+$nilai_tunai-$tahapan;
				$wakalah_fee = $dana_awal*$rate_wakalah*$flg_thn;	
		  		$hasil_investasi = ($dana_awal - $wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
		  		$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
		  		$nilai_tunai = $this->rounddown($nilai_tunai,-3);
		  	}
			if ($i<(($masa_perjanjian*12)+1)) {
				$dana_awal = $dana_awal;
				$nilai_tunai = $nilai_tunai;
				$dana_kebajikan_ti = $dana_kebajikan_ti;
				$dana_kebajikan_pa = $dana_kebajikan_pa;
				$tbr_tot = $tbr_tot;
				$akum_kontribusi = $akum_kontribusi;
			} else {
				$dana_awal = 0;
				$nilai_tunai = 0;
				$dana_kebajikan_ti = 0;
				$dana_kebajikan_pa = 0;
				$tbr_tot = 0;
				$akum_kontribusi = 0;
			}
			if($carbay=='5'){
				$akum_kontribusi = $kontribusi;
			}
		  	$result[]=array(
		  		'bln'=>$i,
		  		'usia'=>$usia,
		  		'pembayaran'=>$vkon,
		  		'akum_kontribusi'=>$akum_kontribusi,
		  		'dana_kebajikan_ti'=>$dana_kebajikan_ti,
		  		'dana_kebajikan_pa'=>$dana_kebajikan_pa,
		  		'waiver'=>$waiver,
		  		'tbr_ti'=>$tbr_ti,
		  		'tbr_ti_waiver'=>$tbr_ti_waiver,
		  		'tbr_pa'=>$tbr_pa,
		  		'tbr_tpd'=>$tbr_tpd,
		  		'tbr_ci'=>$tbr_ci,
		  		'tbr_anak'=>$tbr_anak,
		  		'tbr_tot'=>$tbr_tot,
		  		'biaya_admin'=>$biaya_admin,
		  		'biaya_kontribusi'=>$biaya_kontribusi,
		  		'dana_awal'=>$dana_awal,
		  		'wakalah_fee'=>$wakalah_fee,
		  		'hasil_investasi'=>$hasil_investasi,
		  		'nilai_tunai'=>$nilai_tunai,
		  		'tahapan'=>$tahapan,
		  		'jenjang'=>$jenjang,
		  		'persen'=>$persen	
		  		);
	  	}
	  	return $result;
	}

	function getRate_danasiswa() 
	{
		$sql = "SELECT usia,ti,tpd,ci,pa,ta from rate_danasiswa";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

 	function rounddown($number,$digits) {
 		if ((is_numeric($number)) && (is_numeric($digits))) {
              $significance = pow(10,$digits);
             if ($number < 0.0) {
                  return ceil($number * $significance) / $significance;
              } else {
                  return floor($number * $significance) / $significance;
              }
          }
 	}

	function getKomposisiUjroh($product_code)
	{
		$url="http://brilifesyariah.com/index.php/webservices/getKomposisiUjroh";
		$url .= "?product_code=".$product_code;

		$row = file_get_contents($url);
		// $row = getJSONData($url);
		// $row = json_decode($row);

		return $row;
	}

	function getBiayaDanakarya($masa_perjanjian,$carbay) 
  	{  
	        unset($biaya);
		for ($i=1;$i<=$masa_perjanjian;$i++) {  	
			if ($carbay<>'5') {
		  		if ($i<=5){
		  			$biaya[$i]=4.5/100;
		  		} else {
		  			$biaya[$i]=1.5/100;
		  		}	  	
	  		} else {
	  			if ($i==1) {
	  				$biaya[$i]=4/100;
	  			} else {
	  				$biaya[$i]=0;
	  			}
	  		}
	  	}	  	
		if (!isset($biaya)) {
		  $biaya[]=0;
		}
	  	return $biaya;
  	}
  	
  	function fv($pmt,$n,$rate) {
	  	$rate = $rate/12;
	  	$fv = $pmt * ((pow((1+$rate/$n),($n*12))-1)/($rate/$n))*(1+($rate/$n));
		return $fv;
  	}

	function getRateDanakarya() 
	{
		$sql = "SELECT usia,ti,a,b,tpd from rate_danakarya";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function paymodeDanakarya($carbay) 
	{
		switch ($carbay) {
			case '1': //bulanan
				$faktor = 2;
				break;
			case '2': //triwulan
				$faktor = 4;
				break;
			case '3': //semesteran
				$faktor = 7;
				break;
			case '4': //tahunan
				$faktor = 13;
				break;
			case '5': //sekaligus
				$faktor = 0;
				break;
			
			default:
				$faktor = 1;
				break;
		}
		return $faktor;
	}

  	public function calc_danakarya($kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dana_kebajikan_ti,$flg_paa=0,$flg_pab=0,$flg_tpd=0,$t_rate,$t_biaya,$nama,$tanggal_lahir,$tanggal_mulai_asuransi,$akhir_asuransi,$nilaiTunai_init,$rate_wakalah,$biaya_admin,$kontribusi_awal) 
  	{
	  	$thn = 1;
	  	$flg_thn = 0;
	  	$akum_kontribusi = 0;
	  	$akum_tabarru = 0;
	  	$akum_ujroh = 0;
	  	$biaya_kontribusi_awal = 4/100;
	  	$faktor = $this->paymodeDanakarya($carbay);
	  	$tot_kontribusi = $kontribusi*(12/($faktor-1))*$masa_perjanjian;
	  	//$t_biaya = $this->getBiayaDanakarya($masa_perjanjian,$carbay);
	  	$ratearray = $this->getRateDanakarya();
		$result = array();
		foreach ($ratearray as $key ) {
			$rate[1][$key['usia']] = $key['ti'];
			$rate[2][$key['usia']] = $key['a'];
			$rate[3][$key['usia']] = $key['b'];
			$rate[4][$key['usia']] = $key['tpd'];
		}

	  	//$rate_wakalah = $rate_wakalah/100;	
	  	//$biaya_admin = 5000;
	  	$dana_awal = 0;
	  	$usia_awal = $usia;
		for ($i=1;$i<=$masa_perjanjian*12;$i++) 
		{
		  	if (((($i-1) % 12)==0)  && ($i<>1)) {
		  		$usia++;
		  		$thn++;
		  		$flg_thn = 1;
		  	} else {$usia =$usia;$thn=$thn;$flg_thn=0;}
		  	$flag = 0;
		  	$vkontribusi = $kontribusi;
		  	/* Komen karena carabayar tahunan tdk jalan
		  	if ($i==1) {
		  		$n = $i;  		
		  		$flag = 1;  		
		  	} 
		  	if ($i==$n)
		  	{
		  		$n = $n+($faktor-1);
		  		$flag = 1;  		
		  	} else {
		  		$flag = 0;
		  	}
		  	*/
		  	if ($i==1) {
		  		$flag = 1;
		  	} else {
		  		if (($i % ($faktor-1)==0)) {
		  			$flag = 1;
		  		} else {
		  			$flag = 0;
		  		}
		  	}
		  	$vkon = ($vkontribusi*$flag);
		  	//if ($i<=($masa_pembayaran*12)) {
		  	//	$vkon = $vkon;
		  	//} else {
		  	//	$vkon=0;	
		  	//}
		  	$akum_kontribusi = $akum_kontribusi + ($vkontribusi*$flag);
		  	$waiver = $tot_kontribusi-$akum_kontribusi;
		  	 // echo "<pre>";
		  	 // echo "Usia TERM".$usia."=".$rate[1][$usia];
		  	 // echo "Usia PA".$usia."=".$rate[2][$usia];
		  	$tbr_ti =($rate[1][$usia]*$dana_kebajikan_ti)/1000;		  			  	
		  	if ($flg_tpd==1) {
		  		$tbr_tpd = ($rate[4][$usia]*$dana_kebajikan_ti*1)/1000;
		  	} else {$tbr_tpd=0;}
		  	if ($flg_paa==1) {
		  		$tbr_paa = ($rate[2][$usia]*$dana_kebajikan_ti*1)/1000;
		  	} else {$tbr_paa=0;}
		  	if ($flg_pab==1) {
		  		$tbr_pab = ($rate[3][$usia]*$dana_kebajikan_ti*1)/1000;
		  	} else {$tbr_pab=0;}

		  	$tbr_tot = $tbr_ti+$tbr_paa+$tbr_pab+$tbr_tpd;
		  	$tbr_tot = $tbr_tot *9/100;
		  	$akum_tabarru = $akum_tabarru+$tbr_tot;		  	
		  	if ($akum_kontribusi>$tot_kontribusi) {$akum_kontribusi=$tot_kontribusi;}
		  	$biaya_kontribusi = $kontribusi*$t_biaya[$thn]*$flag;
		  	if ($i==1) {
		  		$dana_awal = $kontribusi-$tbr_tot-$biaya_kontribusi-$biaya_admin;			  		
		  		$ujroh_kontribusi_awal = $kontribusi_awal*$biaya_kontribusi_awal;
		  		$dana_awal = $dana_awal+($kontribusi_awal-$ujroh_kontribusi_awal);
		  		$wakalah_fee = $dana_awal*$rate_wakalah;	
		  		$akum_ujroh = $akum_ujroh+$biaya_kontribusi+$biaya_admin+$wakalah_fee;
		  		$akum_ujroh = $akum_ujroh+$ujroh_kontribusi_awal;
		  		$hasil_investasi = ($dana_awal - $wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
		  		//$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
		  		$nilai_tunai = $dana_awal * (1-$rate_wakalah) * pow((1+$rate_investasi),(1/12));
		  	} else {
				$dana_awal = $vkon-$tbr_tot-$biaya_kontribusi-$biaya_admin+$nilai_tunai;
				$wakalah_fee = $dana_awal*$rate_wakalah*$flg_thn;	
				$akum_ujroh = $akum_ujroh+$biaya_kontribusi+$biaya_admin+$wakalah_fee;
		  		$hasil_investasi = ($dana_awal - $wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
		  		//$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
		  		$nilai_tunai = ($dana_awal) * (1-($rate_wakalah*$flg_thn)) * pow((1+$rate_investasi),(1/12));;
		  	}
			if ($nilaiTunai_init<0) {
			    $nilaiTunai_init = 0;
			}
			if ($nilai_tunai<0) {
			   $nilai_tunai = 0;
			}
			if($carbay=='5'){
				$akum_kontribusi = $kontribusi;
			}
		  	$result[]=array(
		  		'bln'=>$i,
		  		'usia'=>$usia_awal,
		  		'pembayaran'=>$vkon,
		  		'akum_kontribusi'=>$akum_kontribusi,
		  		'dana_kebajikan_ti'=>$dana_kebajikan_ti,		  				  		
		  		'tbr_ti'=>$tbr_ti,
		  		'tbr_paa'=>$tbr_paa,
		  		'tbr_tpd'=>$tbr_tpd,
		  		'tbr_pab'=>$tbr_pab,
		  		'tbr_tot'=>$tbr_tot,//tabarru
		  		'biaya_admin'=>$biaya_admin,
		  		'biaya_kontribusi'=>$biaya_kontribusi,
		  		'akum_tabarru'=>$akum_tabarru,
		  		'akum_ujroh'=>$akum_ujroh,
		  		'ujroh'=>($biaya_admin+$biaya_kontribusi),
		  		'dana_awal'=>$dana_awal,
		  		'wakalah_fee'=>$wakalah_fee,
		  		'hasil_investasi'=>$hasil_investasi,
		  		'nilai_tunai'=>$nilai_tunai,
		  		'nama'=>$nama,	
		  		'tanggal_lahir'=>$tanggal_lahir,	
		  		'tanggal_mulai_asuransi'=>$tanggal_mulai_asuransi,	
		  		'akhir_asuransi'=>substr($akhir_asuransi,0,10),	
		  		'masa_perjanjian'=>$masa_perjanjian,
		  		'kontribusi'=>$kontribusi,
		  		'kontribusi_inisial'=>$nilaiTunai_init
		  		);
		}
		//echo "========================================NEW PARTICIAPNT";
		return $result;
  	}

  // 	function calc_danakebajikan($kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dk_awal,$flg_paa,$flg_pab,$flg_tpd) 
  // 	{
		// $dk_awal = round(fv($kontribusi,$masa_perjanjian,$rate_investasi));	  
		// $result = $this->calc_danakarya($kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dk_awal,$flg_paa,$flg_pab,$flg_tpd);  
		// $nt = $result[($masa_perjanjian*12-1)]['nilai_tunai'];	  
		// $delta = $dk_awal-$nt;
		// $z=1;  	 
		// while ($delta>($kontribusi/1000)) {
		//   	$dk_awal = $dk_awal-100000;  	
		//   	$result = $this->calc_danakarya($kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dk_awal,$flg_paa,$flg_pab,$flg_tpd);
		//   	$nt = $result[($masa_perjanjian*12-1)]['nilai_tunai'];
		//   	$delta = $dk_awal - $nt;
		//   	//echo $z."   |".number_format($dk_awal,0,',','.')."      |".number_format($nt,0,',','.')."      |".number_format($delta,0,',','.');
		//   	//echo "<br>";
		//   	$dk[$z]=$dk_awal;
		//   	$z++;	  	
		// }
	 //  	return $dk[$z-1];
  // 	}

  	function getKontribusi($mak,$masa_perjanjian,$rate_investasi) 
  	{  
	  	$rate = $rate_investasi/12;
	  	$n = $masa_perjanjian;
	  	$pmt = $mak / ((pow((1+$rate/$n),($n*12))-1)/($rate/$n))*(1+($rate/$n));
		return $pmt;
	}

	//Metode search kontribusi dengan bisection									
	function searchKontribusi_bisection($mak,$dana_kebajikan,$masa_perjanjian,$usia,$rate_investasi,$paa,$pab,$tpd,$rate,$t_biaya,$kontribusi_awal) {
  		$z = 0;
  		//$mak = 90974753;
  		//$masa_perjanjian=8;
  		//$rate_investasi=0.07;
  		$carbay = '1';
  		//$rate = getRate();
  		//$t_biaya = getBiaya($masa_perjanjian,$carbay);
  		$kontribusi_0 = 0;
  		if (!isset($kontribusi_awal)) {
  			$kontribusi_awal=0;
  		}
  		$res = $this->calc_danakarya($kontribusi_0,$masa_perjanjian,$usia,$carbay,$rate_investasi,$mak,$paa,$pab,$tpd,$rate,$t_biaya,$t_biaya,$nama='',$tanggal_lahir='',$tanggal_mulai_asuransi='',$akhir_asuransi='',$kontribusi_inisial='0',$rate_wakalah=3,$biaya_admin=5000,$kontribusi_awal); 
  		// $nt_0 = $res[$masa_perjanjian*12-1]['nilai_tunai'];
  		$nt_0 = 0;
  		$kontribusi_1 = $this->getKontribusi($mak,$masa_perjanjian,$rate_investasi);
  		$res = $this->calc_danakarya($kontribusi_1,$masa_perjanjian,$usia,$carbay,$rate_investasi,$mak,$paa,$pab,$tpd,$rate,$t_biaya,$t_biaya,$nama='',$tanggal_lahir='',$tanggal_mulai_asuransi='',$akhir_asuransi='',$kontribusi_inisial='0',$rate_wakalah=3,$biaya_admin=5000,$kontribusi_awal); 
  		// $nt_1 = $res[$masa_perjanjian*12-1]['nilai_tunai'];
  		$nt_1 = 0;
  		
  		$delta = $nt_1-$mak;
  		$koefisien = $mak;
  		while (abs($koefisien)>0.001 && ($z<=100)) {  		
  			$new_kontribusi = ($kontribusi_1+$kontribusi_0)/2;
	  		$result = $this->calc_danakarya($new_kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$mak,$paa,$pab,$tpd,$rate,$t_biaya,$t_biaya,$nama='',$tanggal_lahir='',$tanggal_mulai_asuransi='',$akhir_asuransi='',$kontribusi_inisial='0',$rate_wakalah=3,$biaya_admin=5000,$kontribusi_awal);  		
	  		// $nt = $result[($masa_perjanjian*12-1)]['nilai_tunai'] ;
	  		$nt =0 ;
	  		$delta = $mak - $nt;
	  		$koefisien = $kontribusi_1 - $kontribusi_0;
	  		if ($delta>0) {
	  			$kontribusi_0 = $new_kontribusi;
	  			$kontribusi_1 = $kontribusi_1;	  			
	  		} else {
	  			$kontribusi_0 = $kontribusi_0;
	  			$kontribusi_1 = $new_kontribusi;	  			
	  		}
	  		$z++;
	  	}
	  	return $kontribusi_1;
  
  }
  	//Iterasi mencaari kontribusi cara biasa
  	function searchKontribusi($mak,$dana_kebajikan,$masa_perjanjian,$usia,$rate_investasi,$flg_paa=0,$flg_pab=0,$flg_tpd=0,$kontribusi_awal=0) 
  	{
  		// $rate_investasi=0.07;
  		$carbay = '1';
  		$rate = $this->getRateDanakarya();
  		$t_biaya = $this->getBiayaDanakarya($masa_perjanjian,$carbay);
  		$kontribusi_awal = $this->getKontribusi($mak,$masa_perjanjian,$rate_investasi);
  		$z = 1;
  		$delta = $kontribusi_awal;
		if (!isset($kontribusi_awal)) {
			$kontribusi_awal=0;
		}
  		while ($delta>10000) {
	  		$result = $this->calc_danakarya($kontribusi_awal,$masa_perjanjian,$usia,$carbay,$rate_investasi,$mak,$flg_paa,$flg_pab,$flg_tpd,$rate,$t_biaya,$nama='',$tanggal_lahir='',$tanggal_mulai_asuransi='',$akhir_asuransi='',$kontribusi_inisial='0',$rate_wakalah=3,$biaya_admin=5000,$kontribusi_awal);  		
	  		$delta = $mak - $result[($masa_perjanjian*12-1)]['nilai_tunai'] ;
	  		$kontribusi_awal = $kontribusi_awal+1000;
	  		$z++;
	  	}
	  	return $kontribusi_awal-1000;
  	}

  	public function calc_danakebajikan_fast($kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dana_kebajikan_ti=0,$flg_paa=0,$flg_pab=0,$flg_tpd=0,$rate,$t_biaya,$kontribusi_awal)
  	{
  		
  		$dk_0 = 0;
		if (!isset($kontribusi_awal)) {
			$kontribusi_awal=0;
		}
  		$res= $this->calc_danakarya($kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dk_0,$flg_paa,$flg_pab,$flg_tpd,$rate,$t_biaya,$nama='',$tanggal_lahir='',$tanggal_mulai_asuransi='',$akhir_asuransi='',$kontribusi_inisial='0',$rate_wakalah=3,$biaya_admin=5000,$kontribusi_awal);
  		
  		$nt_0 = $res[$masa_perjanjian*12-1]['nilai_tunai'];
  		$dk_1 = round($this->fv($kontribusi,$masa_perjanjian,$rate_investasi));
  		$res= $this->calc_danakarya($kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dk_1,$flg_paa,$flg_pab,$flg_tpd,$rate,$t_biaya,$nama='',$tanggal_lahir='',$tanggal_mulai_asuransi='',$akhir_asuransi='',$kontribusi_inisial='0',$rate_wakalah=3,$biaya_admin=5000,$kontribusi_awal);
  		$n = count($res);
  		$nt_1 = $res[$masa_perjanjian*12-1]['nilai_tunai'];
  		$presisi =0.1;
  		$i=0;
  		$max_iterasi=256;

  		while (abs($nt_1-$dk_1)>$presisi && $i< $max_iterasi) {
  			$new_dk = ($nt_1* ($nt_1-$nt_0)/($dk_1-$dk_0))+$nt_1;  			  			
  			$res_nt = $this->calc_danakarya($kontribusi,$masa_perjanjian,$usia,$carbay,$rate_investasi,$new_dk,$flg_paa,$flg_pab,$flg_tpd,$rate,$t_biaya,$nama='',$tanggal_lahir='',$tanggal_mulai_asuransi='',$akhir_asuransi='',$kontribusi_inisial='0',$rate_wakalah=3,$biaya_admin=5000,$kontribusi_awal);
  			$nt = $res_nt[$masa_perjanjian*12-1]['nilai_tunai'];
  			if ($nt>$nt_1) {
  				$dk_0=$new_dk;
  				$dk_1=$dk_1;
  				$nt_1=$nt;
  			} else {
  				$dk_0 = $dk_0;
  				$dk_1 = $new_dk;
  				$nt_1 = $nt;
  			}
  			$nt_0 = $nt_1;
  			$nt_1 = $nt;
  			$i++;

  			}
  		return $new_dk;
	  }
	  
	  function calc_investama($kontribusi,$masa_pembayaran,$masa_perjanjian,$usia,$carbay,$rate_investasi,$dana_kebajikan_ti,$dana_kebajikan_pa,$dana_kebajikan_tpd,$dana_kebajikan_ci,$dana_kebajikan_hcp) 
  	{
	  	$thn = 1;
	  	$flg_thn = 0;
	  	$akum_kontribusi = 0;
	  	$faktor = $this->paymode($carbay);
	  	$tot_kontribusi = $kontribusi*(12/($faktor-1))*$masa_pembayaran;
	  	$t_biaya = $this->getBiaya($masa_pembayaran,$masa_perjanjian);
	  	$ratearray = $this->getRate_investama();

		foreach ($ratearray as $key ) {
			$rate[1][$key['usia']] = $key['ti'];
			$rate[2][$key['usia']] = $key['tpd'];
			$rate[3][$key['usia']] = $key['ci'];
			$rate[4][$key['usia']] = $key['hcp'];
		}		
	  	
	  	$biaya_admin = 15000;
	  	$rate_wakalah = 0.03;

	  	for ($i=1;$i<=$masa_perjanjian*12;$i++) {
	  // 		if ($i<=12) {
			// 	$rate_wakalah = 0.1;  		
			// } else {
			// 	$rate_wakalah = 0.025;  		
			// }
		  	if (((($i-1) % 12)==0)  && ($i<>1)) {
		  		$usia++;
		  		$thn++;
		  		$flg_thn = 1;
		  	} else {$usia =$usia;$thn=$thn;$flg_thn=0;}
		  	$flag = 0;
		  	$vkontribusi = $kontribusi;
		  	if ($i==1) {
		  		$n = $i;  		
		  		$flag = 1;  		
		  	} 
		  	if ($i==$n)
		  	{
		  		$n = $n+($faktor-1);
		  		$flag = 1;  		
		  	} else {
		  		$flag = 0;
		  	}
		  	$vkon = ($vkontribusi*$flag);
		  	if ($i<=($masa_pembayaran*12)) {
		  		$vkon = $vkon;
		  	} else {
		  		$vkon=0;	
		  	}
		  	$rate_pa = 0.4; //Fixed value based on excel
		  	$akum_kontribusi = $akum_kontribusi + ($vkontribusi*$flag);
		  	$waiver = $tot_kontribusi-$akum_kontribusi;
		  	$tbr_ti =($rate[1][$usia]*$dana_kebajikan_ti)/1000;
		  	$tbr_pa = ($rate_pa*$dana_kebajikan_pa)/1000;
		  	$tbr_tpd = ($rate[2][$usia]*$dana_kebajikan_tpd)/1000;
		  	$tbr_ci = ($rate[3][$usia]*$dana_kebajikan_ci)/1000;
		  	$tbr_hcp = ($rate[4][$usia]*$dana_kebajikan_hcp);  	  			  	

		  	$tbr_tot = $tbr_ti+$tbr_pa+$tbr_tpd+$tbr_ci+$tbr_hcp;
		  	$tbr_tot = $tbr_tot *10/100;
		  	if ($akum_kontribusi>$tot_kontribusi) {$akum_kontribusi=$tot_kontribusi;}
		  	$biaya_kontribusi = $kontribusi*$t_biaya[$thn-1]*$flag/100;
		  	if ($i==1) {
		  		$dana_awal = $kontribusi-$tbr_tot-$biaya_kontribusi-$biaya_admin;	
		  		$wakalah_fee = $dana_awal*$rate_wakalah;	
		  		$hasil_investasi = ($dana_awal - $wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
		  		$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
		  	} else {
				$dana_awal = $vkon-$tbr_tot-$biaya_kontribusi-$biaya_admin+$nilai_tunai;
				$wakalah_fee = $dana_awal*$rate_wakalah*$flg_thn;	
		  		$hasil_investasi = ($dana_awal - $wakalah_fee)*(pow((1+$rate_investasi),(1/12))-1);
		  		$nilai_tunai = $dana_awal + $hasil_investasi - $wakalah_fee;
		  	}
			if($carbay=='5'){
				$akum_kontribusi = $kontribusi;
			}
		  	$result[]=array(
		  		'bln'=>$i,
		  		'usia'=>$usia,
		  		'pembayaran'=>$vkon,
		  		'akum_kontribusi'=>$akum_kontribusi,
		  		'dana_kebajikan_ti'=>$dana_kebajikan_ti,
		  		'dana_kebajikan_pa'=>$dana_kebajikan_pa,
		  		'waiver'=>$waiver,
		  		'tbr_ti'=>$tbr_ti,
		  		'tbr_pa'=>$tbr_pa,
		  		'tbr_tpd'=>$tbr_tpd,
		  		'tbr_ci'=>$tbr_ci,
		  		'tbr_hcp'=>$tbr_hcp,
		  		'tbr_tot'=>$tbr_tot,
		  		'biaya_admin'=>$biaya_admin,
		  		'biaya_kontribusi'=>$biaya_kontribusi,
		  		'dana_awal'=>$dana_awal,
		  		'wakalah_fee'=>$wakalah_fee,
		  		'hasil_investasi'=>$hasil_investasi,
		  		'nilai_tunai'=>$nilai_tunai	
		  		);
	  	}	  	
	  	return $result;
	}

	function getRate_investama() 
	{
		$sql = "SELECT usia,ti,tpd,ci,hcp from rate_investama";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getRate_unitlink() 
	{
		$sql = "SELECT * from rate_unitlink";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getBiaya_unitlink()
	{
		$sql = "SELECT * from ujroh_unitlink";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getHasilInvestasi_unitlink()
	{
		$sql = "SELECT * from fund_rate";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function calc_unitlink($kontribusi,$topup_single,$penarikan,$kepesertaan,$dana_kebajikan,$rider,$fund) 
  	{
	  	$thn = 1;
	  	$flg_thn = 0;
	  	$akum_kontribusi = 0;

	  	$nama_polis = $kepesertaan['nama_polis'];
		$usia_polis = $kepesertaan['usia_polis']; 
		$sex_polis = $kepesertaan['sex_polis'];
		$nama_peserta = $kepesertaan['nama_peserta'];
		$usia_peserta = $kepesertaan['usia_peserta'];
		$sex_peserta = $kepesertaan['sex_peserta'];
		$nama_pasangan = $kepesertaan['nama_pasangan'];
		$usia_pasangan = $kepesertaan['usia_pasangan'];
		$sex_pasangan = $kepesertaan['sex_pasangan'];	
		$kelas_pekerjaan_b = $kepesertaan['kelas_pekerjaan_b'];
		$product_code = $kepesertaan['product_code'];

		$kontribusi_dasar = $kontribusi['kontribusi_dasar'];
		$topup_regular = $kontribusi['topup_regular'];
		$carbay = $kontribusi['carbay'];
		$masa_pembayaran = $kontribusi['masa_pembayaran'];

	  	$fund_agresif = $fund['agresif']/100;
	  	$fund_dinamis = $fund['dinamis']/100;
	  	$fund_stabil = $fund['stabil']/100;
	  	$fund_aman = $fund['aman']/100;

	  	$masa_perjanjian = 100-$usia_peserta;
	  	//range rider usia
	  	$min_polis = 21;$max_polis = 65; 
	  	$min_peserta = 1;$max_peserta= 65;
	  	$min_pasangan = 18;$max_pasangan = 65;

	  	//manfaat dasar : DK + PA (<65) + WV (18-65)
	  	//manfaat rider : all (18-65)	  	
	  	$x_carbay = array(1,12,4,2,1,1);

	  	//$faktor = $this->paymode($carbay);
	  	//$tot_kontribusi_dasar = $kontribusi_dasar*(12/($faktor-1))*$masa_pembayaran;
	  	//$bln_perjanjian = $masa_perjanjian*$x_carbay[$carbay];
	  	$bln_perjanjian = $masa_perjanjian*12;
	  	//$bln_bayar = $masa_pembayaran*$x_carbay[$carbay];
	  	$bln_bayar = $masa_pembayaran*12;
	  	if ($carbay=="5") {
	  		$bln_bayar=1;
	  	}
	  	$total_kontribusi_dasar = $kontribusi_dasar*$x_carbay[$carbay]*$masa_pembayaran;
	  	$total_kontribusi_dasar_topup = ($kontribusi_dasar+$topup_regular)*$x_carbay[$carbay]*$masa_pembayaran;
	  	$total_kontribusi = ($kontribusi_dasar+$topup_regular)*$x_carbay[$carbay]*$masa_pembayaran;
	  	

	  	$hasil_investasi = $this->getHasilInvestasi_unitlink();
	  	$rate = $this->getRate_unitlink();
	  	//$biaya = $this->getBiaya_unitlink();
	  	//change to array variable
	  	$rate_biaya_akuisisi[1]=70;$rate_biaya_akuisisi[2]=50;
	  	$rate_biaya_akuisisi[3]=15;$rate_biaya_akuisisi[4]=10;
	  	$rate_biaya_akuisisi[5]=5;
	  	$rate_biaya_topup=5;
	  	$biaya_admin=25000;

	  	foreach ($hasil_investasi as $key => $value) {
	  		$name = trim($value['fund_name']);
	  		$hi_rate[$name]['low'] = $value['low']/100;
	  		$hi_rate[$name]['mid'] = $value['mid']/100;
	  		$hi_rate[$name]['high'] = $value['high']/100;	  		
	  	}
	  	// print_r($fund_agresif);print_r($fund_dinamis);print_r($fund_stabil);print_r($fund_aman);
	  	// print_r($hi_rate);die();
	  	$rate_investasi_low=($fund_agresif*$hi_rate['agresif']['low']+$fund_dinamis*$hi_rate['dinamis']['low']+$fund_stabil*$hi_rate['stabil']['low']+$fund_aman*$hi_rate['aman']['low']);
	  	$rate_investasi_mid=($fund_agresif*$hi_rate['agresif']['mid']+$fund_dinamis*$hi_rate['dinamis']['mid']+$fund_stabil*$hi_rate['stabil']['mid']+$fund_aman*$hi_rate['aman']['mid']);
	  	$rate_investasi_high=($fund_agresif*$hi_rate['agresif']['high']+$fund_dinamis*$hi_rate['dinamis']['high']+$fund_stabil*$hi_rate['stabil']['high']+$fund_aman*$hi_rate['aman']['high']);

	  	//print_r($rate_investasi_low);echo "<>";
	  	// print_r($rate_investasi_mid);echo "<>";
	  	// print_r($rate_investasi_high);echo "<>";
	  	//die();
	  	// Ambil rate dari table ke array $usia
	  	foreach ($rate as $key=>$value) {
	  		$usia = $value['usia'];
	  		$rate_tbr[$usia]['tim'] = $value['TIM'];
	  		$rate_tbr[$usia]['tiw'] = $value['TIW'];
	  		$rate_tbr[$usia]['wv'] = $value['WV'];
	  		$rate_tbr[$usia]['ci'] = $value['CI'];
	  		$rate_tbr[$usia]['tpd'] = $value['TPD'];
	  		$rate_tbr[$usia]['hcp'] = $value['HCP'];
	  		$rate_tbr[$usia]['hp'] = $value['HP'];
	  		$rate_tbr[$usia]['pab1'] = $value['pab1'];
	  		$rate_tbr[$usia]['pab2'] = $value['pab2'];
	  		$rate_tbr[$usia]['pab3'] = $value['pab3'];
	  		$rate_tbr[$usia]['pab4'] = $value['pab4'];
	  		$rate_tbr[$usia]['par1'] = $value['par1'];
	  		$rate_tbr[$usia]['par2'] = $value['par2'];
	  		$rate_tbr[$usia]['par3'] = $value['par3'];
	  		$rate_tbr[$usia]['par4'] = $value['par4'];
	  	}

	  	//init process

	  	$akum_total_kontribusi = 0;
	  	$akum_kontribusi_dasar = 0;
	  	$thn_polis = 1;
	  	if ($sex_peserta=="P") {
	  			$var_ti = 'tim';	  				
	  		} else {
	  			$var_ti = 'tiw';		  			
	  	}
	  	if ($sex_polis=="P") {
	  			$var_rtip = 'tim';
	  			$var_rtis = 'tiw';
	  		} else {
	  			$var_rtip = 'tiw';
	  			$var_rtis = 'tim';
	  	}
	  	$kelas_pekerjaan_r = 1;
	  	$var_pkrj_b = "pab".$kelas_pekerjaan_b;
	  	$var_pkrj_r = "par".$kelas_pekerjaan_r;
	  	$benefit_waiver = $total_kontribusi_dasar;
	  	$benefit_waiver_rider = $total_kontribusi_dasar_topup;

	  	//iterasi dari bln ke-1 sampai dengan akhir bulan perjanjian
	  	$arr_carbay = array(1,1,3,6,12,1);
	  	$faktor_carbay = $arr_carbay[$carbay];
	  	$nilai_tunai_low=0;
	  	$nilai_tunai_mid=0;
	  	$nilai_tunai_high=0;

	  	$total_manfaat_low=0;
	  	$total_manfaat_mid=0;
	  	$total_manfaat_high=0;

	  	$total_manfaat_non_low=0;
	  	$total_manfaat_non_mid=0;
	  	$total_manfaat_non_high=0;
	  	$tabarru_dasar = 0;
	  	$tabarru_rider = 0;

	  	for ($i=1;$i<=$bln_perjanjian;$i++) {
	  		$v_tot_kontribusi = 0;
	  		$kl_dasar =0; $kl_topup=0;
	  		if ($i<=$bln_bayar) {
	  			if (($i==1) || ($carbay=="1")){
	  				$akum_total_kontribusi = $akum_total_kontribusi + $kontribusi_dasar+$topup_regular;
	  				$akum_kontribusi_dasar = $akum_kontribusi_dasar + $kontribusi_dasar;
	  				$benefit_waiver = $benefit_waiver - $kontribusi_dasar;
	  				$benefit_waiver_rider = $benefit_waiver_rider - $kontribusi_dasar - $topup_regular;
	  				if($benefit_waiver<2000000000){
	  					$benefit_waiver = $benefit_waiver;
	  					$benefit_waiver_rider = $benefit_waiver_rider;
	  				}else{
	  					$benefit_waiver = 2000000000;
	  					$benefit_waiver_rider = 2000000000;
	  				}

					$kl_dasar = $kontribusi_dasar;  	
					$kl_topup = $topup_regular;
					$v_tot_kontribusi = $kl_dasar+$kl_topup;
	  			} else {
	  				if ((($i-1) % $faktor_carbay)==0) {
	  					$akum_total_kontribusi = $akum_total_kontribusi + $kontribusi_dasar+$topup_regular;
	  					$akum_kontribusi_dasar = $akum_kontribusi_dasar + $kontribusi_dasar;
	  					$benefit_waiver = $benefit_waiver - $kontribusi_dasar;
	  					$benefit_waiver_rider = $benefit_waiver_rider - $kontribusi_dasar - $topup_regular;
	  					if($benefit_waiver<2000000000){
		  					$benefit_waiver = $benefit_waiver;
		  					$benefit_waiver_rider = $benefit_waiver_rider;
		  				}else{
		  					$benefit_waiver = 2000000000;
		  					$benefit_waiver_rider = 2000000000;
		  				}

	  					$kl_dasar = $kontribusi_dasar;  	
						$kl_topup = $topup_regular;
						$v_tot_kontribusi = $kl_dasar+$kl_topup;
	  				}
	  			}
	  		}
	  		if ($i>$bln_bayar) {
	  			$benefit_waiver=0;
	  		}
	  		
	  		//tabarru' dasar = TI peserta + PA peserta + WV peserta
	  		

	  		$tabarru_dasar_ti = round((($dana_kebajikan * $rate_tbr[$usia_peserta][$var_ti])/1000),-3)/12;		  		
	  		$tabarru_dasar_pa = round((($dana_kebajikan*$rate_tbr[$usia_peserta][$var_pkrj_b])/1000),-3)/12;
	  		$tabarru_dasar_wv = round((($benefit_waiver*$rate_tbr[$usia_peserta]['wv'])/1000),-3)/12;
	  		$tabarru_dasar = $tabarru_dasar_ti+$tabarru_dasar_pa+$tabarru_dasar_wv;
			$nilai_tunai = 0;
			$tabarru_rider_sb = 0;
	  		//rider TI untuk peserta
	  		if ($usia_peserta<=65 || $usia_peserta>=18) {
		  		$rider_ti=$rider['rider_ti'];
		  		$tabarru_rider_ti = round((($rider_ti*$rate_tbr[$usia_peserta][$var_ti])/1000),-3)/12;
		  		$rider_pa=$rider['rider_pa'];
				//$tabarru_rider_pa = round((($rider_pa*$rate_tbr[$usia_peserta][$var_pkrj_r])/1000),-3)/12;
				// echo"<pre>";print_r($tabarru_rider_pa);die();
				$tabarru_rider_pa = 0;
		  		$rider_tpd=$rider['rider_tpd'];
				  // $tabarru_rider_tpd = round((($rider_tpd*$rate_tbr[$usia_peserta]['tpd'])/1000),-3)/12;	
				  $tabarru_rider_tpd = 0;
		  		$rider_ci=$rider['rider_ci'];
				  // $tabarru_rider_ci = round((($rider_ci*$rate_tbr[$usia_peserta]['ci'])/1000),-3)/12;
				  $tabarru_rider_ci = 0;
		  		$rider_hp = $rider['rider_hp'];
				  // $tabarru_rider_hp = round((($rider_hp*$rate_tbr[$usia_peserta]['hp'])/1000),-3)/12;
				  $tabarru_rider_hp =0;
		  		$rider_hcp = $rider['rider_hcp'];
		  		$tabarru_rider_hcp = round((($rider_hcp*$rate_tbr[$usia_peserta]['hcp'])/1000),3)/12;
	  		}
	  		if ($usia_polis<=65 && $usia_polis>=18) {	
	  			$flg_pdp = $rider['flg_pdp'];	  		
		  		$tabarru_rider_pdp = round((($benefit_waiver_rider*$rate_tbr[$usia_polis][$var_rtip]*$flg_pdp)/1000),-3)/12;//usia polis 	
		  		$flg_pb = $rider['flg_pb'];
		  		$tabarru_rider_pb = round((($benefit_waiver_rider*$rate_tbr[$usia_polis]['wv']*$flg_pb)/1000),-3)/12; //usia polis
	  		}
	  		if ($usia_pasangan<=65 && $usia_pasangan>=18) {
	  			$flg_sdp = $rider['flg_sdp'];
		  		$tabarru_rider_sdp = round((($benefit_waiver_rider*$rate_tbr[$usia_pasangan][$var_rtis]*$flg_sdp)/1000),-3)/12; //usia pasangan
		  		$flg_sb = $rider['flg_sb'];
		  		$tabarru_rider_sb = round((($benefit_waiver_rider*$rate_tbr[$usia_pasangan]['wv']*$flg_sb)/1000),-3)/12; //usia pasangan
			}

			//handle error undefiend variable
			$tabarru_rider_pdp=0;
			$tabarru_rider_pb=0;

			
	  		$tabarru_rider = $tabarru_rider_ti+$tabarru_rider_pa+$tabarru_rider_tpd+$tabarru_rider_ci+$tabarru_rider_hcp+$tabarru_rider_hp+$tabarru_rider_pdp+$tabarru_rider_pb+$tabarru_rider_sdp+$tabarru_rider_sb;
	  		//increase usia dan thn polis
	  		if ((($i-1) % 12 == 0) && $i>11) {
	  			$usia_peserta++;
	  			$usia_polis++;
	  			$usia_pasangan++;
	  			$thn_polis++;
	  		}
	  		//biaya akuisisi, biaya topup, biaya admin
	  		if (isset($rate_biaya_akuisisi[$thn_polis])) {
	  			$biaya_akuisisi = $kl_dasar*$rate_biaya_akuisisi[$thn_polis]/100;
		  		} else {
		  		$biaya_akuisisi = 0;
		  	}
		  	$biaya_topup = $kl_topup*$rate_biaya_topup/100;
	  		$total_biaya = $biaya_admin+$biaya_akuisisi+$biaya_topup;

	  		$dana_investasi_low = $v_tot_kontribusi-$total_biaya-$tabarru_dasar-$tabarru_rider+$nilai_tunai_low;
	  		$dana_investasi_mid = $v_tot_kontribusi-$total_biaya-$tabarru_dasar-$tabarru_rider+$nilai_tunai_mid;
	  		$dana_investasi_high = $v_tot_kontribusi-$total_biaya-$tabarru_dasar-$tabarru_rider+$nilai_tunai_high;
	  		$hasil_investasi_low = $dana_investasi_low*(pow((1+$rate_investasi_low),(1/12))-1);
	  		$nilai_tunai_low = $dana_investasi_low+$hasil_investasi_low;
	  		$hasil_investasi_mid = $dana_investasi_mid*(pow((1+$rate_investasi_mid),(1/12))-1);
	  		$nilai_tunai_mid = $dana_investasi_mid+$hasil_investasi_mid;
	  		$hasil_investasi_high = $dana_investasi_high*(pow((1+$rate_investasi_high),(1/12))-1);
	  		$nilai_tunai_high = $dana_investasi_high+$hasil_investasi_high;

	  		$total_manfaat_low  = round(($dana_kebajikan + $nilai_tunai_low),-3)/1000;
	  		$total_manfaat_mid  = round(($dana_kebajikan + $nilai_tunai_mid),-3)/1000;
	  		$total_manfaat_high = round(($dana_kebajikan + $nilai_tunai_high),-3)/1000;

	  		$total_manfaat_non_low = round(($dana_kebajikan+$dana_kebajikan + $nilai_tunai_low),-3)/1000;
	  		$total_manfaat_non_mid = round(($dana_kebajikan+$dana_kebajikan + $nilai_tunai_mid),-3)/1000;
	  		$total_manfaat_non_high = round(($dana_kebajikan+$dana_kebajikan + $nilai_tunai_high),-3)/1000;

	  		$arr_res[] = array('akum_total_kontribusi'=>$akum_total_kontribusi,
	  				'usia_peserta' => $usia_peserta,
	  				'akum_total_kontribusi' => $akum_total_kontribusi,
	  				'benefit_waiver'=>$benefit_waiver,
	  				'tbr_ti' => $tabarru_dasar_ti,
	  				'tbr_pa' => $tabarru_dasar_pa,
	  				'tbr_wv' => $tabarru_dasar_wv,
	  				'tabarru_dasar'=>$tabarru_dasar,
	  				'dana_kebajikan'=>$dana_kebajikan,
	  				'rate_tbr_ti' => $rate_tbr[$usia_peserta][$var_ti],
	  				'tbr_ti' => $tabarru_dasar_ti,
	  				'rate_tbr_pa' => $rate_tbr[$usia_peserta][$var_pkrj_b],
	  				'tbr_pa' => $tabarru_dasar_pa,
	  				'rate_tbr_wv' => $rate_tbr[$usia_peserta]['wv'],
	  				'tbr_wv' => $tabarru_dasar_wv,
	  				'ujroh_regular' => $rate_biaya_akuisisi,
	  				'ujroh_toptup' => $rate_biaya_topup,
	  				'ujroh_bulanan' => $biaya_admin,
	  				'biaya_akuisisi' => $biaya_akuisisi,
	  				'biaya_topup' => $biaya_topup,
	  				'biaya_admin' => $thn_polis,
	  				'tabarru_rider_ti' =>$tabarru_rider_ti,
	  				'tabarru_rider_pa' =>$tabarru_rider_pa,
	  				'tabarru_rider_tpd' =>$tabarru_rider_tpd,
	  				'tabarru_rider_ci' =>$tabarru_rider_ci,
	  				'tabarru_rider_hp' =>$tabarru_rider_hp,
	  				'tabarru_rider_hcp' =>$tabarru_rider_hcp,
	  				'tabarru_rider_pdp' =>$tabarru_rider_pdp,
	  				'tabarru_rider_pb' =>$tabarru_rider_pb,
	  				'tabarru_rider_sdp' =>$tabarru_rider_sdp,
	  				'tabarru_rider_sb' =>$tabarru_rider_sb,
	  				'tabarru_rider' => $tabarru_rider,
	  				'nilai_tunai_low' => round($nilai_tunai_low,-3)/1000,
	  				'nilai_tunai_mid' => round($nilai_tunai_mid,-3)/1000,
	  				'nilai_tunai_high' => round($nilai_tunai_high,-3)/1000,
	  				'total_manfaat_low' => $total_manfaat_low,
	  				'total_manfaat_mid' => $total_manfaat_mid,
	  				'total_manfaat_high' => $total_manfaat_high,
	  				'total_manfaat_non_low' => $total_manfaat_non_low,
	  				'total_manfaat_non_mid' => $total_manfaat_non_mid,
	  				'total_manfaat_non_high' => $total_manfaat_non_high
	  				);
	  	}
	  	
	  	return $arr_res;
	}

	public function to_numeric($value)
	{
		$value = str_replace('.', '', $value);
		$result = str_replace(',', '', $value);

		return $result;
	}

	public function getRateWakalahFee($kontribusi)
	{
		if($kontribusi>0 && $kontribusi<=100000000) {
			return 3;
		}else if($kontribusi>100000000 && $kontribusi<=200000000) {
			return 2;
		}else if($kontribusi>200000000) {
			return 1;
		}
	}

}