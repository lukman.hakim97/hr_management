<div class="card" id="form-list">
    <div class="card-header">
        <h4><i class="fa fa-list"></i> Form Bank </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div class="buttons">
                    <button class="btn btn-info btn-icon" id="btn-create">
                        <i class="fa fa-plus"></i>
                        Create
                    </button>
                    <button class="btn btn-warning btn-icon" id="btn-edit">
                        <i class="fa fa-edit"></i>
                        Edit
                    </button>
                    <button class="btn btn-danger btn-icon" id="btn-delete">
                        <i class="fa fa-trash"></i>
                        Delete
                    </button>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="input-group">
                    <input type="text" class="form-control border-radius-0" placeholder="Search" id="keyword" name="keyword">
                    <div class="input-group-append">
                        <button class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                            <i class="fas fa-search"></i> Search
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-sm-12 col-md-12 col-lg-12 wrapper-jqGrid">
                <table id="jqGridData"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
</div>

<!-- Form Add -->
<div class="card" id="form-add" style="display: none;">
	<div class="card-header">
        <h4><i class="fa fa-plus"></i> Form Create Bank</h4>
	</div>
	<div class="card-body">
		<form id="form-save" class="form-horizontal" role="form">
        	<div class="alert alert-danger show fade" style="display: none;">
                <div class="alert-body">
                    <button class="close close-alert" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    You have some form errors. Please check below. 
                </div>
            </div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="bank_code">Bank Code</label>
				<div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="bank_code" name="bank_code" placeholder="Bank Code" required="" maxlength="10"/>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="bank_name">Bank Name</label>
				<div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name" required="" maxlength="50"/>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="bank_code_transfer">Bank Code Transfer</label>
				<div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="bank_code_transfer" name="bank_code_transfer" placeholder="Bank Code Transfer" required="" maxlength="5"/>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
				<div class="col-sm-12 col-md-7">
					<button type="submit" class="btn btn-primary btn-icon" id="btn-save">
						<i class="fa fa-save"></i>
						Save
					</button>
                    <button type="button" class="btn btn-secondary btn-back btn-icon">
                        <i class="fa fa-arrow-left"></i>
                        Back
                    </button>
				</div>
			</div>
		</form>
    </div>
</div>

<!-- Form Edit -->
<div class="card" id="form-edit" style="display: none;">
    <div class="card-header">
        <h4><i class="fa fa-edit"></i> Form Edit Setup Bank</h4>
    </div>
    <div class="card-body">
        <form id="form-update" class="form-horizontal" role="form">
            <input type="hidden" id="id_bank" name="id_bank">
            <div class="alert alert-danger show fade" style="display: none;">
                <div class="alert-body">
                    <button class="close close-alert" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    You have some form errors. Please check below. 
                </div>
            </div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="bank_code">Bank Code</label>
				<div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="bank_code" name="bank_code" placeholder="Bank Code" required="" maxlength="10"/>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="bank_name">Bank Name</label>
				<div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name" required="" maxlength="50"/>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="bank_code_transfer">Bank Code Transfer</label>
				<div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="bank_code_transfer" name="bank_code_transfer" placeholder="Bank Code Transfer" required="" maxlength="5"/>
				</div>
			</div>
            <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                <div class="col-sm-12 col-md-7">
                    <button type="submit" class="btn btn-primary btn-icon" id="btn-update">
                        <i class="fa fa-save"></i>
                        Save
                    </button>
                    <button type="button" class="btn btn-secondary btn-back btn-icon">
                        <i class="fa fa-arrow-left"></i>
                        Back
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

        var AlertError = $(".alert-danger");
        var jqGridData = $("#jqGridData");
        var form_update = $("#form-update");

        $("#jqGridData").jqGrid({
            url: site_url + 'setup_bank/jqgrid_bank',
            datatype: "json",
            mtype: "GET",
            postData: {
                keyword: function() {
                    return $("#keyword").val()
                }
            },
            colModel: [
                { label: 'ID', name: 'id', width: 10, hidden: true },
                { label: 'Bank Code', name: 'bank_code', width: 100 },
                { label: 'Bank Name', name: 'bank_name', width: 200 },
                { label: 'Bank Code Transfer', name: 'bank_code_transfer', width: 100, align: 'center' }
            ],
            viewrecords: true,
            autowidth: true,
            height: 350,
            rowNum: 20,
            rownumbers: true,
            shrinkToFit: false,
            sortname: "bank_code",
            sortorder: "desc",
            multiselect: false,
            pager: "#jqGridPager",
            grouping: false
        });

        $("#btn-create", "#form-list").click(function(event) {
            event.preventDefault();
            $("#form-list").hide();
            $("#form-add").show();
        });

        $("#form-save").validate({
            rules: {
                bank_code: { 
                    required:true 
                },
                bank_name: {
                    required: true
                },
                bank_code_transfer: {
                    required: true
                },
            },
            errorElement: "em",
            invalidHandler: function (event, validator) { //display error alert on form submit
                AlertError.show();
            },
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                // error.addClass("help-block");
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                // $(element).addClass("is-valid").removeClass("is-valid");
            },
            highlight: function (element, errorClass, validClass) {
                if (validClass) {
                    $(element).addClass("is-invalid");
                }
            },
            unhighlight: function (element, errorClass, validClass) {               
                if (validClass) {
                    $(element).removeClass("is-invalid").addClass("is-valid");
                }
            },
            submitHandler: function (form) {

                $("#btn-save", "#form-save").attr('disable', true);
                var data = $("#form-save").serialize();
                $.ajax({
                    url: site_url + 'setup_bank/do_save_bank',
                    type: 'POST',
                    timeout: 3000,
                    dataType: 'json',
                    data: data
                })
                .done(function(response) {
                    console.log(response, "done");
                    if (response.success==true) {
                        APP.swalSuccess(response.message);
                    }else{
                        APP.swalError(response.message);
                    }
                })
                .fail(function(response) {
                    APP.swalError();    
                })
                .always(function() {
                    $("#btn-save", "#form-save").attr('disable', false);
                    resetBack();
                });                

            }
        });

        $("#btn-edit", "#form-list").click(function(event) {
            event.preventDefault();
            selrow=jqGridData.jqGrid('getGridParam','selrow');
            data=jqGridData.jqGrid('getRowData',selrow);
            id = data.id;
            if(selrow){

                $.ajax({
                    type:"POST",dataType:"json",data:{id:id},async:false,
                    url:site_url+'setup_bank/get_data_bank_by_id',
                    success:function(response) {
                        $("#id_bank",form_update).val(response.id);
                        $("#bank_code",form_update).val(response.bank_code);
                        $("#bank_name",form_update).val(response.bank_name);
                        $("#bank_code_transfer",form_update).val(response.bank_code_transfer);
                        $("#form-edit").show();
                        $("#form-list").hide();
                    },
                    error: function(response){
                        Metronic.ErrorAlert("An error occurred, please try again later")
                        // write log
                        url='';
                        status=response.status;
                        statusText=response.statusText;
                        responseText=response.responseText;
                        Metronic.write_error_log(url,status,statusText,responseText);
                    }
                })
            }else{
                APP.swalWarning("Please select a row");
            }  
        });

        $("#form-update").validate({
            rules: {
                bank_code: { 
                    required:true 
                },
                bank_name: {
                    required: true
                },
                bank_code_transfer: {
                    required: true
                },
            },
            errorElement: "em",
            invalidHandler: function (event, validator) { //display error alert on form submit
                AlertError.show();
            },
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                // error.addClass("help-block");
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                // $(element).addClass("is-valid").removeClass("is-valid");
            },
            highlight: function (element, errorClass, validClass) {
                if (validClass) {
                    $(element).addClass("is-invalid");
                }
            },
            unhighlight: function (element, errorClass, validClass) {               
                if (validClass) {
                    $(element).removeClass("is-invalid").addClass("is-valid");
                }
            },
            submitHandler: function (form) {

                $("#btn-update", "#form-update").attr('disable', true);
                var data = $("#form-update").serialize();
                $.ajax({
                    url: site_url + 'setup_bank/do_update_bank',
                    type: 'POST',
                    timeout: 3000,
                    dataType: 'json',
                    data: data
                })
                .done(function(response) {
                    console.log(response, "done");
                    if (response.success==true) {
                        APP.swalSuccess(response.message);
                    }else{
                        APP.swalError(response.message);
                    }
                })
                .fail(function(response) {
                    APP.swalError();    
                })
                .always(function() {
                    $("#btn-update", "#form-update").attr('disable', false);
                    resetBack();
                });                

            }
        });

        $("#btn-search", "#form-list").click(function(event) {
            event.preventDefault();
            $("#jqGridData").trigger('reloadGrid');
        });

        $("#btn-delete", "#form-list").click(function(event) {
            event.preventDefault();

            selrow=jqGridData.jqGrid('getGridParam','selrow');
            data=jqGridData.jqGrid('getRowData',selrow);
            id = data.id;
            if(selrow){
                swal({
                    title: "Are you sure?",
                    text: 'To Deleted this Data ! ',
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type:"POST",dataType:"json",data:{id:id},async:false,
                            url:site_url+'setup_bank/do_delete_bank',
                            success:function(response) {
                                if(response.success==true){
                                    swal("Success !", {
                                        icon: "success",
                                    });
                                    jqGridData.trigger('reloadGrid');
                                }else{
                                    APP.swalError();
                                }
                            },
                            error: function(response){
                                Metronic.ErrorAlert("An error occurred, please try again later")
                                // write log
                                url='';
                                status=response.status;
                                statusText=response.statusText;
                                responseText=response.responseText;
                                Metronic.write_error_log(url,status,statusText,responseText);
                            }
                        })
                    } else {
                        swal("Process Cancel !");
                    }
                });
            }else{
                APP.swalWarning("Please select a row");
            }  
        });

        function resetBack() {
            $("#form-list").show();
            $("#form-add").hide();
            $("#form-edit").hide();

            $("#form-save").validate().resetForm();
            $("#form-update").validate().resetForm();
            $("#jqGridData").trigger('reloadGrid');
            AlertError.hide();
        }

        $(document).on("click", ".btn-back", function(event) {
            event.preventDefault();
            resetBack();
        });

        $(document).on("click", ".close-alert", function(event) {
            $(".alert-danger").hide();
        });

    });

</script>
