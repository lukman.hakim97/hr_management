<div class="row">
	<div class="col-lg-8">
		<div class="card">
			<div class="card-header">
				<h4>News</h4>
			</div>
			<div class="card-body">
				<div class="owl-carousel owl-theme owl-loaded owl-drag" id="products-carousel">
					<div class="owl-stage-outer">
						<div class="owl-stage" >
							<div class="owl-item">
								<div>
									<div class="product-item">
										<div class="">
											<img alt="image" src="<?php echo base_url('assets/img/news/Banner.jpg'); ?>" class="img-fluid">
										</div>
										<div class="product-details">
											<div class="product-name">Auto</div>
											<div class="text-muted text-small">Lorem ipsum lorem lorem</div>
											<div class="product-cta">
												<a href="#" class="btn btn-primary">DETAIL</a>
											</div>
										</div>  
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div>
									<div class="product-item">
										<div class="">
											<img alt="image" src="<?php echo base_url('assets/img/news/Banner(2).jpg'); ?>" class="img-fluid">
										</div>
										<div class="product-details">
											<div class="product-name">Fire</div>
											<div class="text-muted text-small">Lorem ipsum lorem lorem</div>
											<div class="product-cta">
												<a href="#" class="btn btn-primary btn-xs">DETAIL</a>
											</div>
										</div>  
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div>
									<div class="product-item">
										<div class="">
											<img alt="image" src="<?php echo base_url('assets/img/news/Banner(1).jpg'); ?>" class="img-fluid">
										</div>
										<div class="product-details">
											<div class="product-name">Travel</div>
											<div class="text-muted text-small">Lorem ipsum lorem lorem</div>
											<div class="product-cta">
												<a href="#" class="btn btn-primary">DETAIL</a>
											</div>
										</div>  
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="owl-nav disabled">
						<div class="owl-prev">prev</div>
						<div class="owl-next">next</div>
					</div>
					<div class="owl-dots disabled">
						<div class="owl-dot active">
							<span></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="card gradient-bottom">
			<div class="card-header">
				<h4>Top Agen</h4>
				<div class="card-header-action">
					<a href="#" class="btn btn-danger">Your Position: 3</a>
				</div>
			</div>
			<div class="card-body" id="top-5-scroll">
				<ul class="list-unstyled list-unstyled-border">
					<li class="media">
						<img class="mr-3 rounded-circle" width="50" src="<?php echo base_url() ?>assets/img/avatar/avatar-1.png" alt="avatar">
						<div class="media-body">
							<div class="float-right text-primary">1</div>
							<div class="media-title">Farhan A Mujib</div>
							<span class="text-small text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</span>
						</div>
					</li>
					<li class="media">
						<img class="mr-3 rounded-circle" width="50" src="<?php echo base_url() ?>assets/img/avatar/avatar-2.png" alt="avatar">
						<div class="media-body">
							<div class="float-right">2</div>
							<div class="media-title">Ujang Maman</div>
							<span class="text-small text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</span>
						</div>
					</li>
					<li class="media">
						<img class="mr-3 rounded-circle" width="50" src="<?php echo base_url() ?>assets/img/avatar/avatar-3.png" alt="avatar">
						<div class="media-body">
							<div class="float-right">3</div>
							<div class="media-title">Rizal Fakhri</div>
							<span class="text-small text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</span>
						</div>
					</li>
					<li class="media">
						<img class="mr-3 rounded-circle" width="50" src="<?php echo base_url() ?>assets/img/avatar/avatar-4.png" alt="avatar">
						<div class="media-body">
							<div class="float-right">4</div>
							<div class="media-title">Alfa Zulkarnain</div>
							<span class="text-small text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</span>
						</div>
					</li>
					<li class="media">
						<img class="mr-3 rounded-circle" width="50" src="<?php echo base_url() ?>assets/img/avatar/avatar-4.png" alt="avatar">
						<div class="media-body">
							<div class="float-right">5</div>
							<div class="media-title">Alfa Zulkarnain</div>
							<span class="text-small text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="card-footer pt-3 d-flex justify-content-center">
			</div>
		</div>
	</div>
</div>

<br>
<?php 
    // echo "<pre>";
    // print_r($this->session->userdata);
?>

<script type="text/javascript">
	$("#products-carousel").owlCarousel({
		items: 1,
		margin: 10,
		autoplay: true,
		autoplayTimeout: 5000,
		loop: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 1
			},
			1200: {
				items: 1
			}
		}
	});
</script>