<div class="card" id="form-list">
    <div class="card-header">
        <h4><i class="fa fa-list"></i> Form Master Treaty </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div class="buttons">
                    <button class="btn btn-info btn-icon" id="btn-create">
                        <i class="fa fa-plus"></i>
                        Create
                    </button>
                    <button class="btn btn-warning btn-icon" id="btn-edit">
                        <i class="fa fa-edit"></i>
                        Edit
                    </button>
                    <button class="btn btn-danger btn-icon" id="btn-delete">
                        <i class="fa fa-trash"></i>
                        Delete
                    </button>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="input-group">
                    <input type="text" class="form-control border-radius-0" placeholder="Search" id="keyword" name="keyword">
                    <div class="input-group-append">
                        <button class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                            <i class="fas fa-search"></i> Search
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-sm-12 col-md-12 col-lg-12 wrapper-jqGrid">
                <table id="jqGridData"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
</div>

<!-- Form Add -->
<div class="card" id="form-add" style="display: none;">
	<div class="card-header">
        <h4><i class="fa fa-plus"></i> Form Create Master Treaty</h4>
	</div>
	<div class="card-body">
		<form id="form-save" class="form-horizontal" role="form">
        	<div class="alert alert-danger show fade" style="display: none;">
                <div class="alert-body">
                    <button class="close close-alert" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    You have some form errors. Please check below. 
                </div>
            </div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="code">Code</label>
				<div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="code" name="code" placeholder="0" required="" />
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="start_date">Start Date</label>
				<div class="col-sm-12 col-md-2">
                    <input type="text" class="form-control maskdate datepicker" id="start_date" name="start_date" placeholder="dd/mm/YYYY" required="" maxlength="10"/>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="end_date">End Date</label>
				<div class="col-sm-12 col-md-2">
                    <input type="text" class="form-control maskdate datepicker" id="end_date" name="end_date" placeholder="dd/mm/YYYY" required="" maxlength="10"/>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="class_code">Class Code</label>
				<div class="col-sm-12 col-md-7">
                    <select name="class_code" id="class_code" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0"><?php echo strtoupper('m')?>otor</option>
                        <option value="0"><?php echo strtoupper('f')?>ire</option>
                        <option value="0"><?php echo strtoupper('b')?>ond</option>
                        <option value="0"><?php echo strtoupper('m')?>arine cargo</option>
                        <option value="0"><?php echo strtoupper('e')?>nginering</option>
                        <option value="0"><?php echo strtoupper('m')?>arine hull</option>
                        <option value="0"><?php echo strtoupper('l')?>iability</option>
                        <option value="0"><?php echo strtoupper('m')?>iscellaneous accident</option>
                        <option value="0"><?php echo strtoupper('p')?>ersonal accident</option>
                        <option value="0"><?php echo strtoupper('w')?>orkman compensation</option>
                    </select>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="minimum_limit">Minimum Limit</label>
				<div class="col-sm-12 col-md-7">
                    <div class="input-group">
                        <div class="input-group-append">
                            <span class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                                Rp
                            </span>
                        </div>
                        <input type="text" class="form-control currency" id="minimum_limit" name="minimum_limit" placeholder="0" required="" />
                    </div>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="maximum_limit">Maximum Limit</label>
				<div class="col-sm-12 col-md-7">
                    <div class="input-group">
                        <div class="input-group-append">
                            <span class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                                Rp
                            </span>
                        </div>
                        <input type="text" class="form-control currency" id="maximum_limit" name="maximum_limit" placeholder="0" required="" />
                    </div>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="reinsurance_code">Reinsurance</label>
				<div class="col-sm-12 col-md-7">
                    <select name="reinsurance_code" id="reinsurance_code" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0">REINDO</option>
                        <option value="0">MAREIN</option>
                    </select>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="reinsurance_share">Reinsurance Share</label>
				<div class="col-sm-12 col-md-2">
                    <div class="input-group">
                        <input type="text" class="form-control" id="reinsurance_share" name="reinsurance_share" placeholder="0" required="" maxlength="3" />
                        <div class="input-group-append">
                            <span class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                                %
                            </span>
                        </div>
                    </div>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="reinsurance_commision">Reinsurance Commision</label>
				<div class="col-sm-12 col-md-2">
                    <div class="input-group">
                        <input type="text" class="form-control" id="reinsurance_commision" name="reinsurance_commision" placeholder="0" required="" maxlength="3" />
                        <div class="input-group-append">
                            <span class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                                %
                            </span>
                        </div>
                    </div>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="treaty_type">Treaty Type</label>
				<div class="col-sm-12 col-md-7">
                    <select name="treaty_type" id="treaty_type" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0">Surplus</option>
                        <option value="0">Facultative</option>
                        <option value="0">Quota Share</option>
                        <option value="0">Retention</option>
                        <option value="0">XOL</option>
                    </select>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="bordereaux">Bordereaux</label>
				<div class="col-sm-12 col-md-7">
                    <select name="bordereaux" id="bordereaux" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0">Monthly</option>
                        <option value="0">Quarterly</option>
                        <option value="0">Halfyearly</option>
                        <option value="0">Yearly</option>
                        <option value="0">Non Bordereaux</option>
                    </select>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
				<div class="col-sm-12 col-md-7">
					<button type="submit" class="btn btn-primary btn-icon" id="btn-save">
						<i class="fa fa-save"></i>
						Save
					</button>
                    <button type="button" class="btn btn-secondary btn-back btn-icon">
                        <i class="fa fa-arrow-left"></i>
                        Back
                    </button>
				</div>
			</div>
		</form>
    </div>
</div>

<!-- Form Edit -->
<div class="card" id="form-edit" style="display: none;">
    <div class="card-header">
        <h4><i class="fa fa-edit"></i> Form Edit Master Treaty</h4>
    </div>
    <div class="card-body">
        <form id="form-update" class="form-horizontal" role="form">
            <div class="alert alert-danger show fade" style="display: none;">
                <div class="alert-body">
                    <button class="close close-alert" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    You have some form errors. Please check below. 
                </div>
            </div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="code">Code</label>
				<div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="code" name="code" placeholder="0" required="" />
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="start_date">Start Date</label>
				<div class="col-sm-12 col-md-2">
                    <input type="text" class="form-control maskdate datepicker" id="start_date" name="start_date" placeholder="dd/mm/YYYY" required="" maxlength="10"/>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="end_date">End Date</label>
				<div class="col-sm-12 col-md-2">
                    <input type="text" class="form-control maskdate datepicker" id="end_date" name="end_date" placeholder="dd/mm/YYYY" required="" maxlength="10"/>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="class_code">Class Code</label>
				<div class="col-sm-12 col-md-7">
                    <select name="class_code" id="class_code" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0"><?php echo strtoupper('m')?>otor</option>
                        <option value="0"><?php echo strtoupper('f')?>ire</option>
                        <option value="0"><?php echo strtoupper('b')?>ond</option>
                        <option value="0"><?php echo strtoupper('m')?>arine cargo</option>
                        <option value="0"><?php echo strtoupper('e')?>nginering</option>
                        <option value="0"><?php echo strtoupper('m')?>arine hull</option>
                        <option value="0"><?php echo strtoupper('l')?>iability</option>
                        <option value="0"><?php echo strtoupper('m')?>iscellaneous accident</option>
                        <option value="0"><?php echo strtoupper('p')?>ersonal accident</option>
                        <option value="0"><?php echo strtoupper('w')?>orkman compensation</option>
                    </select>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="minimum_limit">Minimum Limit</label>
				<div class="col-sm-12 col-md-7">
                    <div class="input-group">
                        <div class="input-group-append">
                            <span class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                                Rp
                            </span>
                        </div>
                        <input type="text" class="form-control currency" id="minimum_limit" name="minimum_limit" placeholder="0" required="" />
                    </div>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="maximum_limit">Maximum Limit</label>
				<div class="col-sm-12 col-md-7">
                    <div class="input-group">
                        <div class="input-group-append">
                            <span class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                                Rp
                            </span>
                        </div>
                        <input type="text" class="form-control currency" id="maximum_limit" name="maximum_limit" placeholder="0" required="" />
                    </div>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="reinsurance_code">Reinsurance</label>
				<div class="col-sm-12 col-md-7">
                    <select name="reinsurance_code" id="reinsurance_code" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0">REINDO</option>
                        <option value="0">MAREIN</option>
                    </select>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="reinsurance_share">Reinsurance Share</label>
				<div class="col-sm-12 col-md-2">
                    <div class="input-group">
                        <input type="text" class="form-control" id="reinsurance_share" name="reinsurance_share" placeholder="0" required="" maxlength="3" />
                        <div class="input-group-append">
                            <span class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                                %
                            </span>
                        </div>
                    </div>
				</div>
			</div>
            <div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="reinsurance_commision">Reinsurance Commision</label>
				<div class="col-sm-12 col-md-2">
                    <div class="input-group">
                        <input type="text" class="form-control" id="reinsurance_commision" name="reinsurance_commision" placeholder="0" required="" maxlength="3" />
                        <div class="input-group-append">
                            <span class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                                %
                            </span>
                        </div>
                    </div>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="treaty_type">Treaty Type</label>
				<div class="col-sm-12 col-md-7">
                    <select name="treaty_type" id="treaty_type" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0">Surplus</option>
                        <option value="0">Facultative</option>
                        <option value="0">Quota Share</option>
                        <option value="0">Retention</option>
                        <option value="0">XOL</option>
                    </select>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="bordereaux">Bordereaux</label>
				<div class="col-sm-12 col-md-7">
                    <select name="bordereaux" id="bordereaux" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0">Monthly</option>
                        <option value="0">Quarterly</option>
                        <option value="0">Halfyearly</option>
                        <option value="0">Yearly</option>
                        <option value="0">Non Bordereaux</option>
                    </select>
				</div>
			</div>
            <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                <div class="col-sm-12 col-md-7">
                    <button type="submit" class="btn btn-primary btn-icon" id="btn-update">
                        <i class="fa fa-save"></i>
                        Save
                    </button>
                    <button type="button" class="btn btn-secondary btn-back btn-icon">
                        <i class="fa fa-arrow-left"></i>
                        Back
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

        var AlertError = $(".alert-danger");

        $("#jqGridData").jqGrid({
            url: site_url + 'setup_treaty/jqgrid_master_treaty',
            datatype: "json",
            mtype: "GET",
            postData: {
                keyword: function() {
                    return $("#keyword").val()
                }
            },
            colModel: [
                { label: 'ID', name: 'id', width: 10, hidden: true },
                { label:'Code', name:'sub_class_code',width:150,align:'center'},
                { label:'Start Date', name:'name',width:150,align:'center'},
                { label:'End Date', name:'sub_class',width:150,align:'center'},
                { label:'Class', name:'payment_mode',width:150,align:'center'},
                { label:'Minimum Limit', name:'contribution_period',width:150,align:'center'},
                { label:'Maximum Limit', name:'min_contribution',width:150,align:'center'},
                { label:'Reinsurance', name:'min_contribution',width:150,align:'center'},
                { label:'Reinsurance Share', name:'min_contribution',width:150,align:'center'},
                { label:'Reinsurance Commision', name:'min_contribution',width:150,align:'center'},
                { label:'Treaty Type', name:'min_contribution',width:150,align:'center'},
                { label:'Bordereaux', name:'min_contribution',width:150,align:'center'}
            ],
            viewrecords: true,
            autowidth: true,
            height: 350,
            rowNum: 20,
            rownumbers: true,
            shrinkToFit: false,
            sortname: "account_name'",
            sortorder: "desc",
            multiselect: false,
            pager: "#jqGridPager",
            grouping: false
        });

        $("#btn-create", "#form-list").click(function(event) {
            event.preventDefault();
            $("#form-list").hide();
            $("#form-add").show();
        });

        $("#form-save").validate({
            rules: {
                agen_code: { 
                    required:true 
                },
                agen_name: {
                    required: true
                },
                dob: {
                    required: true,
                    date: true
                },
                address: {
                    required: true
                },
                mobile_phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 13
                },
                email: {
                    required: true,
                    email: true
                }
            },
            errorElement: "em",
            invalidHandler: function (event, validator) { //display error alert on form submit
                AlertError.show();
            },
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                // error.addClass("help-block");
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                // $(element).addClass("is-valid").removeClass("is-valid");
            },
            highlight: function (element, errorClass, validClass) {
                if (validClass) {
                    $(element).addClass("is-invalid");
                }
            },
            unhighlight: function (element, errorClass, validClass) {               
                if (validClass) {
                    $(element).removeClass("is-invalid").addClass("is-valid");
                }
            },
            submitHandler: function (form) {

                $("#btn-save", "#form-save").attr('disable', true);
                var data = $("#form-save").serialize();
                $.ajax({
                    url: site_url + 'setup_system/save_menu',
                    type: 'POST',
                    timeout: 3000,
                    dataType: 'json',
                    data: data
                })
                .done(function(response) {
                    console.log(response, "done");
                    if (response.status==true) {
                        APP.swalSuccess(response.message);
                    }else{
                        APP.swalError(response.message);
                    }
                })
                .fail(function(response) {
                    APP.swalError();    
                })
                .always(function() {
                    $("#btn-save", "#form-save").attr('disable', false);
                    resetBack();
                });                

            }
        });

        $("#btn-edit", "#form-list").click(function(event) {
            event.preventDefault();
            $("#form-edit").show();
            $("#form-list").hide();
        });

        $("#form-update").validate({
            rules: {
                agen_code: { 
                    required:true 
                },
                agen_name: {
                    required: true
                },
                dob: {
                    required: true,
                    date: true
                },
                address: {
                    required: true
                },
                mobile_phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 13
                },
                email: {
                    required: true,
                    email: true
                }
            },
            errorElement: "em",
            invalidHandler: function (event, validator) { //display error alert on form submit
                AlertError.show();
            },
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                // error.addClass("help-block");
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                // $(element).addClass("is-valid").removeClass("is-valid");
            },
            highlight: function (element, errorClass, validClass) {
                if (validClass) {
                    $(element).addClass("is-invalid");
                }
            },
            unhighlight: function (element, errorClass, validClass) {               
                if (validClass) {
                    $(element).removeClass("is-invalid").addClass("is-valid");
                }
            },
            submitHandler: function (form) {

                $("#btn-update", "#form-update").attr('disable', true);
                var data = $("#form-update").serialize();
                $.ajax({
                    url: site_url + 'setup_system/update_menu',
                    type: 'POST',
                    timeout: 3000,
                    dataType: 'json',
                    data: data
                })
                .done(function(response) {
                    console.log(response, "done");
                    if (response.status==true) {
                        APP.swalSuccess(response.message);
                    }else{
                        APP.swalError(response.message);
                    }
                })
                .fail(function(response) {
                    APP.swalError();    
                })
                .always(function() {
                    $("#btn-update", "#form-update").attr('disable', false);
                    resetBack();
                });                

            }
        });

        $("#btn-search", "#form-list").click(function(event) {
            event.preventDefault();
            $("#jqGridData").trigger('reloadGrid');
        });

        $("#btn-delete", "#form-list").click(function(event) {
            event.preventDefault();
            APP.swalOption('To Deleted this Data !');
        });

        function resetBack() {
            $("#form-list").show();
            $("#form-add").hide();
            $("#form-edit").hide();

            $("#form-save").validate().resetForm();
            $("#form-update").validate().resetForm();
            $("#jqGridData").trigger('reloadGrid');
        }

        $(document).on("click", ".btn-back", function(event) {
            event.preventDefault();
            resetBack();
        });

        $(document).on("click", ".close-alert", function(event) {
            $(".alert-danger").hide();
        });

    });

</script>
