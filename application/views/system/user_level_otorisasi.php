<div class="card" id="form-list">
    <div class="card-header">
        <h4><i class="fa fa-list"></i> Form User Level Authorization </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div class="buttons">
                    <button class="btn btn-info btn-icon" id="btn-create">
                        <i class="fa fa-plus"></i>
                        Create
                    </button>
                    <button class="btn btn-warning btn-icon" id="btn-edit">
                        <i class="fa fa-edit"></i>
                        Edit
                    </button>
                    <button class="btn btn-danger btn-icon" id="btn-delete">
                        <i class="fa fa-trash"></i>
                        Delete
                    </button>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="input-group">
                    <input type="text" class="form-control border-radius-0" placeholder="Search" id="keyword" name="keyword">
                    <div class="input-group-append">
                        <button class="btn btn-info btn-icon border-radius-left-0" id="btn-search">
                            <i class="fas fa-search"></i> Search
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-sm-12 col-md-12 col-lg-12 wrapper-jqGrid">
                <table id="jqGridData"></table>
                <div id="jqGridPager"></div>
            </div>
        </div>
    </div>
</div>

<!-- Form Add -->
<div class="card" id="form-add" style="display: none;">
	<div class="card-header">
        <h4><i class="fa fa-plus"></i> Form Create User Level Authorization</h4>
	</div>
	<div class="card-body">
		<form id="form-save" class="form-horizontal" role="form">
        	<div class="alert alert-danger show fade" style="display: none;">
                <div class="alert-body">
                    <button class="close close-alert" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    You have some form errors. Please check below. 
                </div>
            </div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="add_username">User</label>
				<div class="col-sm-12 col-md-7">
                    <select name="add_username" id="add_username" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0"><?php echo 'Sysadmin'?></option>
                        <option value="0"><?php echo 'Admin'?></option>
                        <option value="0"><?php echo 'Irham Ciptadi'?></option>
                        <option value="0"><?php echo 'Hardiansyah'?></option>
                        <option value="0"><?php echo 'Syaddad'?></option>
                    </select>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="code">Code</label>
				<div class="col-sm-12 col-md-7">
                    <select name="code_add" id="code_add" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0"><?php echo 'CLM - CLAIM'?></option>
                        <option value="0"><?php echo 'UW - UNDERWRITING'?></option>
                        <option value="0"><?php echo 'FIN - FINANCE'?></option>
                    </select>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="level">Level</label>
				<div class="col-sm-12 col-md-7">
                    <select name="level" id="level" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0"><?php echo '1 (0 to 500.000.000)'?></option>
                        <option value="0"><?php echo '2 (0 to 1.000.000.000)'?></option>
                        <option value="0"><?php echo '3 (0 to 10.000.000.000)'?></option>
                    </select>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
				<div class="col-sm-12 col-md-7">
					<button type="submit" class="btn btn-primary btn-icon" id="btn-save">
						<i class="fa fa-save"></i>
						Save
					</button>
                    <button type="button" class="btn btn-secondary btn-back btn-icon">
                        <i class="fa fa-arrow-left"></i>
                        Back
                    </button>
				</div>
			</div>
		</form>
    </div>
</div>

<!-- Form Edit -->
<div class="card" id="form-edit" style="display: none;">
    <div class="card-header">
        <h4><i class="fa fa-edit"></i> Form Edit User Level Authorization</h4>
    </div>
    <div class="card-body">
        <form id="form-update" class="form-horizontal" role="form">
            <div class="alert alert-danger show fade" style="display: none;">
                <div class="alert-body">
                    <button class="close close-alert" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    You have some form errors. Please check below. 
                </div>
            </div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="username">User</label>
				<div class="col-sm-12 col-md-7">
                    <select name="username" id="username" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0"><?php echo 'Sysadmin'?></option>
                        <option value="0"><?php echo 'Admin'?></option>
                        <option value="0"><?php echo 'Irham Ciptadi'?></option>
                        <option value="0"><?php echo 'Hardiansyah'?></option>
                        <option value="0"><?php echo 'Syaddad'?></option>
                    </select>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="code">Code</label>
				<div class="col-sm-12 col-md-7">
                    <select name="code_edit" id="code_edit" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0"><?php echo 'CLM - CLAIM'?></option>
                        <option value="0"><?php echo 'UW - UNDERWRITING'?></option>
                        <option value="0"><?php echo 'FIN - FINANCE'?></option>
                    </select>
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="code">Level</label>
				<div class="col-sm-12 col-md-7">
                    <select name="code" id="code" class="form-control select2" style="width:300px;">
                        <option value="">- Please Select -</option>
                        <option value="0"><?php echo '1 (0 to 500.000.000)'?></option>
                        <option value="0"><?php echo '2 (0 to 1.000.000.000)'?></option>
                        <option value="0"><?php echo '3 (0 to 10.000.000.000)'?></option>
                    </select>
				</div>
			</div>
            <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                <div class="col-sm-12 col-md-7">
                    <button type="submit" class="btn btn-primary btn-icon" id="btn-update">
                        <i class="fa fa-save"></i>
                        Save
                    </button>
                    <button type="button" class="btn btn-secondary btn-back btn-icon">
                        <i class="fa fa-arrow-left"></i>
                        Back
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

        var AlertError = $(".alert-danger");

        $("#jqGridData").jqGrid({
            url: site_url + 'setup_system/jqgrid_user_level_otorisasi',
            datatype: "json",
            mtype: "GET",
            postData: {
                keyword: function() {
                    return $("#keyword").val()
                }
            },
            colModel: [
                { label: 'ID', name: 'id', width: 10, hidden: true },
                { label:'Username', name:'sub_class_code',width:150,align:'center'},
                { label:'Fullname', name:'name',width:150,align:'center'},
                { label:'Code', name:'sub_class',width:150,align:'center'},
                { label:'Level', name:'payment_mode',width:150,align:'center'},
                { label:'Minimum Nominal', name:'contribution_period',width:150,align:'center'},
                { label:'Maximum Nominal', name:'min_contribution',width:150,align:'center'}
            ],
            viewrecords: true,
            autowidth: true,
            height: 350,
            rowNum: 20,
            rownumbers: true,
            shrinkToFit: false,
            sortname: "account_name'",
            sortorder: "desc",
            multiselect: false,
            pager: "#jqGridPager",
            grouping: false
        });

        $("#btn-create", "#form-list").click(function(event) {
            event.preventDefault();
            $("#form-list").hide();
            $("#form-add").show();
        });

        $("#form-save").validate({
            rules: {
                agen_code: { 
                    required:true 
                },
                agen_name: {
                    required: true
                },
                dob: {
                    required: true,
                    date: true
                },
                address: {
                    required: true
                },
                mobile_phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 13
                },
                email: {
                    required: true,
                    email: true
                }
            },
            errorElement: "em",
            invalidHandler: function (event, validator) { //display error alert on form submit
                AlertError.show();
            },
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                // error.addClass("help-block");
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                // $(element).addClass("is-valid").removeClass("is-valid");
            },
            highlight: function (element, errorClass, validClass) {
                if (validClass) {
                    $(element).addClass("is-invalid");
                }
            },
            unhighlight: function (element, errorClass, validClass) {               
                if (validClass) {
                    $(element).removeClass("is-invalid").addClass("is-valid");
                }
            },
            submitHandler: function (form) {

                $("#btn-save", "#form-save").attr('disable', true);
                var data = $("#form-save").serialize();
                $.ajax({
                    url: site_url + 'setup_system/save_menu',
                    type: 'POST',
                    timeout: 3000,
                    dataType: 'json',
                    data: data
                })
                .done(function(response) {
                    console.log(response, "done");
                    if (response.status==true) {
                        APP.swalSuccess(response.message);
                    }else{
                        APP.swalError(response.message);
                    }
                })
                .fail(function(response) {
                    APP.swalError();    
                })
                .always(function() {
                    $("#btn-save", "#form-save").attr('disable', false);
                    resetBack();
                });                

            }
        });

        $("#btn-edit", "#form-list").click(function(event) {
            event.preventDefault();
            $("#form-edit").show();
            $("#form-list").hide();
        });

        $("#form-update").validate({
            rules: {
                agen_code: { 
                    required:true 
                },
                agen_name: {
                    required: true
                },
                dob: {
                    required: true,
                    date: true
                },
                address: {
                    required: true
                },
                mobile_phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 13
                },
                email: {
                    required: true,
                    email: true
                }
            },
            errorElement: "em",
            invalidHandler: function (event, validator) { //display error alert on form submit
                AlertError.show();
            },
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                // error.addClass("help-block");
            },
            success: function (label, element) {
                // Add the span element, if doesn't exists, and apply the icon classes to it.
                // $(element).addClass("is-valid").removeClass("is-valid");
            },
            highlight: function (element, errorClass, validClass) {
                if (validClass) {
                    $(element).addClass("is-invalid");
                }
            },
            unhighlight: function (element, errorClass, validClass) {               
                if (validClass) {
                    $(element).removeClass("is-invalid").addClass("is-valid");
                }
            },
            submitHandler: function (form) {

                $("#btn-update", "#form-update").attr('disable', true);
                var data = $("#form-update").serialize();
                $.ajax({
                    url: site_url + 'setup_system/update_menu',
                    type: 'POST',
                    timeout: 3000,
                    dataType: 'json',
                    data: data
                })
                .done(function(response) {
                    console.log(response, "done");
                    if (response.status==true) {
                        APP.swalSuccess(response.message);
                    }else{
                        APP.swalError(response.message);
                    }
                })
                .fail(function(response) {
                    APP.swalError();    
                })
                .always(function() {
                    $("#btn-update", "#form-update").attr('disable', false);
                    resetBack();
                });                

            }
        });

        $("#btn-search", "#form-list").click(function(event) {
            event.preventDefault();
            $("#jqGridData").trigger('reloadGrid');
        });

        $("#btn-delete", "#form-list").click(function(event) {
            event.preventDefault();
            APP.swalOption('To Deleted this Data !');
        });

        function resetBack() {
            $("#form-list").show();
            $("#form-add").hide();
            $("#form-edit").hide();

            $("#form-save").validate().resetForm();
            $("#form-update").validate().resetForm();
            $("#jqGridData").trigger('reloadGrid');
        }

        $(document).on("click", ".btn-back", function(event) {
            event.preventDefault();
            resetBack();
        });

        $(document).on("click", ".close-alert", function(event) {
            $(".alert-danger").hide();
        });

    });

</script>
