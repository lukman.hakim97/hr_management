<div class="row">
	<div class="col-lg-8">
		<div class="card">
	        <div class="card-header">
	            <h4>Production Report</h4>
	            <div class="card-header-action">
	              <a href="#" class="btn active">Year</a>
	            </div>
	        </div>
	        <div class="card-body">
	            <canvas id="myChart2" height="93"></canvas>
	        </div>
	    </div>
	</div>
	<div class="col-lg-4">
		<div class="card gradient-bottom">
			<div class="card-header">
				<h4>Top Agen</h4>
				<div class="card-header-action">
					<a href="#" class="btn btn-danger">Your Position: 3</a>
				</div>
			</div>
			<div class="card-body" id="top-5-scroll">
				<ul class="list-unstyled list-unstyled-border">
					<li class="media">
						<img class="mr-3 rounded-circle" width="50" src="<?php echo base_url() ?>assets/img/avatar/avatar-1.png" alt="avatar">
						<div class="media-body">
							<div class="float-right text-primary">1</div>
							<div class="media-title">Farhan A Mujib</div>
							<span class="text-small text-muted">Total Polis: 1.000</span> <br>
							<span class="text-small text-muted">Total Cntribution: RP. 5.000.000.000</span>
						</div>
					</li>
					<li class="media">
						<img class="mr-3 rounded-circle" width="50" src="<?php echo base_url() ?>assets/img/avatar/avatar-2.png" alt="avatar">
						<div class="media-body">
							<div class="float-right">2</div>
							<div class="media-title">Ujang Maman</div>
							<span class="text-small text-muted">Total Polis: 500</span> <br>
							<span class="text-small text-muted">Total Cntribution: RP. 3.500.000.000</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="card-footer pt-3 d-flex justify-content-center">
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="card">
	        <div class="card-header">
	            <h4>Production Percentage</h4>
	        </div>
	        <div class="card-body">
	            <canvas id="pieChart" height="150"></canvas>
	        </div>
	        <div class="card-footer pt-3 d-flex justify-content-center">
			</div>
	    </div>
	</div>
</div>

<div class="row" hidden="">
	<div class="col-lg-12">
		<div class="card">
	        <div class="card-header">
	            <h4>IN BRANCH - TOTAL CONTRIBUTION</h4>
	        </div>
	        <div class="card-body">
	            <canvas id="in_branch_contribution" height="80"></canvas>
	            <div class="statistic-details mt-1">
	              <div class="statistic-details-item">
	                <div class="detail-value">Rp. 250.0000.000</div>
	                <div class="detail-name">This Month</div>
	              </div>
	              <div class="statistic-details-item">
	                <div class="detail-value">Rp. 6.003.114.000</div>
	                <div class="detail-name">This Year</div>
	              </div>
	        </div>
	    </div>
	</div>
</div>

<br>
<script type="text/javascript">
	$(document).ready(function(){

		var ctx = document.getElementById("myChart2").getContext('2d');
		var ctx2 = document.getElementById("in_branch_contribution").getContext('2d');
		var ctx3 = document.getElementById("pieChart").getContext('2d');

		var myChart = new Chart(ctx, {
		  type: 'bar',
		  data: {
		    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "Septembre", "October", "November", "December"],
		    datasets: [{
		      label: 'Statistics',
		      data: [1000, 890, 999, 1098, 1021, 992, 1300, 1337, 1587, 1398, 1632, 459],
		      borderWidth: 2,
		      backgroundColor: 'rgba(63,82,227,.8)',
		      borderWidth: 2.5,
		      pointBackgroundColor: '#ffffff',
		      pointRadius: 4
		    }]
		  },
		  options: {
		    legend: {
		      display: false
		    },
		    scales: {
		      yAxes: [{
		        gridLines: {
		          drawBorder: false,
		          color: '#f2f2f2',
		        },
		        ticks: {
		          beginAtZero: true,
		          stepSize: 500
		        }
		      }],
		      xAxes: [{
		        gridLines: {
		          display: false
		        }
		      }]
		    },
		  }
		});

		var inBranchChart = new Chart(ctx2, {
		  type: 'bar',
		  data: {
		    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "Septembre", "October", "November", "December"],
		    datasets: [{
		      label: 'Statistics',
		      data: [570000000, 530003000, 580000000, 490000000, 500003000, 580003000, 523003000, 550093000, 570003000, 470003000, 390003000, 250000000],
		      borderWidth: 2,
		      backgroundColor: 'rgba(63,82,227,.8)',
		      borderWidth: 2.5,
		      pointBackgroundColor: '#ffffff',
		      pointRadius: 4
		    }]
		  },
		  options: {
		    legend: {
		      display: false
		    },
		    scales: {
		      yAxes: [{
		        gridLines: {
		          drawBorder: false,
		          color: '#f2f2f2',
		        },
		        ticks: {
		          beginAtZero: true,
		          stepSize: 100000000
		        }
		      }],
		      xAxes: [{
		        gridLines: {
		          display: false
		        }
		      }]
		    },
		  }
		});	

		var myPieChart = new Chart(ctx3, {
		    type: 'pie',
		    data: {
			    datasets: [{
			        data: [50, 30, 20],
			        backgroundColor: ['#0da8ee', '#fc544b', '#ffa426']
			    }],

			    labels: [
			        'KBM',
			        'Fire',
			        'Travel'
			    ]
			},
			options: {
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							var allData = data.datasets[tooltipItem.datasetIndex].data;
							var tooltipLabel = data.labels[tooltipItem.index];
							var tooltipData = allData[tooltipItem.index];
							var total = 0;
							for (var i in allData) {
								total += allData[i];
							}
							var tooltipPercentage = Math.round((tooltipData / total) * 100);
							return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
						}
					}
				}
			}
		});	
		
	})
</script>