<div class="card">
	<div class="card-header">
		<!-- <h4>Form Change Password</h4> -->
	</div>
	<div class="card-body">
		<form id="saveForm" method="post" class="form-horizontal" action="">
        	<div class="alert alert-danger" style="display:none;">
				<button class="close" data-dismiss="alert"></button>
				You have some form errors. Please check below.
			</div>
			<div class="alert alert-success" style="display:none;">
				<button class="close" data-dismiss="alert"></button>
				Successfully saved
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="old_password">z</label>
				<div class="col-sm-12 col-md-7">
					<input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password" />
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="password">Password</label>
				<div class="col-sm-12 col-md-7">
					<input type="password" class="form-control" id="password" name="password" placeholder="Password" />
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="confirm_password">Confirm password</label>
				<div class="col-sm-12 col-md-7">
					<input type="password" class="form-control" id="confirm_password" name="confirm_password[old][abcd][]"
						placeholder="Confirm password" />
				</div>
			</div>
			<div class="form-group row mb-4">
				<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
				<div class="col-sm-12 col-md-7">
					<button type="submit" class="btn btn-primary" name="save" value="Save" id="btn-save">
						<i class="fa fa-save"></i>
						Save
					</button>
				</div>
			</div>
		</form>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		var AlertError = $(".alert-danger"),
		form = $("#saveForm"),
		btnSave = $("#btn-save");

		$(form).validate({
			rules: {
				old_password: {
					required: true,
					minlength: 6
				},
				password: {
					required: true,
					minlength: 6
				},
				confirm_password: {
					required: true,
					minlength: 6,
					equalTo: "#password"
				},
			},
			errorElement: "em",
			invalidHandler: function (event, validator) { //display error alert on form submit
				AlertError.show();
			},
			errorPlacement: function (error, element) {
				// Add the `help-block` class to the error element
				// error.addClass("help-block");
			},
			success: function (label, element) {
				// Add the span element, if doesn't exists, and apply the icon classes to it.
				// $(element).addClass("is-valid").removeClass("is-valid");
			},
			highlight: function (element, errorClass, validClass) {
				if (validClass) {
					$(element).addClass("is-invalid");
				}
			},
			unhighlight: function (element, errorClass, validClass) {				
				if (validClass) {
					$(element).removeClass("is-invalid").addClass("is-valid");
				}
			},
			submitHandler: function (form) {
				
				$(btnSave).attr('disable', true);
				var data = $(form).serialize();
				$.ajax({
					url : site_url + 'home/save_change_password',
					data: data,
					dataType: "json",
					type: "POST",
					success: function (response) {
						if (response.success) {
							APP.swalSuccess(response.message);
						}else{
							APP.swalError();
						}
						$(btnSave).attr('disable', false);
					},
					error: function() {
						APP.swalError();	
						$(btnSave).attr('disable', false);
					}
				})
			}
		});
	});

</script>
