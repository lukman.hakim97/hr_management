<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= (isset($title) ? $title : '') ?> | <?php echo $this->config->item('app_title') ?></title>

    <!-- Icon -->
    <link rel="icon" href="<?= base_url('assets/img/logo/ico-chubb.png') ?>" />

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- CSS Libraries -->
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/smartwizard/css/smart_wizard.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/smartwizard/css/smart_wizard_theme_arrows.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/smartwizard/css/smart_wizard_theme_dots.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/smartwizard/css/smart_wizard_theme_circles.min.css') ?>">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url('assets/node_modules/bootstrap-daterangepicker/daterangepicker.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/jqgrid/css/trirand/ui.jqgrid-bootstrap4.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/node_modules/owl.carousel/dist/assets/owl.carousel.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css');?>">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/4.4.0/font/octicons.css">

    <!-- <link rel="stylesheet" href="<?php echo base_url('assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/node_modules/datatables/media/css/jquery.dataTables.min.css');?>"> -->
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/node_modules/datatables/media/css/jquery.dataTables.min.css');?>">
    <!-- <link rel="stylesheet" href="<?php echo base_url('assets/global/plugins/uniform/css/uniform.default.css');?>"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/components.css') ?>">
    <link rel="stylesheet" href="<?php add_css('assets/css/style.new.css') ?>">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>">
    
    <script type="text/javascript">
        var site_url = "<?php echo site_url() ?>/";
        var base_url = "<?php echo base_url() ?>/";
    </script>
    <!-- JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- Validate -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

    <!-- Plugin-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jqgrid/js/trirand/jquery.jqGrid.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jqgrid/js/trirand/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/node_modules/owl.carousel/dist/owl.carousel.min.js'); ?>"></script>

    <!-- General JS Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="<?php echo base_url('assets/js/stisla.js') ?>"></script>
    <script src="<?php echo base_url('assets/node_modules/sweetalert/dist/sweetalert.min.js') ?>"></script>
    
    <!-- JS Libraies -->
    <script src="<?php echo base_url('assets/plugins/smartwizard/js/jquery.smartWizard.min.js') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="<?php echo base_url('assets/node_modules/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
    <script src="<?php echo base_url('assets/node_modules/cleave.js/dist/cleave.min.js');?>"></script>
    <script src="<?php echo base_url('assets/node_modules/cleave.js/dist/addons/cleave-phone.id.js');?>"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script> 
    <script src="<?php echo base_url(); ?>assets/plugins/jquery.printElement.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/highcharts.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.8/jstree.min.js" integrity="sha256-NPMXVob2cv6rH/kKUuzV2yXKAQIFUzRw+vJBq4CLi2E=" crossorigin="anonymous"></script>
    <script src="<?php echo base_url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <!-- <script src="<?php echo base_url('assets/global/plugins/uniform/jquery.uniform.min.js') ?>"></script> -->
    
    <!-- <script src="<?php echo base_url(); ?>assets/node_modules/datatables/media/js/jquery.dataTables.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>assets/node_modules/datatables/media/js/jquery.dataTables.min.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>assets/node_modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    
    <!-- CORE JS -->
    <script src="<?php add_js('assets/global/scripts/metronic.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/node_modules/owl.carousel/dist/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/node_modules/chart.js/dist/Chart.min.js'); ?>"></script>
    
    <!-- Template JS File -->
    <script src="<?php echo base_url('assets/js/scripts.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
    
    <!-- Specific File -->
    <?php echo put_headers(); ?>

    <!-- Specific File -->  
    <?php echo put_footers(); ?>
    
    <!-- Initial -->
    <script type="text/javascript">
        $(document).ready(function() {   
            $.jgrid.defaults.width = 780;
            $.jgrid.defaults.styleUI = 'Bootstrap4';
            $.jgrid.defaults.iconSet = 'Octicons';
            
            APP.init();
            Metronic.init();

            // var url = window.location;
            // var ele = $("a[href$='"+url+"']");
            // console.log(ele);
            // ele.parent().parent().parent().css({display:"block"}).parent().addClass('open').parent().css({display:"block"}).parent().addClass('open').addClass('start').addClass('active');
            
            var current = location.href;
            $('ul li a').each(function(){
                var $this = $(this);
                // if the current path is like this link, make it active
                if($this.attr('href').indexOf(current) !== -1){
                    $this.parent().addClass('active');
                }
            });

        });

        $( document ).ready(function() {
            var url = window.location;
            var ele = $("a[href$='"+url+"']");
            console.log(ele);
            ele.parent().parent().css({display:"block"}).parent().addClass('open').parent().css({display:"block"}).parent().addClass('open').addClass('start').addClass('active');
        });

        $(document).ajaxStart(function(){
            // $.blockUI({ message: '<div  align="center"><div class="loader"></div></div>' ,css: { backgroundColor: '#fff', padding:'30px'} })
            $.blockUI({ message: '<div style="padding:5px 0;">Please wait...</div>' ,css: { backgroundColor: '#fff', color: '#000', fontSize: '12px'} })
        }).ajaxStop($.unblockUI);
    </script>
</head>
<body>
    <style type="text/css">
        .btn-green{
            background-color: #5cb85c;
            color: white;
        }
    </style>
    <div id="app">
        <div class="main-wrapper">
            <?php $this->load->view('layout/sidebar') ?>
            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1><?= $nav ?></h1>
                        <div class="section-header-breadcrumb">
                            <?php foreach($nav_list  as $i => $nav_item): ?>
                            <div class="breadcrumb-item <?= ($nav == $nav_item ? 'active' : '')?>"><?= anchor($nav_item, $nav_item) ?></div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="section-body">
                        <?= $contents; ?>
                    </div>
                </section>
            </div>
            <footer class="main-footer">
                <div class="footer-left">
                    Copyright &copy; 2020 <div class="bullet"></div> <a href="#">Group 5</a>
                </div>
                <div class="footer-right">
                    1.0
                </div>
            </footer>
        </div>
    </div>
</body>
</html>