<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
	<form class="form-inline mr-auto">
		<ul class="navbar-nav mr-3">
			<li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
		</ul>
	</form>
	<ul class="navbar-nav navbar-right">
		<li class="dropdown">
			<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
				<img alt="image" src="<?php echo base_url() ?>assets/img/avatar/avatar-1.png"
					class="rounded-circle mr-1">
				<div class="d-sm-none d-lg-inline-block">Hi, <?php echo $this->session->userdata('fullname') ?></div>
			</a>
			<div class="dropdown-menu dropdown-menu-right">
				<div class="dropdown-title">Profile</div>
				<a href="<?php echo site_url('home/change_password') ?>" class="dropdown-item has-icon">
					<i class="fas fa-key"></i> Change Password
				</a>
				<div class="dropdown-divider"></div>
				<a href="<?php echo site_url('home/logout') ?>" class="dropdown-item has-icon text-danger">
					<i class="fas fa-sign-out-alt"></i> Logout
				</a>
			</div>
		</li>
	</ul>
</nav>
<div class="main-sidebar">
	<aside id="sidebar-wrapper" style="font-size: 13px">
		<div class="sidebar-brand">
			<a href="<?php echo site_url('home') ?>"><?php echo $this->config->item('app_name') ?></a>
		</div>
		<div class="sidebar-brand sidebar-brand-sm">
			<a href="<?php echo site_url('home') ?>"><?php echo $this->config->item('app_name_short') ?></a>
		</div>
		<ul class="sidebar-menu">
			<li class="menu-header">Dashboard</li>
			<li><a class="nav-link" href="<?php echo site_url('home/dashboard') ?>"><i class="fas fa-tachometer-alt"></i><span>Dashboard</span></a></li>
			<li><a class="nav-link" href="<?php echo site_url('home') ?>"><i class="fas fa-newspaper"></i><span>News</span></a></li>
		</ul>
		<ul class="sidebar-menu">
			<li class="menu-header">Common</li>
			<li>
				<a class="nav-link" href="<?php echo site_url('common/company') ?>">
					<i class="fas fa-business-time"></i><span>Company</span></a>
			</li>
			<li>
				<a class="nav-link" href="<?php echo site_url('common/outlet') ?>">
					<i class="fas fa-business-time"></i><span>Branch</span>
				</a>
			</li>
			<li>
				<a class="nav-link" href="<?php echo site_url('common/departement') ?>">
					<i class="fas fa-business-time"></i><span>Departement</span>
				</a>
			</li>
			<li>
				<a class="nav-link" href="<?php echo site_url('common/position') ?>">
					<i class="fas fa-business-time"></i><span>Position</span>
				</a>
			</li>
		</ul>
		<ul class="sidebar-menu">
			<li class="menu-header">System</li>
			<li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-cog"></i><span>Setup</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="<?php echo site_url('setup_system/menu') ?>">Setup Menu</a></li>
                    <li><a class="nav-link" href="<?php echo site_url('setup_system/user') ?>">Setup User</a></li>
                    <li><a class="nav-link" href="<?php echo site_url('setup_system/privilage') ?>">Setup Privilage</a></li>
                    <li><a class="nav-link" href="<?php echo site_url('setup_bank') ?>">Bank Setup</a></li>
                </ul>
            </li>
		</ul>
		<ul class="sidebar-menu">
			<li class="menu-header">Setup</li>
			<li>
				<a class="nav-link" href="<?php echo site_url('setup_product/class_of_bussiness') ?>"><i class="fas fa-business-time"></i><span>Menu 1</span></a>
			</li>
			<li>
				<a class="nav-link" href="<?php echo site_url('setup_system/soal') ?>">
					<i class="fas fa-business-time"></i><span>Soal</span>
				</a>
			</li>
		</ul>
	</aside>
</div>
