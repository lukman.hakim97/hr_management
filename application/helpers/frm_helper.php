<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function frm_open($action='', $attributes='', $hidden=array())
{
	$html = form_open($action, $attributes, $hidden);
	echo $html;
}

function frm_body()
{
	$html = '<div class="form-body">';
	echo $html;
}

function frm_body_close()
{
	$html = '</div>';
	echo $html;
}

function frm_row($extra='')
{
	$html = '<div class="row" '.$extra.'>';
	echo $html;
}

function frm_row_close()
{
	$html = '</div>';
	echo $html;
}

function frm_column($col_class,$col_id='',$extra='')
{
	if ($col_id!='') {
		$col_id = ' id="'.$col_id.'"';
	}
	$html = '<div class="'.$col_class.'" '.$col_id.' '.$extra.'>';
	echo $html;
}

function frm_column_close()
{
	$html = '</div>';
	echo $html;
}

function frm_column_group($col_class)
{
	$html = '<div class="'.$col_class.'">';
	// $html .= '<div class="col-group">';
	$html .= '<div class="row">';
	echo $html;
}
function frm_column_group_close()
{
	$html = '</div>';
	$html .= '</div>';
	echo $html;
}

function frm_label($text,$extra='')
{
	$html = '<label class="control-label small" '.$extra.'>'.$text.'</label>';
	echo $html;
}
function frm_input($data = '', $value = '', $extra = '')
{
	$extra .= ' class="form-control" ';
	$html = form_input($data, $value, $extra);
	echo $html;
}
function frm_input_upper($data = '', $value = '', $extra = '')
{
	$extra .= ' class="form-control toupper" ';
	$html = form_input($data, $value, $extra);
	echo $html;
}
function frm_input_currency($data = '', $value = '', $extra = '')
{
	$extra .= ' class="form-control maskmoney currency text-right" ';
	$html = form_input($data, $value, $extra);
	echo $html;
}
function frm_input_group()
{
	echo '<div class="input-group">';
}
function frm_input_group_addon($addon)
{
	// echo '<span class="input-group-addon">'.$addon.'</span>';
	echo '<div class="input-group-append border-right-0"><div class="input-group-text">'.$addon.'</div></div>';
}
function frm_input_group_btn($addon)
{
	echo '<div class="input-group-append">'.$addon.'</div>';
}
function frm_input_group_close()
{
	echo '</div>';
}
function frm_datepicker($data = '', $value = '', $extra = '')
{
	// $extra .= ' class="form-control small datepicker maskdate" placeholder="dd/mm/yyyy" ';
	$extra .= ' class="form-control datemask datepicker-birthday" placeholder="dd/mm/yyyy" ';
	$html = form_input($data, $value, $extra);
	echo $html;
}
function frm_textarea($data = '', $value = '', $extra = '')
{
	$extra .= ' class="form-control" ';
	$html = form_textarea($data, $value, $extra);
	echo $html;
}
function frm_textarea_upper($data = '', $value = '', $extra = '')
{
	$extra .= ' class="form-control toupper" ';
	$html = form_textarea($data, $value, $extra);
	echo $html;
}
function frm_dropdown($name = '', $options = array(), $selected = array(), $extra = '')
{
	$extra .= ' class="form-control select2" ';
	$html = form_dropdown($name, $options, $selected, $extra);
	echo $html;
}
function frm_multiselect($name = '', $options = array(), $selected = array(), $extra = '')
{
	echo form_multiselect($name, $options, $selected, $extra);
}
function frm_radio_open()
{
	$html = '<div class="radio-list">';
	echo $html;
}
function frm_radio($data = '', $value = '', $checked = FALSE, $text = '', $extra = '')
{	
	$html = '<label class="radio-inline">';
	$html .= form_radio($data, $value, $checked, $extra);
	$html .= $text;
	$html .= '</label>';
	echo $html;
}
function frm_radio_close()
{
	$html = '</div>';
	echo $html;
}

function frm_checkbox_open($extra='')
{
	$html = '<div class="checkbox-list" '.$extra.' >';
	echo $html;
}
function frm_checkbox($data = '', $value = '', $checked = FALSE, $text = '', $extra = '')
{	
	$html = '<label class="checkbox-inline" style="font-size:12px !important; padding-rigth:10px !important;">';
	$html .= form_checkbox($data, $value, $checked, $extra);
	$html .= $text;
	$html .= '</label>';
	echo $html;
}
function frm_checkbox_close()
{
	$html = '</div>';
	echo $html;
}

function frm_action()
{
	echo '<div class="form-actions">';
}

function frm_action_close()
{
	echo '</div>';
}

function frm_submit($data = '', $value = '', $extra = '')
{
	echo form_submit($data, $value, $extra);
}

function frm_reset($data = '', $value = '', $extra = '')
{
	echo form_reset($data, $value, $extra);
}

function frm_hidden($name = '', $value = '', $extra = '')
{
	// echo form_hidden($name, $value, $extra);
	echo '<input type="hidden" name="'.$name.'" value="'.$value.'" '.$extra.'>';
}