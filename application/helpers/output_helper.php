<?php
    function greetings() {
        $time = date("H");
        /* Set the $timezone variable to become the current timezone */
        $timezone = date("e");
        /* If the time is less than 1200 hours, show good morning */
        if ($time < "12") {
            $greetings = "Good morning";
        } else
        /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
        if ($time >= "12" && $time < "17") {
            $greetings = "Good afternoon";
        } else
        /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
        if ($time >= "17" && $time < "19") {
            $greetings = "Good evening";
        } else
        /* Finally, show good night if the time is greater than or equal to 1900 hours */
        if ($time >= "19") {
            $greetings = "Good night";
        }
        return $greetings;
    }

    function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array($first_name, $last_name);
    }

    function auto_version($file='')
    {
        if(!file_exists($file)) return $file;
        $mtime = filemtime($file);
        return $file.'?'.$mtime;
    }

    function printr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die();
    }

    //Dynamically add Javascript files to header page
    if(!function_exists('add_js')){
        function add_js($file='')
        {
            $str = '';
            $ci = &get_instance();
            $footer_js  = $ci->config->item('footer_js');

            if(empty($file)){
                return;
            }

            if(is_array($file)){
                if(!is_array($file) && count($file) <= 0){
                    return;
                }
                foreach($file AS $item){
                    $footer_js[] = $item;
                }
                $ci->config->set_item('footer_js',$footer_js);
            }else{
                $str = $file;
                $footer_js[] = $str;
                $ci->config->set_item('footer_js',$footer_js);
            }
        }
    }

    //Dynamically add CSS files to header page
    if(!function_exists('add_css')){
        function add_css($file='')
        {
            $str = '';
            $ci = &get_instance();
            $header_css = $ci->config->item('header_css');

            if(empty($file)){
                return;
            }

            if(is_array($file)){
                if(!is_array($file) && count($file) <= 0){
                    return;
                }
                foreach($file AS $item){   
                    $header_css[] = $item;
                }
                $ci->config->set_item('header_css',$header_css);
            }else{
                $str = $file;
                $header_css[] = $str;
                $ci->config->set_item('header_css',$header_css);
            }
        }
    }

    if(!function_exists('put_headers')){
        function put_headers()
        {
            $str = '';
            $ci = &get_instance();
            $header_css = $ci->config->item('header_css');

            foreach($header_css AS $item){
                $str .= '<link rel="stylesheet" href="'.base_url().auto_version($item).'" type="text/css" />'."\n";
            }

            return $str;
        }
    }

    if(!function_exists('put_footers')){
        function put_footers()
        {
            $str = '';
            $ci = &get_instance();
            $footer_js  = $ci->config->item('footer_js');

            foreach($footer_js AS $item){
                $str .= '<script type="text/javascript" src="'.base_url().auto_version($item).'"></script>'."\n";
            }

            return $str;
        }
    }
    //Dynamically add Javascript files to header page

?>