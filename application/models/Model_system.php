<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*[PERHATIAN]
Source Code ini milik PT ama salam Indonesia.
Dilarang menggunakan sebagian atau seluruhnya tanpa izin tertulis dari PT ama salam Indonesia 
*/

class Model_system extends CI_Model {

	public function jqgrid_branch($sidx='',$sord='',$limit_rows='',$start='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

        $sql = array(
            array('id'=>'1','branch_code'=>'001','branch_name'=>'Kantor Pusat')
        );

		return $sql;
	}
	
	public function jqgrid_level_otorisasi($sidx='',$sord='',$limit_rows='',$start='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

        $sql = array(
            array('id'=>'1','code'=>'CLM','level'=>'1','min_nominal'=>'0','max_nominal'=>'100.000.000'),
            array('id'=>'2','code'=>'CLM','level'=>'2','min_nominal'=>'0','max_nominal'=>'500.000.000'),
            array('id'=>'3','code'=>'CLM','level'=>'3','min_nominal'=>'0','max_nominal'=>'1.000.000.000'),
            array('id'=>'3','code'=>'UW','level'=>'1','min_nominal'=>'0','max_nominal'=>'1.000.000.000')
        );

		return $sql;
	}
	
	public function jqgrid_user_level_otorisasi($sidx='',$sord='',$limit_rows='',$start='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

        $sql = array(
            array('id'=>'1','username'=>'Syadmin','fullname'=>'Administration','code'=>'CLM','level'=>'1','min_nominal'=>'0','max_nominal'=>'100.000.000'),
            array('id'=>'2','username'=>'underwriting','fullname'=>'Underwriter','code'=>'UW','level'=>'1','min_nominal'=>'0','max_nominal'=>'500.000.000'),
            array('id'=>'3','username'=>'finance','fullname'=>'Finance','code'=>'FIN','level'=>'1','min_nominal'=>'0','max_nominal'=>'1.000.000.000'),
        );

		return $sql;
	}
	
	public function jqgrid_master_treaty($sidx='',$sord='',$limit_rows='',$start='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

        $sql = array(
            array('id'=>'1','code'=>'001','start_date'=>'01/12/2019','end_date'=>'31/12/2019','class_code'=>'Enginering','min_limit'=>'0','max_limit'=>'100.000.000','reinsurance'=>'REINDO','reinsurance_share'=>'15%','reinsurance_commision'=>'25%','treaty_type'=>'Surplus','bordereaux'=>'Monthly')
        );

		return $sql;
	}
	
	public function jqgrid_process_reinsurance($sidx='',$sord='',$limit_rows='',$start='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

        $sql = array(
            array('id'=>'1','start_date'=>'01/12/2019','end_date'=>'31/12/2019','status'=>'Already process','process_date'=>'31/12/2019','process_by'=>'Admin')
        );

		return $sql;
	}
	
	public function jqgrid_menu($sidx='',$sord='',$limit_rows='',$start='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

        $sql = array(
            array('id'=>'1','parent'=>'Setup','title'=>'Home','url'=>'setup_system/menu','type'=>'Normal','icon'=>'home'),
            array('id'=>'2','parent'=>'Setup','title'=>'User','url'=>'setup_system/user','type'=>'Normal','icon'=>'user'),
            array('id'=>'3','parent'=>'Setup','title'=>'Privilege','url'=>'setup_system/privilege','type'=>'Normal','icon'=>'privilege'),
        );

		return $sql;
	}
	
	public function jqgrid_user($sidx='',$sord='',$limit_rows='',$start='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

        $sql = array(
            array('id'=>'1','username'=>'Sysadmin','fullname'=>'Administrator','role'=>'admin','status'=>'Active','register_date'=>'10/12/2019'),
            array('id'=>'2','username'=>'finance','fullname'=>'Finance','role'=>'finance','status'=>'Active','register_date'=>'10/12/2019'),
            array('id'=>'3','username'=>'uw','fullname'=>'Underwriter','role'=>'uw','status'=>'Active','register_date'=>'10/12/2019'),
        );

		return $sql;
	}
	
	public function jqgrid_privilege($sidx='',$sord='',$limit_rows='',$start='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

        $sql = array(
            array('id'=>'1','name'=>'Sysadmin','description'=>'Administrator'),
            array('id'=>'2','name'=>'finance','description'=>'Finance'),
            array('id'=>'3','name'=>'uw','description'=>'Underwriter'),
        );

		return $sql;
    }

}