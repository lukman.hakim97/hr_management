<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*[PERHATIAN]
Source Code ini milik PT ama salam Indonesia.
Dilarang menggunakan sebagian atau seluruhnya tanpa izin tertulis dari PT ama salam Indonesia 
*/

class Model_setup_bank extends CI_Model {
	
	public function jqgrid_bank($sidx='',$sord='',$limit_rows='',$start='',$keyword='')
	{
		$order = '';
		$limit = '';
		$param = array();

		if ($sidx!='' && $sord!='') $order = "ORDER BY $sidx $sord";
		if ($limit_rows!='' && $start!='') $limit = "LIMIT $limit_rows OFFSET $start";

		$sql = "SELECT * FROM tbl_bank ";
		
		if($keyword!=''){
			$sql .=" where upper(bank_code) like ? or UPPER(bank_name) like ?";
			$keyword 	= strtolower($keyword);
			$keyword 	= strtoupper($keyword);
			$param[] 	= '%'.$keyword.'%';
			$param[] 	= '%'.$keyword.'%';
		}

		$sql.= "$order $limit";

		$query = $this->db->query($sql,$param);
		return $query->result_array();
		
	}
	
	function get_data_bank_by_id($id){
		$sql = "SELECT * FROM tbl_bank where id = ?";
		$q = $this->db->query($sql,array($id));
		return $q->row_array();
	}
}