<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*[PERHATIAN]
Source Code ini milik PT ama salam Indonesia.
Dilarang menggunakan sebagian atau seluruhnya tanpa izin tertulis dari PT ama salam Indonesia 
*/

class Model_get extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }

	public function ExtractPeriodDate($from_date,$thru_date,$type) {

		$from_date = new DateTime($from_date);
		$thru_date =new DateTime($thru_date);
		$interval = $thru_date->diff($from_date);

		switch ($type) {
			case 'FullYear':
			$time = $interval->format('%y');
			break;
			case 'FullDay':
			$time = $interval->format('%a');
			break;
			case 'MonthOfLastYear':
			$time = $interval->format('%m');
			break;
			case 'FullMonth':
			$time = $interval->format('%m') + ($interval->format('%y') * 12);
			break;
			case 'DayOfLastYear':
			$time = $interval->format('%d');
			break;
			default:
			die("error function ExtractPeriodDate() : The Parameter of Type \"".$type."\" Is Undefined.");
			break;
		}
		return $time;

	}

	function group_policy_by_keyword($keyword)
	{
		$sql = "SELECT
				a.cif_no,
				a.policy_no,
				a.agen_code,
				e.nama as agen_name,
				e.upline_agen,
				a.spa_no,
				a.flag_administrasi_peserta,
				b.nama as lembaga,
				c.product_code,
				b.model_cabang,
				b.cif_no,
				b.branch_code,
				f.branch_name,
				c.product_name,
				(case when coalesce(d.manajemen_fee,0) > 0 then d.manajemen_fee else 
					(case when coalesce(a.manajemen_fee,0) > 0 then a.manajemen_fee else c.manajemen_fee end)
				end) as manajemen_fee,
				c.minimal_premi,
				'1' as flag_produk,
				'0' as flag_product_saving
				from life_grup_policy a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				left join life_grup_policy_nota_teknik d ON d.spa_no = a.spa_no
				left join life_agen e ON e.no_agen = a.agen_code
				left join life_branch f ON f.branch_code = b.branch_code
				where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?) 
				--and a.status_aktif = '1' 
				/*20180828 adesagita, agen dan branch dileft join dan status aktif dikomen karena budiman select beberaa polis ga bisa terkait kondisi tsb*/
				";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses
		
		$sql .= " UNION ALL ";

		$sql .= "SELECT
				a.cif_no,
				a.policy_no,
				a.agen_code,
				e.nama as agen_name,
				e.upline_agen,
				a.spa_no,
				a.flag_administrasi_peserta,
				b.nama as lembaga,
				c.product_code,
				b.model_cabang,
				b.cif_no,
				b.branch_code,
				f.branch_name,
				c.product_name,
				'0' as manajemen_fee,
				c.minimal_premi,
				'2' as flag_produk,
				'1' as flag_product_saving
				from life_grup_policy_saving a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				inner join life_agen e ON e.no_agen = a.agen_code
				inner join life_branch f ON f.branch_code = b.branch_code
				where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?) ";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses
		
		$sql .= " UNION ALL ";

		$sql .= "SELECT
				g.cif_no,
				a.policy_no,
				a.agen_code,
				e.nama as agen_name,
				e.upline_agen,
				a.spa_no,
				'0' flag_administrasi_peserta,
				g.nama as lembaga,
				c.product_code,
				'0' as model_cabang,
				g.cif_no,
				a.branch_code,
				f.branch_name,
				c.product_name,
				'0' as manajemen_fee,
				c.minimal_premi,
				'3' as flag_produk,
				'2' as flag_product_saving
				from life_individu_policy a
				left join life_individu_policy_cif b on a.id=b.policy_id
				left join life_product c on c.product_code=a.product_code
				inner join life_agen e ON e.no_agen = a.agen_code
				inner join life_branch f ON f.branch_code = a.branch_code
				inner join life_individu_cif g ON g.cif_no = b.cif_no
				where (UPPER(a.policy_no) like ? OR UPPER(g.nama) like ?) and  b.flag_cif = '0' and a.policy_no is not null ";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
           if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND a.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_participant_by_keyword($keyword,$policy_no)
	{
		$param = array();
		$sql = "select
				a.participant_no,
				a.nama,
				a.tanggal_lahir,
				a.jenis_kelamin,
				a.awal_kontrak,
				a.akhir_kontrak,
				a.total_benefit,
				b.alamat,
				b.kota,
				b.kodepos,
				b.telpon,
				b.tlp_seluler_pejabat
				from life_grup_policy_participant a
				inner join life_grup_policy c on c.spa_no=a.spa_no
				inner join life_grup_cif b on b.cif_no=c.cif_no
				where c.policy_no = ?";

			$param[] = $policy_no;

		if($keyword!=''){
			$sql .= " and (UPPER(a.participant_no) like ? OR UPPER(a.nama) like ?)";
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		}
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function grouplife_policy_batch($q)
	{
		$sql = "select * from life_grup_participant_batch where upper(batch_no) like ?";
		$query = $this->db->query($sql,array(strtoupper(strtolower('%'.$q.'%'))));
		return $query->result_array();
	}

	function getBatchNoBySpaNo($spa_no='')
	{
		$sql = "SELECT * from life_grup_participant_batch where spa_no = ? ";
		$query = $this->db->query($sql,array($spa_no));
		return $query->result_array();
	}

	function GetBenefitByGroup($spa_no,$usia,$kontrak)
	{
		$sql = "SELECT premium_rate,benefit_code,benefit_name,
		description,
		benefit_value,
		(premium_rate*benefit_value) as kontribusi,
		flag_benefit_setiap_peserta
		from (select
			(case when d.premium_rate_type = 0 then
				(select fn_get_premium_rate(c.product_code,b.benefit_code,?,?,?))
			else d.premium_rate_value end) as premium_rate,
				b.benefit_code,b.benefit_name,b.description,a.benefit_value,
				c.flag_benefit_setiap_peserta
			from life_grup_policy_benefit a
			left join life_benefit b on b.benefit_code=a.benefit_code
			left join life_grup_policy c on c.spa_no=a.spa_no
			left join life_product_benefit d on d.benefit_code=b.benefit_code and d.product_code=c.product_code
			where a.spa_no=? ORDER BY a.sequence
		) as x
		";
		$query = $this->db->query($sql,array($usia,$kontrak,$spa_no,$spa_no));
		return $query->result_array();
	}

	function getUsiaAsuransi($date,$date2='')
	{
		date_default_timezone_set('Asia/Jakarta');
		$date = date('Y-m-d', strtotime($date));
		if ($date2=='') {
			$date2 = date('Y-m-d');
		}
		$day = $this->ExtractPeriodDate($date,$date2,'FullDay');
		$usia = round($day/365.25,0);
    	$usia = array('usia'=>$usia);
    	return $usia;
	}
	
	function getUsiaAsuransiProduct($date,$date2='',$usia_type)
	{
		if ($date2=='') {
			$date2 = date('Y-m-d');
		}
		$sql = "select fn_get_age_insurance(?,?,?) as usia";
		$query = $this->db->query($sql,array($date,$date2,$usia_type));
		return $query->row_array();
	}

	function getRiwayatKesehatanByProduk($product_code)
	{
		$sql = "select a.id_question,b.no_question,b.question
				from life_product_medical_question a
				left join life_medical_question b on a.id_question = b.id
				where a.product_code=?
				order by b.no_question asc";
		$query = $this->db->query($sql,array($product_code));
		return $query->result_array();
	}

	function getUwStatus($product_code,$usia,$manfaat)
	{
		$sql = "select fn_get_uwpolicy(?,?,?) uw_status";
		$query = $this->db->query($sql,array($product_code,$usia,$manfaat));
		return $query->row_array();
	}

	function getUwStatusSaving($product_code,$usia,$manfaat,$spa_no)
	{
		$sql = "select fn_get_uwpolicy_saving(?,?,?,?) uw_status";
		$query = $this->db->query($sql,array($product_code,$usia,$manfaat,$spa_no));
		return $query->row_array();
	}

	function TglAkhirAsuransi($from_date,$month,$day)
	{
		$sql = "select ('$from_date'::date + ' $month month'::interval) + ' $day day'::interval as date";
		$query = $this->db->query($sql);
		$row = $query->row_array();
		return $row['date'];
	}
	function TglAkhirAsuransi1($from_date,$year,$month)
	{
		$sql = "select ('$from_date'::date + ' $year year'::interval) + ' $month month'::interval as date";
		$query = $this->db->query($sql);
		$row = $query->row_array();
		return $row['date'];
	}

	function getCabangCIF($cif_no)
	{
		$sql = "select a.id as cif_branch_id, a.kode,a.nama
				from life_grup_cif_branch a, life_grup_cif b
				where a.cif_no=b.cif_no
				and b.model_cabang=1
				and a.cif_no = ?";

		$query = $this->db->query($sql,array($cif_no));
		return $query->result_array();
	}


	// BEGIN COUNTER BATCH
	function getCounterSequenceBatch($thn,$bln,$policy_no)
	{
		$sql = "select sequence from life_counter_batch
				where thn=? and bln=? and policy_no=?";
		$query = $this->db->query($sql,array($thn,$bln,$policy_no));
		$row = $query->row_array();
		if (count($row)>0) {
			return $row['sequence'];
		} else {
			$this->insertNewCounterBatch($thn,$bln,$policy_no);
		}
	}

	function updateCounterBatch($thn,$bln,$policy_no,$current_sequence)
	{
		$data = array('sequence'=>$current_sequence+1);
		$param = array('thn'=>$thn,'bln'=>$bln,'policy_no'=>$policy_no);
		$this->db->trans_begin();
		$this->db->update('life_counter_batch',$data,$param);
		if ($this->db->trans_status()===true) {
			$this->db->trans_commit();
		} else {
			$this->db->trans_rollback();
		}
	}

	function insertNewCounterBatch($thn,$bln,$policy_no)
	{
		$data = array('thn'=>$thn,'bln'=>$bln,'policy_no'=>$policy_no,'sequence'=>0);
		$this->db->trans_begin();
		$this->db->insert('life_counter_batch',$data);
		if ($this->db->trans_status()===true) {
			$this->db->trans_commit();
		} else {
			$this->db->trans_rollback();
		}
	}
	// END

	// BEGIN COUNTER GLOBAL
	function getCounterSequence($param,$counter_type)
	{
		$sql = "select sequence from life_counter where param=? and counter_type=?";
		$query = $this->db->query($sql,array($param,$counter_type));
		$row = $query->row_array();
		if (count($row)>0) {
			return $row['sequence'];
		} else {
			$this->insertNewCounter($param,$counter_type);
		}
	}

	function updateCounter($param,$counter_type,$current_sequence)
	{
		$data = array('sequence'=>$current_sequence+1);
		$param = array('param'=>$param,'counter_type'=>$counter_type);
		$this->db->trans_begin();
		$this->db->update('life_counter',$data,$param);
		if ($this->db->trans_status()===true) {
			$this->db->trans_commit();
		} else {
			$this->db->trans_rollback();
		}
	}

	function insertNewCounter($param,$counter_type)
	{
		$data = array('param'=>$param,'counter_type'=>$counter_type,'sequence'=>0);
		$this->db->trans_begin();
		$this->db->insert('life_counter',$data);
		if ($this->db->trans_status()===true) {
			$this->db->trans_commit();
		} else {
			$this->db->trans_rollback();
		}
	}
	// END

	function getDataParticipantById($id)
	{
		$sql = "select
					a.id,a.nama,a.tanggal_lahir,
					a.awal_kontrak,
					a.contract_period_month,
					a.contract_period_day,
					a.akhir_kontrak,
					a.participant_no,
					a.uw_type,
					c.product_name,
					a.spa_no
				from life_grup_policy_participant a
				left join life_grup_policy b on a.spa_no=b.spa_no
				left join life_product c on c.product_code=b.product_code
				where a.id = ?";
		$query = $this->db->query($sql,array($id));
		return $query->row_array();
	}

	function getDataBenefitParticipantByNo($participant_no,$spa_no)
	{
		$sql = "select
					b.benefit_name,b.description,
					a.benefit_value,a.kontribusi
				from life_grup_policy_participant_benefit a
				left join life_benefit b on a.benefit_code=b.benefit_code
				where a.participant_no = ? and a.spa_no=?";
		$query = $this->db->query($sql,array($participant_no,$spa_no));
		return $query->result_array();
	}

	function group_data_spaj_by_keyword($keyword)
	{
		$param=array();
		$sql = "select
					a.spa_no,
					b.nama as lembaga,
					d.uw_policy_code,
					d.uw_policy_name,
					a.ujroh,
					a.manajemen_fee,
					(select count(*) from life_grup_policy_nota_teknik where spa_no=a.spa_no) as is_exists
				from life_grup_policy a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				left join uw_policy d on d.uw_policy_code=c.uw_policy_code
				where (UPPER(a.spa_no) like ? OR UPPER(b.nama) like ?) ";
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		if ($this->_branch_code!="00") {
			$sql .= " and b.branch_code=?";
			$param[] = $this->_branch_code;
		}
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function GetBenefitBySpaNo($spa_no)
	{
		$sql = "select
				b.benefit_code,
				b.benefit_name,
				c.premium_rate_type,
				c.premium_rate_code,
				c.premium_rate_value
				from life_grup_policy_benefit a
				inner join life_benefit b on b.benefit_code=a.benefit_code
				inner join life_grup_policy d on a.spa_no=d.spa_no
				inner join life_product_benefit c on c.benefit_code=b.benefit_code and c.product_code=d.product_code
				where a.spa_no=?
			   ";
		$query = $this->db->query($sql,array($spa_no));
		return $query->result_array();
	}

	function GetRateTunggal($benefit_code)
	{
		$sql = "select
				a.rate_code,
				a.description
				from life_premium_rate a
				left join life_product_benefit b on b.premium_rate_code=a.rate_code
				where b.benefit_code=?
			   ";
		$query = $this->db->query($sql,array($benefit_code));
		return $query->result_array();
	}

	function getImportDataPesertaNumUnprocessed($import_id)
	{
		$sql = "select count(*) jml from life_grup_policy_participant_import_data where import_id=? and status_proses='0'";
		$query = $this->db->query($sql,array($import_id));
		$row = $query->row_array();
		return $row['jml'];
	}

	// BEGIN COUNTER Policy
	function getCounterSequencePolicy($thn,$bln,$spa_no)
	{
		$sql = "select sequence from life_counter_polis
				where year=? and month=? and spa_no=?";
		$query = $this->db->query($sql,array($thn,$bln,$spa_no));
		$row = $query->row_array();
		if (count($row)>0) {
			return $row['sequence'];
		} else {
			$this->insertNewCounterPolicy($thn,$bln,$spa_no);
		}
	}

	function updateCounterPolicy($thn,$bln,$spa_no,$current_sequence)
	{
		$data = array('sequence'=>$current_sequence+1);
		$param = array('year'=>$thn,'month'=>$bln,'spa_no'=>$spa_no);
		$this->db->trans_begin();
		$this->db->update('life_counter_polis',$data,$param);
		if ($this->db->trans_status()===true) {
			$this->db->trans_commit();
		} else {
			$this->db->trans_rollback();
		}
	}

	function insertNewCounterPolicy($thn,$bln,$spa_no)
	{
		$data = array('year'=>$thn,'month'=>$bln,'spa_no'=>$spa_no,'sequence'=>0);
		$this->db->trans_begin();
		$this->db->insert('life_counter_polis',$data);
		if ($this->db->trans_status()===true) {
			$this->db->trans_commit();
		} else {
			$this->db->trans_rollback();
		}
	}

	function get_ujroh_th1_by_product_code($product_code)
	{
		$sql = "select ujroh from life_product_ujroh where product_code=? and tahunke=1";
		$query = $this->db->query($sql,array($product_code));
		$row = $query->row_array();
		if (count($row)==0) {
			return 0;
		} else {
			return $row['ujroh'];
		}
	}
	// END

	function GetManajemenFee($spa_no) {
		$sql = "SELECT  (case when coalesce(a.manajemen_fee,0) > 0 then a.manajemen_fee else
							(case when coalesce(b.manajemen_fee,0) > 0 then coalesce(b.manajemen_fee,0) else coalesce(c.manajemen_fee,0) end)
						end) as manajemen_fee
				FROM life_grup_policy a
				LEFT JOIN life_grup_policy_nota_teknik b ON a.spa_no=b.spa_no
				LEFT JOIN life_product c ON c.product_code=a.product_code
				WHERE a.spa_no=?
		";
		$query = $this->db->query($sql,array($spa_no));
		$row = $query->row_array();
		if (count($row)>0) {
			return $row['manajemen_fee'];
		} else {
			return 0;
		}
	}

	function group_participant_klaim_by_keyword($keyword,$policy_no)
	{
		$param = array();
		$sql = "SELECT
				a.id as id_participant,
				a.participant_no,
				a.nama,
				a.alamat,
				a.alamat_kota,
				a.alamat_kodepos,
				a.no_telp_seluler,
				a.tanggal_lahir,
				a.jenis_kelamin,
				a.awal_kontrak,
				a.akhir_kontrak,
				a.total_benefit,
				a.total_premi,
				b.alamat,
				b.kota,
				b.kodepos,
				b.telpon,
				b.tlp_seluler_pejabat,
				b.nama_pejabat,
				b.jabatan,
				a.status_cover,
				a.status_uw,
				d.tanggal_pengajuan tanggal_pengajuan_claim,
				e.tanggal_pengajuan tanggal_pengajuan_refund
				from life_grup_policy_participant a
				inner join life_grup_policy c on c.spa_no=a.spa_no
				inner join life_grup_cif b on b.cif_no=c.cif_no
				LEFT JOIN life_grup_claim d ON d.spa_no=a.spa_no AND d.participant_no=a.participant_no
				LEFT JOIN life_grup_refund e ON e.participant_id=a.id
				where c.policy_no = ? AND a.payment_status=1
				";
				// and a.status_cover !='2'

			$param[] = $policy_no;

		if($keyword!=''){
			$sql .= " and (UPPER(a.participant_no) like ? OR UPPER(a.nama) like ?)";
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		}
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function get_nilai_klaim($id_participant,$tgl_valuasi)
	{
		$sql = "select coalesce(fn_get_ots_manfaat(?,?),0) nilai_klaim";
		$query = $this->db->query($sql,array($id_participant,$tgl_valuasi));
		$row = $query->row_array();
		return $row['nilai_klaim'];
	}

	function group_policy_klaim_by_keyword($keyword)
	{
		$sql = "SELECT
				a.cif_no,
				a.policy_no,
				a.agen_code,
				a.spa_no,
				a.ujroh,
				a.flag_administrasi_peserta,
				b.nama as lembaga,
				c.product_code,
				b.model_cabang,
				b.cif_no,
				c.product_name,
				(case when coalesce(d.manajemen_fee,0) > 0 then d.manajemen_fee else 
					(case when coalesce(a.manajemen_fee,0) > 0 then a.manajemen_fee else c.manajemen_fee end)
				end) as manajemen_fee,
				c.minimal_premi,
				TRIM(e.nama_bank) nama_bank,
				TRIM(e.cabang_bank) cabang_bank,
				TRIM(e.no_rekening) no_rekening,
				TRIM(e.atas_nama) atas_nama,
				max(e.created_date)
				from life_grup_policy a
				inner join life_grup_cif b on a.cif_no=b.cif_no
				inner join life_product c on c.product_code=a.product_code
				left join life_grup_policy_nota_teknik d ON d.spa_no = a.spa_no
				left join life_grup_claim e ON e.spa_no = a.spa_no
				where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?)			
		";
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		if ($this->_branch_code!="00") {
			$sql .= "and b.branch_code=?";
			$param[] = $this->_branch_code;
		}
		
		$sql .= " GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 ";

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function fn_get_akumulasi_manfaat($nama,$tanggal_lahir)
	{
		$sql = "select coalesce(fn_get_akumulasi_manfaat(?,?),0) akumulasi_manfaat";
		$query = $this->db->query($sql,array($nama,$tanggal_lahir));
		$row = $query->row_array();
		return $row['akumulasi_manfaat'];
	}

	function policy_participant_by_keyword($policy_no,$keyword)
	{
		$sql = "select
					a.id,
					a.spa_no,
					a.participant_no,
					a.nama
				from life_grup_policy_participant a
				left join life_grup_policy b on a.spa_no=b.spa_no
				where b.policy_no=?
				and (UPPER(a.participant_no) like ? OR UPPER(a.nama) like ?)
				order by a.nama asc
				";
		$query = $this->db->query($sql,array($policy_no,'%'.strtoupper(strtolower($keyword)).'%','%'.strtoupper(strtolower($keyword)).'%'));
		return $query->result_array();
	}

	function getParticipantIdBySpaParticipantNo($spa_no,$participant_no)
	{
		$sql = "select id from life_grup_policy_participant where spa_no=? and participant_no=?";
		$query = $this->db->query($sql,array($spa_no,$participant_no));
		$row = $query->row_array();
		if (isset($row['id'])) {
			return $row['id'];
		} else {
			return NULL;
		}
	}

	function cek_menu_is_exists($url)
	{
		$sql = "select count(*) jml from m_menu where upper(replace(menu_url,'/','')) = ?";
		$query = $this->db->query($sql,array($url));
		$row = $query->row_array();
		if ($row['jml']>0) {
			return true;
		} else {
			return false;
		}
	}

	function get_menu_id_by_url($url)
	{
		$sql = "select menu_id from m_menu where upper(replace(menu_url,'/','')) = ?";
		$query = $this->db->query($sql,array($url));
		$row = $query->row_array();
		if (isset($row['menu_id'])) {
			return $row['menu_id'];
		} else {
			return false;
		}
	}

	function cek_url_is_allowed($menu_id,$role_id)
	{
		$sql = "select count(*) jml from m_user_nav where menu_id = ? and role_id = ?";
		$query = $this->db->query($sql,array($menu_id,$role_id));
		$row = $query->row_array();
		if ($row['jml']>0) {
			return true;
		} else {
			return false;
		}
	}

	function group_participant_settle_by_keyword($keyword,$policy_no)
	{
		$param = array();
		$sql = "select
				a.id as id_participant,
				a.spa_no,
				a.participant_no,
				a.nama,
				a.awal_kontrak,
				a.akhir_kontrak,
				a.total_benefit,
				a.total_premi,
				a.payment_date,
				a.payment_status,
				a.payment_by as settle_by_id,
				h.fullname as payment_by,
				f.nama_bank,
				d.id as bank_trx_detail_id,
				d.jumlah_respon,
				d.tgl_trx
				from life_grup_policy_participant a
				left join life_finance_bank_trx_participant b on b.id_participant=a.id
				left join life_finance_bank_trx_detail d on d.id = b.id_bank_trx
				left join life_finance_bank_trx c on c.id = d.import_id
				left join life_bank_account e on e.account_code = c.kode_rekening
				left join life_bank f on f.bank_code = e.bank_code
				left join life_grup_policy g on g.spa_no = a.spa_no
				inner join m_user h on h.user_id = a.payment_by::integer
				where g.policy_no = ? and a.status_uw = '1' and a.payment_status = '1'
			   ";
				// where g.policy_no = ? and a.status_uw = '1' and a.status_cover !='2' and a.payment_status = '1'

			$param[] = $policy_no;

		if($keyword!=''){
			$sql .= " and (UPPER(a.participant_no) like ? OR UPPER(a.nama) like ?)";
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		}
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_policy_settle_by_keyword($keyword)
	{
		$sql = "SELECT
				a.cif_no,
				a.policy_no,
				a.agen_code,
				a.spa_no,
				a.flag_administrasi_peserta,
				b.nama as lembaga,
				c.product_code,
				b.model_cabang,
				b.cif_no,
				c.product_name,
				(case when coalesce(d.manajemen_fee,0) > 0 then d.manajemen_fee else 
					(case when coalesce(a.manajemen_fee,0) > 0 then a.manajemen_fee else c.manajemen_fee end)
				end) as manajemen_fee,
				c.minimal_premi,
				e.nama_bank,
				e.cabang_bank,
				e.no_rekening,
				e.atas_nama
				from life_grup_policy a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				left join life_grup_policy_nota_teknik d ON d.spa_no = a.spa_no
				left join life_grup_claim e ON e.spa_no = a.spa_no
				where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?)
				
		";
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		// if ($this->_branch_code!="00") {
		// 	$sql .= "and b.branch_code=?";
		// 	$param[] = $this->_branch_code;
		// }


        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses


		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function getPesertaByNama($spa_no,$nama)
	{
		$sql = "
			select
				tanggal_lahir,
				jenis_kelamin,contract_period_month,
				contract_period_day,awal_kontrak,akhir_kontrak,
				total_benefit
			from life_grup_policy_participant
			where spa_no=? and upper(nama) = ?
			ORDER BY awal_kontrak DESC LIMIT 1
		";
		$query = $this->db->query($sql,array($spa_no,strtoupper(strtolower($nama))));
		return $query->row_array();
	}

	function group_participant_for_refund_by_keyword($keyword,$policy_no)
	{
		$param = array();
		$sql = "SELECT
				a.id as id_participant
				,a.participant_no
				,a.nama
				,a.contract_period_month
				,a.alamat
				,a.alamat_kota
				,a.alamat_kodepos
				,a.no_telp_seluler
				,a.tanggal_lahir
				,a.jenis_kelamin
				,a.awal_kontrak
				,a.akhir_kontrak
				,a.total_benefit
				,a.total_premi
				,b.alamat
				,b.kota
				,b.kodepos
				,b.telpon
				,b.tlp_seluler_pejabat
				,b.nama_pejabat
				,b.jabatan
				,(a.total_premi*c.ujroh/100) as ujroh
				,(a.total_premi*c.manajemen_fee/100) as manajemen_fee
				,a.status_cover
				,d.tanggal_pengajuan tanggal_pengajuan_claim
				,e.tanggal_pengajuan tanggal_pengajuan_refund
				,a.status_uw
				from life_grup_policy_participant a
				inner join life_grup_policy c on c.spa_no=a.spa_no
				inner join life_grup_cif b on b.cif_no=c.cif_no
				LEFT JOIN life_grup_claim d ON d.spa_no=a.spa_no AND d.participant_no=a.participant_no
				LEFT JOIN life_grup_refund e ON e.participant_id=a.id
				where c.policy_no = ? AND a.payment_status=1

				";

			$param[] = $policy_no;

		if($keyword!=''){
			$sql .= " and (UPPER(a.participant_no) like ? OR UPPER(a.nama) like ?)";
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		}
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_nota_by_keyword($keyword)
	{
		$sql = "SELECT
				a.no_nota,
				c.nama as lembaga,
				d.product_name,
				b.policy_no
				from life_nota_tagihan a
				left join life_grup_policy b on b.policy_no=a.policy_no
				left join life_grup_cif c on c.cif_no=b.cif_no
				left join life_product d on d.product_code=b.product_code
				where (UPPER(a.no_nota) like ?)";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		// if ($this->_branch_code!="00") {
		// 	$sql .= " and b.branch_code=?";
		// 	$param[] = $this->_branch_code;
		// }

        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND b.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND c.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND c.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND c.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses


		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_policy_migrasi_by_keyword($keyword)
	{
		$sql = "select * from mgr_master_policy where (upper(policy_no) like ? or upper(policy_name) like ?)";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_policy_active_by_keyword($keyword)
	{
		$sql = "SELECT
				a.cif_no,
				a.policy_no,
				a.agen_code,
				a.spa_no,
				a.flag_administrasi_peserta,
				b.nama as lembaga,
				c.product_code,
				b.model_cabang,
				b.cif_no,
				c.product_name,
				(case when coalesce(d.manajemen_fee,0) > 0 then d.manajemen_fee else 
					(case when coalesce(a.manajemen_fee,0) > 0 then a.manajemen_fee else c.manajemen_fee end)
				end) as manajemen_fee,
				c.minimal_premi
				from life_grup_policy a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				left join life_grup_policy_nota_teknik d ON d.spa_no = a.spa_no
				where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?) and a.status_aktif = '1' ";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		// if ($this->_branch_code!="00") {
		// 	$sql .= " and b.branch_code=?";
		// 	$param[] = $this->_branch_code;
		// }

        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses


		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_data_polis_by_keyword($keyword)
	{
		$param=array();
		$sql = "SELECT
				a.spa_no,
				a.policy_no,
				b.nama as pemegang_polis,
				d.uw_policy_code,
				d.uw_policy_name,
				a.ujroh,
				a.manajemen_fee,
				c.product_code,
				c.product_name,
				e.no_agen as agen_code,
				e.nama as agen_name,
				(select count(*) from life_grup_policy_nota_teknik where spa_no=a.spa_no) as is_exists
				from life_grup_policy a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				left join uw_policy d on d.uw_policy_code=c.uw_policy_code
				left join life_agen e on e.no_agen=a.agen_code
				where a.status_aktif=1
				and (UPPER(a.spa_no) like ? OR UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?) 
			   ";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		
        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_participant_for_refundregis_by_keyword($keyword,$policy_no)
	{
		$param = array();
		$sql = "SELECT
				a.id as id_participant
				,a.participant_no
				,a.nama
				,a.alamat
				,a.alamat_kota
				,a.alamat_kodepos
				,a.no_telp_seluler
				,a.tanggal_lahir
				,a.jenis_kelamin
				,a.awal_kontrak
				,a.akhir_kontrak
				,a.total_benefit
				,a.total_premi
				,b.alamat
				,b.kota
				,b.kodepos
				,b.telpon
				,b.tlp_seluler_pejabat
				,b.nama_pejabat
				,b.jabatan
				,(a.total_premi*c.ujroh/100) as ujroh
				,(a.total_premi*c.manajemen_fee/100) as manajemen_fee
				from life_grup_policy_participant a
				inner join life_grup_policy c on c.spa_no=a.spa_no
				inner join life_grup_cif b on b.cif_no=c.cif_no
				where c.policy_no = ? and a.status_uw = '1' and a.status_cover='1'
				and a.id not in (SELECT participant_id FROM life_grup_refund x WHERE x.participant_id=a.id )
				";

			$param[] = $policy_no;

		if($keyword!=''){
			$sql .= " and (UPPER(a.participant_no) like ? OR UPPER(a.nama) like ?)";
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		}
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function individu_policy_active_by_keyword($keyword)
	{
		$sql = "SELECT
				 a.id as policy_id
				,a.spa_no
				,a.policy_no
				,d.product_code
				,d.product_name
				,c.id as cif_id
				,c.cif_no
				,c.nama
				,a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,e.rekening_bank
				,e.rekening_no
				,e.rekening_nama
				,e.rekening_cabang
				,coalesce(f.biaya_freelook,0) biaya_freelook
				,coalesce(f.biaya_surrender,0) biaya_surrender
				,a.tgl_akseptasi::date
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			INNER JOIN life_product d ON a.product_code=d.product_code /*2017-05-08 left join karena belum ada setting di life_product*/
			LEFT JOIN life_individu_policy_detail e ON e.policy_id=a.id
			LEFT JOIN life_product_individu f ON f.product_code=a.product_code
			WHERE (upper(a.policy_no) like ? OR upper(c.nama) like ?) and a.policy_no is not null
			GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 /*digrup karna ada beberapa polis yang flag_cif 0 nya double*/
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function individu_policy_active_by_keyword_for_claim_saving($keyword)
	{
		$sql = "SELECT
				 a.id as policy_id
				,a.spa_no
				,a.policy_no
				,d.product_code
				,d.product_name
				,c.id as cif_id
				,c.cif_no
				,c.nama
				,a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,e.rekening_bank
				,e.rekening_no
				,e.rekening_nama
				,e.rekening_cabang
				,coalesce(f.biaya_freelook,0) biaya_freelook
				,coalesce(f.biaya_surrender,0) biaya_surrender
				,a.tgl_akseptasi::date
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			INNER JOIN life_product d ON a.product_code=d.product_code /*2017-05-08 left join karena belum ada setting di life_product*/
			LEFT JOIN life_individu_policy_detail e ON e.policy_id=a.id
			LEFT JOIN life_product_individu f ON f.product_code=a.product_code
			WHERE (upper(a.policy_no) like ? OR upper(c.nama) like ?) and a.policy_no is not null
			AND d.flag_insurance!='3' /*20180328 adesagita, untuk produk unitlink tidak diambil*/
			AND a.status_polis in ('1','9','6','11','12') /*20180731 hanya yang aktif dan registrasi klaim. 20180914 yang lapse otomatis juga bisa saving*/

			GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 /*digrup karna ada beberapa polis yang flag_cif 0 nya double*/
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function individu_dana_investasi($policy_id)
	{
		$sql = "SELECT
				b.fund_code,
				b.fund_name,
				b.keterangan,
				coalesce(d.alokasi,0) as alokasi,
				coalesce(fn_get_saldo_unit(c.policy_no,b.fund_code,CURRENT_DATE),0) saldo_unit,
				coalesce(fn_get_nab_buy(b.fund_code),0) as nab_buy,
				coalesce(fn_get_nab_sell(b.fund_code),0) as nab_sell
			FROM life_product_fund a
			LEFT JOIN life_fund b ON b.fund_code=a.fund_code
			LEFT JOIN life_individu_policy c ON c.product_code=a.product_code
			LEFT JOIN life_individu_policy_fund d ON d.policy_id=c.id AND d.fund_code=b.fund_code
			WHERE d.policy_id=?
			ORDER BY a.sequence ASC
		";
		$query = $this->db->query($sql,array($policy_id));
		return $query->result_array();
	}

	function individu_saldo_dana_investasi($policy_no,$trx_date)
	{
		// $sql = "
		// SELECT 
		// 	fund_code
		// 	,fund_name
		// 	,jml_unit
		// 	,coalesce((jml_unit*harga_beli),0) as jml_nominal
		// 	,coalesce(harga_jual,0) as harga_jual
		// 	,coalesce(harga_beli,0) as harga_beli
		// FROM (
		// 	SELECT
		// 		b.fund_code,
		// 		b.fund_name,
		// 		(select max(trunc(random() * 10000)) from generate_series(a.sequence,5)) + 1 as jml_unit,
		// 		--1000 as jml_unit,
		// 		fn_get_nab_buy(b.fund_code,?) as harga_jual,
		// 		fn_get_nab_sell(b.fund_code,?) as harga_beli
		// 	FROM life_product_fund a, life_fund b, life_individu_policy c, life_individu_policy_fund d
		// 	WHERE a.fund_code=b.fund_code and c.policy_no=? and c.id=d.policy_id and b.fund_code=d.fund_code
		// 	ORDER BY a.sequence ASC
		// ) AS saldo
		// ";
		//
		//-- coalesce((d.saldo/d.last_nab_value),0) as jml_unit,
		//-- coalesce(d.saldo,0) as jml_nominal
		//
		$sql = "
			SELECT
				c.fund_code,
				c.fund_name,
				(fn_get_saldo_unit(b.policy_no,c.fund_code,CURRENT_DATE)) as jml_unit,
				(fn_get_saldo_unit(b.policy_no,c.fund_code,CURRENT_DATE)*fn_get_nab_buy(c.fund_code)) as jml_nominal
			FROM life_individu_policy_fund a
			LEFT JOIN life_individu_policy b ON b.id=a.policy_id
			LEFT JOIN life_fund c ON c.fund_code=a.fund_code
			LEFT JOIN life_individu_policy_sa_balance d ON d.policy_no=b.policy_no AND d.fund_code=a.fund_code
			WHERE b.policy_no=? and a.alokasi > 0
		";
		$query = $this->db->query($sql,array($policy_no));
		return $query->result_array();
	}

	function group_policy_saving_active_by_keyword($keyword)
	{
		$sql = "SELECT
				a.cif_no,
				a.policy_no,
				a.agen_code,
				a.spa_no,
				a.flag_administrasi_peserta,
				b.nama as lembaga,
				c.product_code,
				b.model_cabang,
				b.cif_no,
				c.product_name,
				a.manajemen_fee,
				c.minimal_premi
				from life_grup_policy_saving a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				left join life_grup_policy_nota_teknik_saving d ON d.spa_no = a.spa_no
				where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?)";
				// where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?) and a.status_aktif = '1' ";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		// if ($this->_branch_code!="00") {
		// 	$sql .= " and b.branch_code=?";
		// 	$param[] = $this->_branch_code;
		// }

        //cek session user akses
        // $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        // $user_id = $this->session->userdata('id');
        // if($flag_akses_polis!='0'){ // 0=super akses
        //     if($flag_akses_polis=='1'){ //jika akses polis
        //         $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
        //     }else if($flag_akses_polis=='2'){ //jika akses branch
        //         $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
        //     }else if($flag_akses_polis=='3'){ //jika akses cif
        //         $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
        //     }else if($flag_akses_polis=='4'){ //jika akses cif grup
        //         $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
        //     }
        // }
        //end cek session user akses


		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function informasi_polis_by_policy_id($policy_id)
	{
		$sql = "
			select
				c.*,
				b.flag_participant_sama
			from life_individu_policy_cif a
			left join life_individu_policy b on b.id=a.policy_id
			left join life_individu_cif c on c.cif_no=a.cif_no
			where a.flag_cif='0' and a.policy_id=?
		";

		$query = $this->db->query($sql,$policy_id);
		return $query->row_array();
	}
	/*
	* NEW PAK SITHOT
	* Untuk Menghitung Masa
	*/

	function GetManajemenFeeSaving($spa_no) {
		$sql = "SELECT manajemen_fee
				FROM life_grup_policy_saving a
				WHERE a.spa_no=?
		";
		$query = $this->db->query($sql,array($spa_no));
		$row = $query->row_array();
		if (count($row)>0) {
			return $row['manajemen_fee'];
		} else {
			return 0;
		}
	}

	function get_faktor_manfaat_single($spa_no,$benefit_code)
	{
		$sql = "
			SELECT
				b.faktor_manfaat
			FROM life_grup_policy_nota_teknik_saving a
			LEFT JOIN life_grup_policy_nota_teknik_saving_benefit b ON a.id=b.notateknik_id
			WHERE a.spa_no=? AND b.benefit_code=?
			AND flag_benefit_value = 1
		";
		$query = $this->db->query($sql,array($spa_no,$benefit_code));
		$row = $query->row_array();
		if (count($row)>0) {
			return $row['faktor_manfaat'];
		} else {
			return 0;
		}
	}

	function fn_get_faktormanfaat($spa_no,$jenis,$benefit_code,$masakerja)
	{
		$sql = "
			SELECT fn_get_faktormanfaat(?,?,?,?) as fm
		";
		$query = $this->db->query($sql,array($spa_no,$jenis,$benefit_code,$masakerja));
		$row = $query->row_array();
		if (count($row)>0) {
			return $row['fm'];
		} else {
			return 0;
		}
	}

	function get_manfaat_saving($spa_no,$benefit_code)
	{
		$sql = "
			SELECT
				b.benefit_value
			FROM life_grup_policy_nota_teknik_saving a
			LEFT JOIN life_grup_policy_nota_teknik_saving_benefit b ON a.id=b.notateknik_id
			WHERE a.spa_no=? AND b.benefit_code=?
			AND flag_benefit_value = 2
		";
		$query = $this->db->query($sql,array($spa_no,$benefit_code));
		$row = $query->row_array();
		if (count($row)>0) {
			return $row['benefit_value'];
		} else {
			return 0;
		}
	}
   	function getPensiun($tgl_lahir,$usia_pensiun)
   	{
		if($usia_pensiun=='' || $usia_pensiun==NULL) $usia_pensiun=0;
	    $date = new DateTime($tgl_lahir);
        $interval = new DateInterval('P'.$usia_pensiun.'Y'); 
        return $date->add($interval);
   	}
   
   	function masaKontrak($tgl_pensiun,$tgl_awal)
   	{
        $tgl_awal = new DateTime($tgl_awal);
        $interval = $tgl_pensiun->diff($tgl_awal);
	    return $interval;
   	}

   	function masaLalu($tgl_kerja,$tgl_awal) 
   	{
        $tgl_kerja = new DateTime($tgl_kerja);
        $tgl_awal = new DateTime($tgl_awal);
        $interval = $tgl_kerja->diff($tgl_awal);
        return $interval;
   	}

   	function backService($tgl_kerja, $tgl_valuasi)
   	{
      $tgl_kerja = new DateTime($tgl_kerja);
      $tgl_valuasi = new DateTime($tgl_valuasi);
      $interval = $tgl_kerja->diff($tgl_valuasi);
      $bs = $interval->days;
      $bs = round($bs/365.25);
      return $bs;
   	}

   	function currentService($tgl_lahir, $usia_pensiun, $valuasi) 
   	{
      $tgl_pensiun = $this->getPensiun($tgl_lahir, $usia_pensiun);
      $masa_depan = $this->masaKontrak($tgl_pensiun, $valuasi);
      $masa_depan = round($masa_depan->days/365.25);
      return $masa_depan;
   	}

	function group_policy_saving_klaim_by_keyword($keyword)
	{
		$sql = "select
				a.policy_no,
				a.spa_no,
				b.nama,
				b.cif_no,
				c.product_code,
				c.product_name
				from life_grup_policy_saving a,
				life_grup_cif b,
				life_product c
				where b.cif_no = a.cif_no
				and c.product_code = a.product_code
				and (upper(a.policy_no) like ? or upper(b.nama) like ?)		
				and a.flag_approval = '1'	
			   ";
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		if ($this->_branch_code!="00") {
			$sql .= "and b.branch_code=?";
			$param[] = $this->_branch_code;
		}
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_participant_saving_klaim_tunai_keyword($keyword,$policy_no)
	{
		$param = array();
		$sql = "select
				a.participant_no,
				a.nama,
				a.tanggal_lahir,
				a.awal_kontrak,
				a.akhir_kontrak,
				a.total_benefit,
				a.total_premi,
				b.saldo
				from life_grup_policy_participant_saving a,
				life_grup_policy_participant_saving_balance b,
				life_grup_policy_saving c
				where a.id = b.id_participant
				and c.spa_no = a.spa_no
				and c.policy_no = ? AND a.payment_status=1
				";

			$param[] = $policy_no;

		if($keyword!=''){
			$sql .= " and (UPPER(a.participant_no) like ? OR UPPER(a.nama) like ?)";
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		}
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function group_policy_bersama_by_keyword($keyword)
	{
		$sql = "SELECT
				a.cif_no,
				a.policy_no,
				a.agen_code,
				e.nama as agen_name,
				e.upline_agen,
				a.spa_no,
				a.flag_administrasi_peserta,
				b.nama as lembaga,
				c.product_code,
				b.model_cabang,
				b.cif_no,
				b.branch_code,
				c.product_name,
				(case when coalesce(d.manajemen_fee,0) > 0 then d.manajemen_fee else 
					(case when coalesce(a.manajemen_fee,0) > 0 then a.manajemen_fee else c.manajemen_fee end)
				end) as manajemen_fee,
				c.minimal_premi
				from life_grup_policy a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				left join life_grup_policy_nota_teknik d ON d.spa_no = a.spa_no
				inner join life_agen e ON e.no_agen = a.agen_code
				inner join life_product_coinsurance f on f.product_code = a.product_code
				where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?) and a.status_aktif = '1' ";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		// if ($this->_branch_code!="00") {
		// 	$sql .= " and b.branch_code=?";
		// 	$param[] = $this->_branch_code;
		// }

        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses


		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}
   	/*
		[START KEYWORD POLICY MANFEE]
		@MLH 25 Oktober 2016 
   	*/
	function group_policy_by_keyword_manfee($keyword)
	{
		$sql = "SELECT
				d.cif_no,
				a.policy_no,
				d.agen_code,
				d.spa_no,
				b.nama as lembaga,
				c.product_code,
				b.model_cabang,
				b.cif_no,
				c.product_name,
				c.minimal_premi
				from life_grup_policy_manfee a
				inner join life_grup_policy d on d.policy_no = a.policy_no
				left join life_grup_cif b on d.cif_no=b.cif_no
				left join life_product c on c.product_code=d.product_code
				where (UPPER(d.policy_no) like ? OR UPPER(b.nama) like ?) and d.status_aktif = '1' ";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		// if ($this->_branch_code!="00") {
		// 	$sql .= " and b.branch_code=?";
		// 	$param[] = $this->_branch_code;
		// }

        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses


		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}
	/*
		[END KEYWORD]
	*/

	function get_usia_type_by_product($product_code)
	{
		$sql = "SELECT coalesce(usia_type,0) usia_type from life_product where product_code = ?"; /*Ade Sagita 20181012 dicoalesce supaya result tidak null*/
		$query = $this->db->query($sql,array($product_code));
		$row = $query->row_array();
		if (isset($row['usia_type'])) {
			return $row['usia_type'];
		} else {
			return null;
		}
	}

	function getUsiaAsuransiIndividu($date,$date2='')
	{
		if ($date2=='') {
			$date2 = date('Y-m-d');
		}
		$sql = "select fn_get_usia(?,?) as usia";
		$query = $this->db->query($sql,array($date,$date2));
		return $query->row_array();
	}

	public function getJtempoPolisIndividu($n,$interval)
	{
		// $n = '2017-01-31';
		$time = strtotime($n);
		// echo $n;
		// echo "<br/>";
		$final = date("Y-m-d", strtotime("+$interval month", $time));
		return $final;
	}

	function get_pejabat_by_code_value($code_value='') /*untuk pejabat tanda tangan. pejabat|jabatan */
	{
		$param = array();
		$sql = "SELECT display_text FROM life_list_code_detail 
				WHERE UPPER(code_value)=? ";
		$param[] 	= strtoupper($code_value);

		$query = $this->db->query($sql,$param);
		$data = $query->row_array();
		if (count($data)>0) {
			return $data['display_text'];
		}else{
			return 'n/a|n/a';
		}
	}

	function get_potongan_transfer($nominal='0') /*untuk mendapatkan potongan transfer */
	{
		$param = array();
		$sql = "SELECT display_sort from life_list_code_detail 
				WHERE code_group='potongan_transfer'
				AND ? BETWEEN code_value::numeric AND display_text::numeric 
				";
		$param[] = $nominal;

		$query = $this->db->query($sql,$param);
		$data = $query->row_array();
		if (isset($data['display_sort'])) {
			return $data['display_sort'];
		}else{
			return '0';
		}
	}

	public function get_data_pejabat_ttd($jumlah_persetujuan_klaim,$code)
	{
		$param = array();
		$sql = "SELECT * from life_pejabat_ttd where ( ? between nominal1 and nominal2) and (code = ?) ";
		$param[] = $jumlah_persetujuan_klaim;
		$param[] = $code;
		$query = $this->db->query($sql,$param);
		return $query->row_array();
	}

	function individu_pemegang_polis($keyword)
	{
		$sql = "select
				c.nama
				from life_individu_policy a
				inner join life_individu_policy_cif b on b.policy_id = a.id
				inner join life_individu_cif c on c.cif_no = b.cif_no
				where (upper(c.nama) like ?)
			  ";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$sql .= " group by 1 order by 1 asc";

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function get_display_text_by_code_value($code_value='')
	{
		$param = array();
		$sql = "SELECT display_text from life_list_code_detail 
				WHERE UPPER(code_value)=?
				";
		$param[] = strtoupper(trim($code_value));
		$query = $this->db->query($sql,$param);
		$data = $query->row_array();
		if (isset($data['display_text'])) {
			return $data['display_text'];
		}else{
			return '-';
		}
	}

	/*[BEGIN] INFORMASI POLIS
	| @MLH 18 april 2017
	*/
	public function informasi_polis($policy_id,$policy_no,$cif_id) {
		$data['pemegang_polis'] = $this->informasi_polis_customer($policy_id,'0');
		$data['peserta'] = $this->informasi_polis_customer($policy_id,'1');
		$data['peyor'] = $this->informasi_polis_customer($policy_id,'2');
		$data['pasangan'] = $this->informasi_polis_customer($policy_id,'4');
		$data['vpasangan'] = count($data['pasangan']);
		$data['product'] = $this->informasi_polis_product($policy_id);
		$data['tambahan'] = $this->informasi_polis_informasi_tambahan($policy_id);
		$data['kesehatan'] = $this->informasi_polis_kesehatan($policy_id,'1');
		$data['kesehatan_payor'] = $this->informasi_polis_kesehatan($policy_id,'2');
		$data['kesehatan_pasangan'] = $this->informasi_polis_kesehatan($policy_id,'4');
		$data['fund'] = $this->informasi_polis_fund($policy_id);
		$data['benefit'] = $this->informasi_polis_benefit($cif_id);
		$data['pmn_polis'] = $this->informasi_polis_PMN($policy_id,'0');
		$data['pmn_payor'] = $this->informasi_polis_PMN($policy_id,'2');
		$data['pajak_polis'] = $this->informasi_polis_PAJAK($policy_id,'0');
		if(count($data['peyor'])>0){
			$data['bidang_usaha'] = $this->get_list_code_detail('bidang_usaha',$data['peyor']['kantor_bidang_usaha']);
		}
		$data['bidang_usaha_polis'] = $this->get_list_code_detail('bidang_usaha',$data['pemegang_polis']['kantor_bidang_usaha']);
		$data['bidang_usaha_peserta'] = $this->get_list_code_detail('bidang_usaha',$data['peserta']['kantor_bidang_usaha']);
		if($data['vpasangan']>0){
			$data['bidang_usaha_pasangan'] = $this->get_list_code_detail('bidang_usaha',$data['pasangan']['kantor_bidang_usaha']);
		}
		$data['sumber_dana'] = $this->get_list_sumber_dana($policy_id);
		$data['polis_pertanyaan'] = $this->get_list_polis_pertanyaan($policy_id);
		return $data;
	}

	function informasi_polis_customer($policy_id,$flag_cif) {
		$sql = "
			SELECT
				c.*,
				b.flag_participant_sama,
				b.alamat_korespondensi,
				b.provinsi_korespondensi,
				b.kota_korespondensi,
				b.kode_pos_korespondensi,
				b.rt_m_korespondensi,
				b.rw_m_korespondensi,
				b.kelurahan_m_korespondensi,
				b.kecamatan_m_korespondensi,
				b.periode_bayar_investasi,
				b.payment_channel,
				b.periode_premi,
				a.hubungan_peserta,
				b.alamat_surat,
				b.alamat_pengiriman_polis
			from life_individu_policy_cif a
			left join life_individu_policy b on b.id=a.policy_id
			left join life_individu_cif c on c.cif_no=a.cif_no
			where a.flag_cif=? and a.policy_id=?
		";
		$query = $this->db->query($sql,array($flag_cif,$policy_id));
		return $query->row_array();
	}

	function informasi_polis_product($policy_id) {
		$sql = "
			SELECT
				a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,a.tgl_awal_premi
				,a.tgl_akhir_premi
				,a.masa_kontrak
				,a.masa_premi
				,a.periode_premi
				,a.periode_bayar_investasi
				,a.kontribusi_dasar
				,a.topup_reguler
				,a.topup_awal
				,a.kontribusi_dasar
				,a.manfaat
				,a.tanggal_mass_debet
				,d.display_text as payment_channel
				,b.akad_investasi
				,c.display_text as akad_investasi
				,a.asumsi_investasi
				,f.usia_masuk
			from life_individu_policy a
			left join life_product_individu b on b.product_code=a.product_code
			left join life_list_code_detail c on c.code_group='akad_product' and c.code_value=b.akad_investasi
			left join life_list_code_detail d on d.code_group='payment_channel' and d.code_value::integer=a.payment_channel
			left join life_product e on e.product_code=a.product_code
			left join life_individu_policy_cif f on f.policy_id = a.id
			where a.id=? and f.flag_cif = '1'
		";
		$query = $this->db->query($sql,array($policy_id));
		$row = $query->row_array();
		$row['benefit_utama'] = $this->informasi_polis_product_benefit($policy_id,'B');
		$row['benefit_tambahan'] = $this->informasi_polis_product_benefit($policy_id,'R');

		return $row;
	}

	// function informasi_polis_product_benefit($policy_id,$benefit_type) {
	// 	$sql = "
	// 		SELECT
	// 			c.benefit_code,
	// 			c.benefit_name,
	// 			c.description as benefit_description,
	// 			c.sequence,
	// 			f.jenis_plan,
	// 			(coalesce(fn_get_premium_rate(d.product_code,b.benefit_code,a.usia_masuk,d.masa_kontrak,e.jenis_kelamin,e.kelas_pekerjaan),0)) rate_tabarru,
	// 			coalesce(sum(
	// 				case when c.benefit_code = 'U003' 
	// 					then (d.kontribusi_dasar::numeric* 
	// 						(case when d.periode_premi=1 then 12
	// 							when d.periode_premi=2 then 4
	// 							when d.periode_premi=3 then 2
	// 							when d.periode_premi=4 then 1
	// 							when d.periode_premi=5 then 1 
	// 							else 0 end )*d.masa_premi::numeric - d.kontribusi_dasar)
	// 				when c.benefit_code = 'U011' 
	// 					then ((d.kontribusi_dasar::numeric+d.topup_reguler)* 
	// 						(case when d.periode_premi=1 then 12
	// 							when d.periode_premi=2 then 4
	// 							when d.periode_premi=3 then 2
	// 							when d.periode_premi=4 then 1
	// 							when d.periode_premi=5 then 1 
	// 							else 0 end )*d.masa_premi::numeric - (d.kontribusi_dasar+d.topup_reguler))
	// 				when c.benefit_code = 'U012' 
	// 					then ((d.kontribusi_dasar::numeric+d.topup_reguler)* 
	// 						(case when d.periode_premi=1 then 12
	// 							when d.periode_premi=2 then 4
	// 							when d.periode_premi=3 then 2
	// 							when d.periode_premi=4 then 1
	// 							when d.periode_premi=5 then 1 
	// 							else 0 end )*d.masa_premi::numeric - (d.kontribusi_dasar+d.topup_reguler))
	// 				when c.benefit_code = 'U013' 
	// 					then ((d.kontribusi_dasar::numeric+d.topup_reguler)* 
	// 						(case when d.periode_premi=1 then 12
	// 							when d.periode_premi=2 then 4
	// 							when d.periode_premi=3 then 2
	// 							when d.periode_premi=4 then 1
	// 							when d.periode_premi=5 then 1 
	// 							else 0 end )*d.masa_premi::numeric - (d.kontribusi_dasar+d.topup_reguler))
	// 				when c.benefit_code = 'U014' 
	// 					then ((d.kontribusi_dasar::numeric+d.topup_reguler)* 
	// 						(case when d.periode_premi=1 then 12
	// 							when d.periode_premi=2 then 4
	// 							when d.periode_premi=3 then 2
	// 							when d.periode_premi=4 then 1
	// 							when d.periode_premi=5 then 1 
	// 							else 0 end )*d.masa_premi::numeric - (d.kontribusi_dasar+d.topup_reguler))
	// 				else 
	// 					b.manfaat
	// 				end
	// 			),0) manfaat
	// 		from life_individu_policy_cif a
	// 		inner join life_individu_policy_cif_benefit b on a.id=b.policy_cif_id
	// 		inner join life_benefit c on c.benefit_code=b.benefit_code
	// 		inner join life_individu_policy d on d.id = a.policy_id
	// 		inner join life_individu_cif e on e.cif_no = a.cif_no
	// 		left join life_plan f on f.plan_code = b.plan_code
	// 		where a.policy_id=? and b.benefit_type=? 
	// 		group by 1,2,3,4,5,6
	// 		order by 4 asc
	// 	";
	// 	$query = $this->db->query($sql,array($policy_id,$benefit_type));
	// 	return $query->result_array();
	// }
	
	function informasi_polis_informasi_tambahan($policy_id) {
		// detail
		$sql = "SELECT * from life_individu_policy_detail where policy_id = ?";
		$query = $this->db->query($sql,array($policy_id));
		$data['detail'] = $query->row_array();

		// ahliwaris
		$sql = "SELECT * from life_individu_policy_ahliwaris where policy_id=? and flag_beasiswa <> '1' and hubungan <> '6'";
		$query = $this->db->query($sql,array($policy_id));
		$data['ahliwaris'] = $query->result_array();

		// formulir
		$sql = "select 
				a.dokumen_1,
				a.dokumen_2,
				a.dokumen_3,
				a.dokumen_4,
				a.dokumen_5
				from life_individu_master_spaj_detail a 
				inner join life_individu_master_spaj b on b.id = a.master_id
				inner join life_individu_policy c on c.spa_no = b.spa_no
				where c.id = ?
			   ";
		$query = $this->db->query($sql,array($policy_id));
		$data['dokumen'] = $query->row_array();

		return $data;
	}

	function informasi_polis_kesehatan($policy_id,$flag_cif) {
		$sql = "
			SELECT 
				e.question,
				d.answer,
				d.keterangan
			from life_individu_policy_cif a
			left join life_individu_policy b on b.id = a.policy_id
			left join life_individu_cif c on c.cif_no = a.cif_no
			left join life_individu_policy_cif_kesehatan d on d.policy_cif_id = a.id
			left join life_medical_question e on e.id = d.id_question
			where a.policy_id = ? and a.flag_cif = ?
		";
		$query = $this->db->query($sql,array($policy_id,$flag_cif));
		return $query->result_array();
	}

	function informasi_polis_fund($policy_id) {
		$sql = "
			SELECT
				c.fund_code,
				c.fund_name,
				c.keterangan,
				a.alokasi
			from life_individu_policy_fund a
			inner join life_product_fund b on b.fund_code = a.fund_code
			inner join life_fund c on c.fund_code = a.fund_code
			where a.policy_id = ?
			group by 1,2,3,4
			order by 1
		";
		$query = $this->db->query($sql,array($policy_id));
		return $query->result_array();
	}
	
	function informasi_polis_benefit($cif_id) {
		// basic
		$sql = "
				SELECT
					a.benefit_code,
					a.benefit_type,
					b.benefit_name,
					b.description,
					a.manfaat,
					a.kontribusi,
					a.rate_tabarru
				from life_individu_policy_cif_benefit a, life_benefit b
				where a.benefit_code = b.benefit_code
				and a.policy_cif_id = ? and a.benefit_type = 'B' order by b.sequence asc
				";
		$query = $this->db->query($sql,array($cif_id));
		$data['basic'] = $query->row_array();

		// rider
		$sql = "SELECT
					a.benefit_code,
					a.benefit_type,
					b.benefit_name,
					b.description,
					a.manfaat,
					a.kontribusi,
					a.rate_tabarru,
					d.nama,
					c.flag_cif
				from life_individu_policy_cif_benefit a, life_benefit b, life_individu_policy_cif c, life_individu_cif d
				where a.benefit_code = b.benefit_code and c.id = a.policy_cif_id and d.cif_no = c.cif_no
				and a.policy_cif_id = ? and a.benefit_type = 'R' order by b.sequence asc";
		$query = $this->db->query($sql,array($cif_id));
		$data['rider'] = $query->result_array();

		return $data;
	}

	function informasi_polis_PMN($policy_id,$flag_cif) {
		$sql = "SELECT 
				a.*
				from life_individu_policy_cif_pmn a
				left join life_individu_policy_cif b on b.id = a.policy_cif_id
				where b.policy_id = ? and b.flag_cif = ?
			   ";
		$query = $this->db->query($sql,array($policy_id,$flag_cif));
		return $query->result_array();
	}

	function informasi_polis_PAJAK($policy_id,$flag_cif) {
		$sql = "SELECT 
				a.*
				from life_individu_policy_cif_tax a
				left join life_individu_policy_cif b on b.id = a.policy_cif_id
				where b.policy_id = ? and b.flag_cif = ?
			   ";
		$query = $this->db->query($sql,array($policy_id,$flag_cif));
		return $query->result_array();
	}

	function get_list_code_detail($code_group,$value='')
	{
		$sql = "
			SELECT
				code_value,
				display_text,
				display_sort
			FROM life_list_code_detail
			WHERE code_group=?
		";	
		$param[] = $code_group;
		if ($value!="") {
			$sql .= " AND code_value=?";
			$param[] = $value;
		}
		$sql .= " ORDER BY display_sort ASC";

		$query = $this->db->query($sql,$param);

		if ($value=="") {
			return $query->result_array();
		} else {
			return $query->row_array();
		}
	}

	function get_list_sumber_dana($policy_id)
	{
		$param = array();
		$sql = "SELECT * FROM life_individu_policy_sumber_dana where policy_id = ?";
		$param[] = $policy_id;
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}
	/*[END] MODULE INFORMASI POLIS @MLH*/

	function get_msisdn_by_policy_no($policy_no) {
		$sql = "SELECT a.no_hp no_hp
				FROM life_individu_cif a 
				INNER JOIN life_individu_policy_cif b ON b.cif_no = a.cif_no
				INNER JOIN life_individu_policy c ON c.id = b.policy_id
				WHERE c.policy_no=? AND b.flag_cif='0'
				LIMIT 1
				";
		$query = $this->db->query($sql,array($policy_no));
		$data = $query->row_array();
		$retVal = (isset($data['no_hp'])) ? $data['no_hp'] : '0' ;
		return $retVal;
	}

	public function informasi_polis_danasiswa($policy_id,$policy_no,$cif_id) {
		$data['pemegang_polis'] = $this->informasi_polis_customer($policy_id,'0');
		$data['peserta'] = $this->informasi_polis_customer($policy_id,'1');
		$data['peyor'] = $this->informasi_polis_customer($policy_id,'2');
		$data['product'] = $this->informasi_polis_product($policy_id);
		$data['tambahan'] = $this->informasi_polis_informasi_tambahan($policy_id);
		$data['penerima_beasiswa'] = $this->informasi_polis_penerima_beasiswa($policy_id);
		$data['kesehatan'] = $this->informasi_polis_kesehatan($policy_id,'1');
		$data['kesehatan_payor'] = $this->informasi_polis_kesehatan($policy_id,'2');
		$data['kesehatan_pasangan'] = $this->informasi_polis_kesehatan($policy_id,'4');
		$data['benefit'] = $this->informasi_polis_benefit($cif_id);
		$data['pmn_peserta'] = $this->informasi_polis_PMN($policy_id,'1');
		$data['pmn_polis'] = $this->informasi_polis_PMN($policy_id,'0');
		$data['pajak_polis'] = $this->informasi_polis_PAJAK($policy_id,'0');
		$data['pmn_payor'] = $this->informasi_polis_PMN($policy_id,'2');
		return $data;
	}

	function informasi_polis_penerima_beasiswa($policy_id) {
		$sql = "SELECT * from life_individu_policy_ahliwaris where policy_id=? and (hubungan = '6' or flag_beasiswa = '1')";
		$query = $this->db->query($sql,array($policy_id));
		return $query->row_array();
	}

	function individu_policy_proses_by_keyword($keyword)
	{
		$sql = "
			SELECT
				 a.id as policy_id
				,a.spa_no
				,a.policy_no
				,d.product_code
				,d.product_name
				,c.id as cif_id
				,c.cif_no
				,c.nama
				,a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,e.rekening_bank
				,e.rekening_no
				,e.rekening_nama
				,e.rekening_cabang
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			LEFT JOIN life_product d ON a.product_code=d.product_code /*2017-05-08 left join karena belum ada setting di life_product*/
			LEFT JOIN life_individu_policy_detail e ON e.policy_id=a.id
			WHERE (upper(a.spa_no) like ? OR upper(c.nama) like ?) and a.policy_no is null
			GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14 /*digrup karna ada beberapa polis yang flag_cif 0 nya double*/
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function get_cif($keyword)
	{
		$sql = "
			SELECT
				a.policy_no
				,c.cif_no
				,c.nama
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			WHERE (upper(a.policy_no) like ? OR upper(c.nama) like ?) and a.policy_no is not null
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function get_jenis_rate_by_spa_no($spa_no)
	{
		$sql = "SELECT jenis_rate from life_grup_policy_nota_teknik where spa_no = ?";
		$query = $this->db->query($sql,array($spa_no));
		$row = $query->row_array();
		if (isset($row['jenis_rate']) && $row['jenis_rate']!='' && $row['jenis_rate']!=null) {
			return $row['jenis_rate'];
		} else {
			return 'B'; //rate bulanan
		}
	}
	
	function individu_policy_active_ul_by_keyword($keyword)
	{
		$sql = "
			SELECT
				 a.id as policy_id
				,a.spa_no
				,a.policy_no
				,d.product_code
				,d.product_name
				,c.id as cif_id
				,c.cif_no
				,c.nama
				,a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,e.rekening_bank
				,e.rekening_no
				,e.rekening_nama
				,e.rekening_cabang
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			LEFT JOIN life_product d ON a.product_code=d.product_code /*2017-05-08 left join karena belum ada setting di life_product*/
			LEFT JOIN life_individu_policy_detail e ON e.policy_id=a.id
			WHERE (upper(a.policy_no) like ? OR upper(c.nama) like ?) and a.policy_no is not null and d.flag_insurance = '3'
			GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14 /*digrup karna ada beberapa polis yang flag_cif 0 nya double*/
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function individu_policy_active_by_keyword_ul($keyword)
	{
		$sql = "SELECT
				 a.id as policy_id
				,a.spa_no
				,a.policy_no
				,d.product_code
				,d.product_name
				,c.id as cif_id
				,c.cif_no
				,c.nama
				,a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,e.rekening_bank
				,e.rekening_cabang
				,e.rekening_no
				,e.rekening_nama
				,coalesce(f.biaya_freelook,0) biaya_freelook
				,coalesce(f.biaya_surrender,0) biaya_surrender
				,a.tgl_akseptasi::date
				,e.penerimaan_date
				,a.manfaat
				,sum(g.kontribusi) as kontribusi_dasar
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			INNER JOIN life_product d ON a.product_code=d.product_code
			LEFT JOIN life_individu_policy_detail e ON e.policy_id=a.id
			INNER JOIN life_product_individu f ON f.product_code=a.product_code
			INNER JOIN life_individu_policy_kontribusi g ON g.policy_no = a.policy_no
			WHERE (upper(a.policy_no) like ? OR upper(c.nama) like ?) and a.policy_no is not null
			AND f.flag_unitlink=1
			AND a.status_polis in('1','9', '6', '11') /*adesagita 20180910 tambah status_polis=aktif dan reg claim. 20181015 tambah yg lapse juga karena masih ibsa klaim kecuali 12*/
			GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function informasi_polis_product_benefit($policy_id,$benefit_type) {
		$sql = "
			SELECT
				c.benefit_code,
				c.benefit_name,
				c.description as benefit_description,
				c.sequence,
				f.jenis_plan,
				coalesce(b.manfaat,0) manfaat,
				(coalesce(fn_get_premium_rate(d.product_code,b.benefit_code,a.usia_masuk,d.masa_kontrak,e.jenis_kelamin,e.kelas_pekerjaan),0)) rate_tabarru,
				coalesce(g.manfaat,0) AS manfaat_plan
			from life_individu_policy_cif a
			inner join life_individu_policy_cif_benefit b on a.id=b.policy_cif_id
			inner join life_benefit c on c.benefit_code=b.benefit_code
			inner join life_individu_policy d on d.id = a.policy_id
			inner join life_individu_cif e on e.cif_no = a.cif_no
			left join life_plan f on f.plan_code = b.plan_code
			LEFT JOIN life_plan_santunan g ON g.plan_code=f.plan_code AND g.product_code=d.product_code AND g.benefit_code=c.benefit_code AND g.jenis_santunan='1'
			where a.policy_id=? and b.benefit_type=? 
			group by 1,2,3,4,5,6,7,8
			order by 4 asc
		";
		$query = $this->db->query($sql,array($policy_id,$benefit_type));
		return $query->result_array();
	}

	function group_policy_haji_by_keyword($keyword)
	{
		$sql = "SELECT
					a.cif_no,
					a.policy_no,
					a.agen_code,
					a.spa_no,
					a.flag_administrasi_peserta,
					b.nama as lembaga,
					c.product_code,
					b.model_cabang,
					b.cif_no,
					c.product_name,
					(case when coalesce(d.manajemen_fee,0) > 0 then d.manajemen_fee else 
						(case when coalesce(a.manajemen_fee,0) > 0 then a.manajemen_fee else c.manajemen_fee end)
					end) as manajemen_fee,
					c.minimal_premi
				from life_grup_policy a
				left join life_grup_cif b on a.cif_no=b.cif_no
				left join life_product c on c.product_code=a.product_code
				left join life_grup_policy_nota_teknik d ON d.spa_no = a.spa_no
				where (UPPER(a.policy_no) like ? OR UPPER(b.nama) like ?) 
				and c.product_code in ('305TH','TIB1020','TIB0510','TIB0105')
				";
		
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		// if ($this->_branch_code!="00") {
		// 	$sql .= " and b.branch_code=?";
		// 	$param[] = $this->_branch_code;
		// }

        //cek session user akses
        $flag_akses_polis = $this->session->userdata('flag_akses_polis');
        $user_id = $this->session->userdata('id');
        if($flag_akses_polis!='0'){ // 0=super akses
            if($flag_akses_polis=='1'){ //jika akses polis
                $sql .= "AND a.policy_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='2'){ //jika akses branch
                $sql .= "AND b.branch_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='3'){ //jika akses cif
                $sql .= "AND b.cif_no IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }else if($flag_akses_polis=='4'){ //jika akses cif grup
                $sql .= "AND b.kelompok_code IN (SELECT z.policy_branch_cif FROM life_user_policy_akses z WHERE z.user_id=$user_id ) ";
            }
        }
        //end cek session user akses


		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function get_data_polis($policy_no) {
		$sql = "select * from life_grup_policy where policy_no=?";
		$query = $this->db->query($sql,array($policy_no));
		return $query->row_array();
	}

	function get_data_polis_benefit($spa_no) {
		$sql = "select * from life_grup_policy_benefit where spa_no=?";
		$query = $this->db->query($sql,array($spa_no));
		return $query->result_array();
	}

	function get_list_polis_pertanyaan($policy_id)
	{
		$param = array();
		$sql = "SELECT * FROM life_individu_policy_other_question where policy_id = ?";
		$param[] = $policy_id;
		$query = $this->db->query($sql,$param);
		$row = $query->result_array();
		$result = array();
		for ($i=0; $i < count($row); $i++) { 
			$detail = $this->get_list_polis_lain($row[$i]['id']);
			$result[] = array(
							'id' => $row[$i]['id'],
							'policy_id' => $row[$i]['policy_id'],
							'pertanyaan' => $row[$i]['pertanyaan'],
							'jawaban' => $row[$i]['jawaban'],
							'keterangan' => $row[$i]['keterangan'],
							'detail' => $detail
						);
		}
		return $result;
	}

	function get_list_polis_lain($other_question_id)
	{
		$param = array();
		$sql = "SELECT * FROM life_individu_policy_other_office where other_question_id = ?";
		$param[] = $other_question_id;
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function get_polis_kantor($other_question_id)
	{
		$param = array();
		$sql = "SELECT * FROM life_individu_policy_other_office where other_question_id = ?";
		$param[] = $other_question_id;
		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	function get_temp_peserta_gelondongan($id)
	{
		$param = array();
		$sql = "select a.*, b.product_code, b.spa_no from life_temp_peserta_gelondongan a, life_grup_policy b where a.policy_no = b.policy_no and a.id = ?";
		$param[] = $id;
		$query = $this->db->query($sql,$param);
		return $query->row_array();
	}

	function getBenefitbyProduct($product_code)
	{
		$sql = "select
				life_benefit.benefit_name,
				life_benefit.benefit_code,
				life_product_benefit.product_code,
				life_benefit.description
				from
				life_product_benefit
				inner join life_benefit on life_product_benefit.benefit_code = life_benefit.benefit_code
				where life_product_benefit.product_code = ?
			   ";
		$query=$this->db->query($sql,array($product_code));
		return $query->result_array();
	}

	function getPaymentDateGelondongan($policy_no,$participant_no)
	{
		$param = array();
		$sql = "select payment_date,payment_by,payment_status from life_grup_policy_participant where spa_no = (select spa_no from life_grup_policy where policy_no = ?) and participant_no = ?";
		$param[] = $policy_no;
		$param[] = $participant_no;
		$query = $this->db->query($sql,$param);
		return $query->row_array();
	}

	function cekExistParticipant($spa_no,$nama,$jenis_kelamin,$tanggal_lahir,$usia,$masa_kontrak,$manfaat,$kontribusi,$awal_kontrak,$akhir_kontrak)
	{
		$sql = "select count(*) jumlah from life_grup_policy_participant where spa_no = ? and nama = ? and tanggal_lahir = ? and usia = ? and contract_period_month = ? and total_benefit = ? and total_premi = ? and awal_kontrak = ? and akhir_kontrak = ?";
		$query = $this->db->query($sql,array($spa_no,$nama,$tanggal_lahir,$usia,$masa_kontrak,$manfaat,$kontribusi,$awal_kontrak,$akhir_kontrak));
		$row = $query->row_array();
		return $row['jumlah'];
	}

	function cekExistClaim($spa_no,$participant_no)
	{
		$sql = "select count(*) jumlah from life_grup_claim where spa_no = ? and participant_no = ?";
		$query = $this->db->query($sql,array($spa_no,$participant_no));
		$row = $query->row_array();
		return $row['jumlah'];
	}
	
	function get_polis_massdebet($keyword)
	{
		$sql = "
			SELECT
				 a.id as policy_id
				,a.spa_no
				,a.policy_no
				,d.product_code
				,d.product_name
				,c.id as cif_id
				,c.cif_no
				,c.nama
				,a.no_aplikasi
				,a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,a.tanggal_mass_debet
				,a.payment_channel
				,e.rekening_bank
				,e.rekening_no
				,e.rekening_nama
				,e.rekening_cabang
				,e.no_va
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			LEFT JOIN life_product d ON a.product_code=d.product_code /*2017-05-08 left join karena belum ada setting di life_product*/
			LEFT JOIN life_individu_policy_detail e ON e.policy_id=a.id
			WHERE (upper(a.policy_no) like ? OR upper(c.nama) like ?) and a.policy_no is not null
			GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	public function generate_balance($ammount,$sender_id,$received_id,$description)
	{
		$sender = "UPDATE user_account_balance SET saldo_balance = saldo_balance - ?, description = ? WHERE account_id = ?";
		$receiver = "UPDATE user_account_balance SET saldo_balance = saldo_balance + ?, description = ? WHERE account_id = ?";

		$this->db->trans_start(TRUE);
		$this->db->query($sender, array($ammount,('TRFTO'.$description),$sender_id));
		$this->db->query($receiver, array($ammount,('TRFFR'.$description),$received_id));
		$this->db->trans_complete();

		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			log_message('debug', 'DB Transaction Failure');
			$return = array('status'=>false);
		}else{
			$this->db->trans_commit();
			log_message('debug', 'DB Transaction success');
			$return = array('status'=>true);
		}

		echo json_encode($return);
	}
	
	function individu_policy_batal_by_keyword($keyword)
	{
		$sql = "SELECT
				 a.id as policy_id
				,a.spa_no
				,a.policy_no
				,d.product_code
				,d.product_name
				,c.id as cif_id
				,c.cif_no
				,c.nama
				,a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,e.rekening_bank
				,e.rekening_no
				,e.rekening_nama
				,e.rekening_cabang
				,coalesce(f.biaya_freelook,0) biaya_freelook
				,coalesce(f.biaya_surrender,0) biaya_surrender
				,a.tgl_akseptasi::date
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			INNER JOIN life_product d ON a.product_code=d.product_code /*2017-05-08 left join karena belum ada setting di life_product*/
			LEFT JOIN life_individu_policy_detail e ON e.policy_id=a.id
			LEFT JOIN life_product_individu f ON f.product_code=a.product_code
			WHERE (upper(a.policy_no) like ? OR upper(c.nama) like ?) and a.policy_no is null
			AND (a.verify_status = 3 or a.uw_status = 4)
			GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 /*digrup karna ada beberapa polis yang flag_cif 0 nya double*/
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}
	
	function get_polis_pemulihan($keyword)
	{
		$sql = "
			SELECT
				 a.id
				,a.policy_no
				,d.product_name
				,c.nama
				,b.id as policy_cif_id
				,(SELECT max(x.tgl_jto) FROM life_individu_policy_kontribusi x WHERE x.policy_no=a.policy_no) jto_last_payment
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON b.policy_id=a.id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON c.cif_no=b.cif_no
			INNER JOIN life_product d ON d.product_code=a.product_code
			WHERE a.status_polis = '11' AND (upper(a.policy_no) like ? OR upper(c.nama) like ?)
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}
	
	function get_rekening_giro($bank,$keyword,$from_date,$thru_date,$jumlah)
	{
		$sql = "SELECT
				a.id
				,a.tgl_trx
				,a.deskripsi
				,a.no_referensi
				,a.jumlah
				,(a.jumlah-coalesce(a.jumlah_respon,0)) as outstanding
				,coalesce(a.jumlah_respon,0) jumlah_respon
				,a.flag_penggabungan
				FROM life_finance_bank_trx_detail a, life_finance_bank_trx b
				where a.import_id=b.id
				and a.debet_credit='C'
				and b.kode_rekening=?
				and a.flag_premi=1
			";
			
		$param[] = $bank;

		if ($keyword!="") {
			$sql .= " and upper(a.deskripsi) like ?";
			$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		}

		if ($jumlah!="") {
			$sql .= " and a.jumlah::varchar like ?";
			$param[] = '%'.$jumlah.'%';
		}

		if ($from_date!="" && $thru_date!="") {
			$sql .= " AND a.tgL_trx::date BETWEEN ? AND ? ";
			$param[] = $from_date;
			$param[] = $thru_date;
		}

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}
	
	function individu_policy_active_by_keyword_ul_for_surr($keyword)
	{
		$sql = "SELECT
				 a.id as policy_id
				,a.spa_no
				,a.policy_no
				,d.product_code
				,d.product_name
				,c.id as cif_id
				,c.cif_no
				,c.nama
				,a.tgl_awal_kontrak
				,a.tgl_akhir_kontrak
				,e.rekening_bank
				,e.rekening_cabang
				,e.rekening_no
				,e.rekening_nama
				,coalesce(f.biaya_freelook,0) biaya_freelook
				,coalesce(f.biaya_surrender,0) biaya_surrender
				,a.tgl_akseptasi::date
				,e.penerimaan_date
				,a.manfaat
				,sum(g.kontribusi) as kontribusi_dasar
			FROM life_individu_policy a
			INNER JOIN life_individu_policy_cif b ON a.id=b.policy_id AND b.flag_cif=0
			INNER JOIN life_individu_cif c ON b.cif_no=c.cif_no
			INNER JOIN life_product d ON a.product_code=d.product_code
			LEFT JOIN life_individu_policy_detail e ON e.policy_id=a.id
			INNER JOIN life_product_individu f ON f.product_code=a.product_code
			INNER JOIN life_individu_policy_kontribusi g ON g.policy_no = a.policy_no
			INNER JOIN life_individu_policy_regis_claim h ON h.policy_no=a.policy_no /*adesagita 20181128 hanya yg sudah regis yg bisa surr dan freelook agar nilai unit tidak berubah*/
			WHERE (upper(a.policy_no) like ? OR upper(c.nama) like ?) and a.policy_no is not null
			AND f.flag_unitlink=1
			AND a.status_polis in('1','9', '6', '11') /*adesagita 20180910 tambah status_polis=aktif dan reg claim. 20181015 tambah yg lapse juga karena masih ibsa klaim kecuali 12*/
			GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
		";

		$param[] = '%'.strtoupper(strtolower($keyword)).'%';
		$param[] = '%'.strtoupper(strtolower($keyword)).'%';

		$query = $this->db->query($sql,$param);
		return $query->result_array();
	}

	/*[BEGIN] PESERTA NON MEDIS SAVING
	| @MLH 2018 DESEMBER 28
	*/
	function getDataParticipantSavingById($id)
	{
		$sql = "SELECT
					a.id,
					a.nama,
					a.tanggal_lahir,
					a.awal_kontrak,
					a.contract_period_month,
					a.contract_period_day,
					a.akhir_kontrak,
					a.participant_no,
					a.uw_type,
					c.product_name,
					a.spa_no
				from life_grup_policy_participant_saving a
				left join life_grup_policy_saving b on a.spa_no=b.spa_no
				left join life_product c on c.product_code=b.product_code
				where a.id = ?";
		$query = $this->db->query($sql,array($id));
		return $query->row_array();
	}

	function getDataBenefitParticipantSavingByNo($participant_id)
	{
		$sql = "SELECT
					b.benefit_name
					,b.description
					,a.benefit_value
					,a.kontribusi
				from life_grup_policy_participant_saving_benefit a
				left join life_benefit b on a.benefit_code=b.benefit_code
				where a.id_participant = ?";
		$query = $this->db->query($sql,array($participant_id));
		return $query->result_array();
	}
	/*[END] PESERTA NON MEDIS */
}


/*[PERHATIAN]
Source Code ini milik PT ama salam Indonesia.
Dilarang menggunakan sebagian atau seluruhnya tanpa izin tertulis dari PT ama salam Indonesia 
*/
