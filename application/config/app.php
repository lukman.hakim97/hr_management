<?php if(!defined('BASEPATH')) exit("No direct script access allowed");

    // Configuration APP Name
    $config['app_title'] = "HR MANAGEMENT SYSTEM";
    $config['app_name'] = "HR MANAGEMENT SYSTEM";
    $config['app_name_short'] = "HRM";
    $config['app_description'] = "";
    $config['app_logo'] = "HRM";
    $config['app_logo_print'] = "HRM";
    $config['app_company_name'] = "GROUP 5 - 2020";

    $config['header_css'] = array();
    $config['footer_js'] = array();  
?>