<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct(true);
		// $this->load->model('model_administration');
	}

	public function index()
	{
		$this->template->set('title', 'Home');
		$this->template->set('nav', 'Home');
		$this->template->set('nav_list', array('Home', 'Dashboard'));
		$this->template->load_main('home');
	}

	public function change_password() {
		$this->template->set('title', 'Change Password');
		$this->template->set('nav', 'Change Password');
		$this->template->set('nav_list', array('Home', 'Profile', 'Change Password'));
		$this->template->load_main('profile/change_password.php');
	}

	public function save_change_password() {
		$agen_code = $this->session->userdata('agen_code');
		$token = $this->session->userdata('token');
		$old_password = $this->input->post('old_password');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		$bValid = true;
		$message = '';

		if($confirm_password!=$password){
			$bValid = false;
			$message = 'Konfirmasi Password Tidak Sama Dengan Password Baru !';
		}

		if($password=='' || $confirm_password==''){
			$bValid = false;
			$message = 'Password Baru & Konfirmasi Password Baru Belum Di Isi !';
		}

		if($bValid == true){
			$url = API_URL.'changeUserPassword';
			$fields['referal_number'] = $agen_code;
			$fields['old_password'] = mysql_escape_string($old_password);
			$fields['password'] = mysql_escape_string($password);
			$ret = api_post_data_add($url,$fields,$token);
			$data = json_encode($ret['data'],true);
			$res = (array)$ret['data'];
			if($ret['message']===200){
				$return = array('success'=>true,'message'=>$res['message']);
			}else{
				$return = array('success'=>false,'message'=>'Error !');
			}
		}else{
			$return = array('success'=>false,'message'=>$message);
		}

		echo json_encode($return);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login','refresh');
	}

	public function dashboard()
	{
		$this->template->set('title', 'Home');
		$this->template->set('nav', 'Dashboard');
		$this->template->set('nav_list', array('Home', 'Dashboard'));
		$this->template->load_main('dashboard');
	}
}

