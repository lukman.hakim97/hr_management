<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends MY_Controller {

    public function __construct()
	{
        parent::__construct(true);
        $this->load->model('model_system');
    }
    
    public function index()
	{
		$data['title'] = 'Issuance';
    }

    /**
     * [BEGIN] MENU SETUP 
     * | @MLH 20191206
    */
    public function menu() {
        $this->template->set('title', 'Menu');
        $this->template->set('nav', 'Menu');
        $this->template->set('nav_list', array('Home', 'System', 'Menu'));
        $this->template->load_main('system/menu');
    }

    public function jqgrid_menu()
	{
		ini_set('memory_limit','-1');
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
		$limit_rows = isset($_REQUEST['rows'])?$_REQUEST['rows']:15;
		$sidx = isset($_REQUEST['sidx'])?$_REQUEST['sidx']:'id';
		$sord = isset($_REQUEST['sord'])?$_REQUEST['sord']:'ASC';

		$totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
		if ($totalrows) { $limit_rows = $totalrows; }

		$result = $this->model_system->jqgrid_menu('','','','');

		$count = count($result);
		if ($count > 0) { $total_pages = ceil($count / $limit_rows); } else { $total_pages = 0; }

		if ($page > $total_pages)
		$page = $total_pages;
		$start = $limit_rows * $page - $limit_rows;
		if ($start < 0) $start = 0;

        $result = $this->model_system->jqgrid_menu($sidx,$sord,$limit_rows,$start);

		$responce['page'] = $page;
		$responce['total'] = $total_pages;
		$responce['records'] = $count;

		$i = 0;
		foreach ($result as $row)
		{
			$responce['rows'][$i]['id'] = $row['id'];
			$responce['rows'][$i]['cell'] = array(
                $row['id']
                ,$row['id']
                ,$row['parent']
                ,$row['title']
                ,$row['url']
                ,$row['type']
                ,$row['icon']
			);
			$i++;
		}

		echo json_encode($responce);
	}
    
    function save_menu(){
        $response = [];
        $response['status'] = true;
		$response['message'] = 'Successfully Saved !';
		echo json_encode($response);
    }

    function update_menu(){
        $response = [];
        $response['status'] = true;
		$response['message'] = 'Successfully Updated !';
		echo json_encode($response);
    }
	/**
     * [END] MENU SETUP
    */

    /**
     * [BEGIN] USER USER 
     * | @MLH 20191206
    */
    public function user() {
        $this->template->set('title', 'User');
        $this->template->set('nav', 'User');
        $this->template->set('nav_list', array('Home', 'System', 'User'));
        $this->template->load_main('system/user');
    }

    public function jqgrid_user()
	{
		ini_set('memory_limit','-1');
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
		$limit_rows = isset($_REQUEST['rows'])?$_REQUEST['rows']:15;
		$sidx = isset($_REQUEST['sidx'])?$_REQUEST['sidx']:'id';
		$sord = isset($_REQUEST['sord'])?$_REQUEST['sord']:'ASC';

		$totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
		if ($totalrows) { $limit_rows = $totalrows; }

		$result = $this->model_system->jqgrid_user('','','','');

		$count = count($result);
		if ($count > 0) { $total_pages = ceil($count / $limit_rows); } else { $total_pages = 0; }

		if ($page > $total_pages)
		$page = $total_pages;
		$start = $limit_rows * $page - $limit_rows;
		if ($start < 0) $start = 0;

        $result = $this->model_system->jqgrid_user($sidx,$sord,$limit_rows,$start);

		$responce['page'] = $page;
		$responce['total'] = $total_pages;
		$responce['records'] = $count;

		$i = 0;
		foreach ($result as $row)
		{
			$responce['rows'][$i]['id'] = $row['id'];
			$responce['rows'][$i]['cell'] = array(
                $row['id']
                ,$row['id']
                ,$row['username']
                ,$row['fullname']
                ,$row['role']
                ,$row['status']
                ,$row['register_date']
			);
			$i++;
		}

		echo json_encode($responce);
	}
    
    function save_user(){
        $response = [];
        $response['status'] = true;
		$response['message'] = 'Successfully Saved !';
		echo json_encode($response);
    }

    function update_user(){
        $response = [];
        $response['status'] = true;
		$response['message'] = 'Successfully Updated !';
		echo json_encode($response);
    }
	/**
     * [END] USER SETUP
    */

    /**
     * [BEGIN] PRIVILAGE
     * | @MLH 20191206
    */
    public function privilage() {
        $this->template->set('title', 'Privilege');
        $this->template->set('nav', 'Privilege');
        $this->template->set('nav_list', array('Home', 'System', 'Privilege'));
        $this->template->load_main('system/privilage');
    }

    public function jqgrid_privilege()
	{
		ini_set('memory_limit','-1');
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
		$limit_rows = isset($_REQUEST['rows'])?$_REQUEST['rows']:15;
		$sidx = isset($_REQUEST['sidx'])?$_REQUEST['sidx']:'id';
		$sord = isset($_REQUEST['sord'])?$_REQUEST['sord']:'ASC';

		$totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
		if ($totalrows) { $limit_rows = $totalrows; }

		$result = $this->model_system->jqgrid_privilege('','','','');

		$count = count($result);
		if ($count > 0) { $total_pages = ceil($count / $limit_rows); } else { $total_pages = 0; }

		if ($page > $total_pages)
		$page = $total_pages;
		$start = $limit_rows * $page - $limit_rows;
		if ($start < 0) $start = 0;

        $result = $this->model_system->jqgrid_privilege($sidx,$sord,$limit_rows,$start);

		$responce['page'] = $page;
		$responce['total'] = $total_pages;
		$responce['records'] = $count;

		$i = 0;
		foreach ($result as $row)
		{
			$responce['rows'][$i]['id'] = $row['id'];
			$responce['rows'][$i]['cell'] = array(
                $row['id']
                ,$row['id']
                ,$row['name']
                ,$row['description']
			);
			$i++;
		}

		echo json_encode($responce);
	}
    
    function save_privilage(){
        $response = [];
        $response['status'] = true;
		$response['message'] = 'Successfully Saved !';
		echo json_encode($response);
    }

    function update_privilage(){
        $response = [];
        $response['status'] = true;
		$response['message'] = 'Successfully Updated !';
		echo json_encode($response);
    }
	/**
     * [END] PRIVILAGE SETUP
    */

     /**
     * [BEGIN] LIST CODE
     * | @MLH 20191206
    */
    public function list_code() {
        $this->template->set('title', 'List Code');
        $this->template->set('nav', 'List Code');
        $this->template->set('nav_list', array('Home', 'System', 'List Code'));
        $this->template->load_main('system/list_code');
    }
    
    function save_list_code(){
        $response = [];
        $response['status'] = true;
		$response['message'] = 'Successfully Saved !';
		echo json_encode($response);
    }

    function update_list_code(){
        $response = [];
        $response['status'] = true;
		$response['message'] = 'Successfully Updated !';
		echo json_encode($response);
    }
	/**
     * [END] list_code SETUP
    */

     /**
     * [BEGIN] BRANCH
     * | @MLH 20191206
    */
    public function branch() {
        $this->template->set('title', 'Setup Branch');
        $this->template->set('nav', 'Setup Branch');
        $this->template->set('nav_list', array('Home', 'Setup Parameter', 'Setup Branch'));
        $this->template->load_main('system/branch');
    }
    
    public function jqgrid_branch()
	{
		ini_set('memory_limit','-1');
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
		$limit_rows = isset($_REQUEST['rows'])?$_REQUEST['rows']:15;
		$sidx = isset($_REQUEST['sidx'])?$_REQUEST['sidx']:'id';
		$sord = isset($_REQUEST['sord'])?$_REQUEST['sord']:'ASC';

		$totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
		if ($totalrows) { $limit_rows = $totalrows; }

		$result = $this->model_system->jqgrid_branch('','','','');

		$count = count($result);
		if ($count > 0) { $total_pages = ceil($count / $limit_rows); } else { $total_pages = 0; }

		if ($page > $total_pages)
		$page = $total_pages;
		$start = $limit_rows * $page - $limit_rows;
		if ($start < 0) $start = 0;

        $result = $this->model_system->jqgrid_branch($sidx,$sord,$limit_rows,$start);

		$responce['page'] = $page;
		$responce['total'] = $total_pages;
		$responce['records'] = $count;

		$i = 0;
		foreach ($result as $row)
		{
			$responce['rows'][$i]['id'] = $row['id'];
			$responce['rows'][$i]['cell'] = array(
                $row['id']
                ,$row['branch_code']
                ,$row['branch_name']
			);
			$i++;
		}

		echo json_encode($responce);
	}
	/**
     * [END] PRODUCT BRANCH
    */

     /**
     * [BEGIN] LEVEL OTORISASI
     * | @MLH 20191206
    */
    public function level_otorisasi() {
        $this->template->set('title', 'Setup Level Authorization');
        $this->template->set('nav', 'Setup Level Authorization');
        $this->template->set('nav_list', array('Home', 'Setup Parameter', 'Setup Level Authorization'));
        $this->template->load_main('system/level_otorisasi');
    }
    
    public function jqgrid_level_otorisasi()
	{
		ini_set('memory_limit','-1');
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
		$limit_rows = isset($_REQUEST['rows'])?$_REQUEST['rows']:15;
		$sidx = isset($_REQUEST['sidx'])?$_REQUEST['sidx']:'id';
		$sord = isset($_REQUEST['sord'])?$_REQUEST['sord']:'ASC';

		$totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
		if ($totalrows) { $limit_rows = $totalrows; }

		$result = $this->model_system->jqgrid_level_otorisasi('','','','');

		$count = count($result);
		if ($count > 0) { $total_pages = ceil($count / $limit_rows); } else { $total_pages = 0; }

		if ($page > $total_pages)
		$page = $total_pages;
		$start = $limit_rows * $page - $limit_rows;
		if ($start < 0) $start = 0;

        $result = $this->model_system->jqgrid_level_otorisasi($sidx,$sord,$limit_rows,$start);

		$responce['page'] = $page;
		$responce['total'] = $total_pages;
		$responce['records'] = $count;

		$i = 0;
		foreach ($result as $row)
		{
			$responce['rows'][$i]['id'] = $row['id'];
			$responce['rows'][$i]['cell'] = array(
                $row['id']
                ,$row['code']
                ,$row['level']
                ,$row['min_nominal']
                ,$row['max_nominal']
			);
			$i++;
		}

		echo json_encode($responce);
	}
	/**
     * [END] LEVEL OTORISASI
    */

    /**
     * [BEGIN] LEVEL OTORISASI
     * | @MLH 20191206
    */
    public function user_level_otorisasi() {
        $this->template->set('title', 'Setup User Level Authorization');
        $this->template->set('nav', 'Setup User Level Authorization');
        $this->template->set('nav_list', array('Home', 'Setup Parameter', 'Setup User Level Authorization'));
        $this->template->load_main('system/user_level_otorisasi');
    }
    
    public function jqgrid_user_level_otorisasi()
	{
		ini_set('memory_limit','-1');
		$page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
		$limit_rows = isset($_REQUEST['rows'])?$_REQUEST['rows']:15;
		$sidx = isset($_REQUEST['sidx'])?$_REQUEST['sidx']:'id';
		$sord = isset($_REQUEST['sord'])?$_REQUEST['sord']:'ASC';

		$totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
		if ($totalrows) { $limit_rows = $totalrows; }

		$result = $this->model_system->jqgrid_user_level_otorisasi('','','','');

		$count = count($result);
		if ($count > 0) { $total_pages = ceil($count / $limit_rows); } else { $total_pages = 0; }

		if ($page > $total_pages)
		$page = $total_pages;
		$start = $limit_rows * $page - $limit_rows;
		if ($start < 0) $start = 0;

        $result = $this->model_system->jqgrid_user_level_otorisasi($sidx,$sord,$limit_rows,$start);

		$responce['page'] = $page;
		$responce['total'] = $total_pages;
		$responce['records'] = $count;

		$i = 0;
		foreach ($result as $row)
		{
			$responce['rows'][$i]['id'] = $row['id'];
			$responce['rows'][$i]['cell'] = array(
                $row['id']
                ,$row['username']
                ,$row['fullname']
                ,$row['code']
                ,$row['level']
                ,$row['min_nominal']
                ,$row['max_nominal']
			);
			$i++;
		}

		echo json_encode($responce);
	}
	/**
     * [END] LEVEL OTORISASI
    */

    public function company() {
        $this->template->set('title', 'Company');
        $this->template->set('nav', 'Company');
        $this->template->set('nav_list', array('Home', 'Company'));
        $this->template->load_main('common/company/main.php');
    }
    
    public function outlet() {
        $this->template->set('title', 'Branch');
        $this->template->set('nav', 'Branch');
        $this->template->set('nav_list', array('Home', 'Branch'));
        $this->template->load_main('common/branch/main.php');
    }
    
    public function departement() {
        $this->template->set('title', 'Departement');
        $this->template->set('nav', 'Departement');
        $this->template->set('nav_list', array('Home', 'Departement'));
        $this->template->load_main('common/departement/main.php');
    }
    
    public function position() {
        $this->template->set('title', 'Position');
        $this->template->set('nav', 'Position');
        $this->template->set('nav_list', array('Home', 'Position'));
        $this->template->load_main('common/position/main.php');
	}
}