<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup extends MY_Controller {

	public function __construct()
	{
		parent::__construct(true);
	}

	public function index()
	{
		show_404();
	}

	public function setup_reas_treaty() {
		$this->template->set('title', 'OR/Remuneration');
		$this->template->set('nav', 'OR/Remuneration');
        $this->template->set('nav_list', array('Home', 'Setup', 'OR/Remuneration'));
		$this->template->load_main('setup/setup_reas_treaty');
    }
    
    public function jqgrid_setup_reas_treaty() {
		$response["rows"] = [
            [
                'id' => 1,
                'no_referensi' => 101,
                'reasuradur' => 'AMA Salam Indonesia',
                'deskripsi' => '-',
                'produk' => 'SALAM Prima Syariah'
            ],
            [
                'id' => 2,
                'no_referensi' => 102,
                'reasuradur' => 'Salam Indonesia',
                'deskripsi' => '-',
                'produk' => 'SALAM Prima Syariah'
            ],
            [
                'id' => 3,
                'no_referensi' => 104,
                'reasuradur' => 'AMA Salam',
                'deskripsi' => '-',
                'produk' => 'SALAM Prima Syariah'
            ]
        ];
		echo json_encode($response);
    }

    public function setup_reasuradur() {
		$this->template->set('title', 'Reinsurance');
		$this->template->set('nav', 'Reinsurance');
        $this->template->set('nav_list', array('Home', 'Setup', 'Reinsurance'));
		$this->template->load_main('setup/setup_reasuradur');
    }
    
    public function jqgrid_setup_reasuradur() {
		$response["rows"] = [
            [
                'id' => 1,
                'company_code' => 'ASI',
                'company_name' => 'AMA Salam Indonesia',
                'address' => 'Jakarta',
                'postal_code' => 'SALAM Prima Syariah',
                'phone' => '+62 813 1212 12',
                'website' => 'salamenterprise.com',
                'email' => 'SALAM Prima Syariah',
            ],
            [
                'id' => 2,
                'company_code' => 'SI',
                'company_name' => 'Salam Indonesia',
                'address' => '-',
                'postal_code' => 'SALAM Prima Syariah',
                'phone' => '+62 813 1212 12',
                'website' => '',
                'email' => 'admin@salamindonesia.com',
            ],
            [
                'id' => 3,
                'company_code' => 'AS',
                'company_name' => 'AMA Salam',
                'address' => '-',
                'postal_code' => 'admin@amasalam.com',
                'phone' => '+62 813 1212 12',
                'website' => '',
                'email' => 'admin@amasalam.com',
            ]
        ];
		echo json_encode($response);
    }

    public function setup_koasuradur() {
		$this->template->set('title', 'Coinsurance');
		$this->template->set('nav', 'Coinsurance');
        $this->template->set('nav_list', array('Home', 'Setup', 'Coinsurance'));
		$this->template->load_main('setup/setup_koasuradur');
    }
    
    public function jqgrid_setup_koasuradur() {
		$response["rows"] = [
            [
                'id' => 1,
                'company_code' => 'ASI',
                'company_name' => 'AMA Salam Indonesia',
                'address' => 'Jakarta',
                'postal_code' => 'SALAM Prima Syariah',
                'phone' => '+62 813 1212 12',
                'website' => 'salamenterprise.com',
                'email' => 'SALAM Prima Syariah',
            ],
            [
                'id' => 2,
                'company_code' => 'SI',
                'company_name' => 'Salam Indonesia',
                'address' => '-',
                'postal_code' => 'SALAM Prima Syariah',
                'phone' => '+62 813 1212 12',
                'website' => '',
                'email' => 'admin@salamindonesia.com',
            ],
            [
                'id' => 3,
                'company_code' => 'AS',
                'company_name' => 'AMA Salam',
                'address' => '-',
                'postal_code' => 'admin@amasalam.com',
                'phone' => '+62 813 1212 12',
                'website' => '',
                'email' => 'admin@amasalam.com',
            ]
        ];
		echo json_encode($response);
    }

    public function setup_rate_kontribusi_coas() {
		$this->template->set('title', 'Contribution Coas');
		$this->template->set('nav', 'Contribution Coas');
        $this->template->set('nav_list', array('Home', 'Setup', 'Contribution Coas'));
		$this->template->load_main('setup/setup_rate_kontribusi_coas');
    }
    
    public function jqgrid_setup_rate_kontribusi_coas() {
		$response["rows"] = [
            [
                'id' => 1,
                'rate_code' => '038AD2',
                'rate_name' => 'SALAM - BRI S MIKRO Syariah',
                'rate_value' => 0.3000,
                'contract' => '1 - 1',
                'age' => '17 - 64 years'
            ],
            [
                'id' => 2,
                'rate_code' => '038AD2',
                'rate_name' => 'SALAM - BRI S MIKRO Syariah',
                'rate_value' => 0.3000,
                'contract' => '2 - 2',
                'age' => '17 - 64 years'
            ],
            [
                'id' => 3,
                'rate_code' => '038AD2',
                'rate_name' => 'SALAM - BRI S MIKRO Syariah',
                'rate_value' => 0.3000,
                'contract' => '3 - 3',
                'age' => '17 - 64 years'
            ]
        ];
		echo json_encode($response);
    }

    public function setup_occupation() {
        $this->template->set('title', 'Occupation');
        $this->template->set('nav', 'Occupation');
        $this->template->set('nav_list', array('Home', 'Setup', 'Occupation'));
        $this->template->load_main('setup/setup_occupation');
    }
    
    public function jqgrid_setup_occupation() {
        $response["rows"] = [
            [
                'id' => 1,
                'occupation_code' => '038AD2',
                'occupation_name' => 'Example Occupation',
                'description' => '-',
                'class_name' => 'Motor',
                'class_code' => '001',
            ],
            [
                'id' => 2,
                'occupation_code' => '038AD2',
                'occupation_name' => 'Example Occupation 2',
                'description' => '-',
                'class_name' => 'Fire',
                'class_code' => '002',
            ],
            [
                'id' => 3,
                'occupation_code' => '038AD2',
                'occupation_name' => 'Example Occupation 3',
                'description' => '-',
                'class_name' => 'Marine Cargo',
                'class_code' => '003',
            ]
        ];
        echo json_encode($response);
    }

    public function setup_vehicle_type() {
        $this->template->set('title', 'Vehicle Type');
        $this->template->set('nav', 'Vehicle Type');
        $this->template->set('nav_list', array('Home', 'Setup', 'Vehicle Type'));
        $this->template->load_main('setup/setup_vehicle_type');
    }
    
    public function jqgrid_setup_vehicle_type() {
        $response["rows"] = [
            [
                'id' => 1,
                'type_code' => '038AD2',
                'type_name' => 'Pickup',
                'description' => '-',
            ],
            [
                'id' => 2,
                'type_code' => '038AD2',
                'type_name' => 'Bus',
                'description' => '-',
            ],
            [
                'id' => 3,
                'type_code' => '038AD2',
                'type_name' => 'Van',
                'description' => '-',
            ]
        ];
        echo json_encode($response);
    }

}