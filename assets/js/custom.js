/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";

var APP = function() {

    return {
        init: function() {

        },
        ConvertNumeric: function(number) {
            number = number.replace(/\./g, '');
            return number;
        },
        NumberFormat: function(number, decimals, dec_point, thousands_sep) {
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            // Add this number to the element as text.
            return s.join(dec);
        },
        swalWarning: function(content = '') {
            swal("Warning", content, 'warning');
        },
        swalSuccess: function(content = 'Successfully saved!') {
            swal("Success", content, 'success');
        },
        swalError: function(content = 'An error occurred, please try again later!') {
            swal("Error", content, 'error');
        },
        swalInfo: function(content = 'Please try again!') {
            swal("Info", content, 'info');
        },
        swalOption: function(content = '') {
            swal({
                    title: "Are you sure?",
                    text: content,
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Success !", {
                            icon: "success",
                        });
                    } else {
                        swal("Process Cancel !");
                    }
                });
        },
        scrollTo: function(el, offeset) {
            var pos = (el && el.size() > 0) ? el.offset().top : 0;

            if (el) {
                if ($('body').hasClass('page-header-fixed')) {
                    pos = pos - $('.page-header').height();
                } else if ($('body').hasClass('page-header-top-fixed')) {
                    pos = pos - $('.page-header-top').height();
                } else if ($('body').hasClass('page-header-menu-fixed')) {
                    pos = pos - $('.page-header-menu').height();
                }
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            $('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },
        ToDatePicker: function(date) {
            if (date == "" || date == null) {
                return "-";
            }
            var Sdate = date.split('-');
            var date = Sdate[2] + '/' + Sdate[1] + '/' + Sdate[0];
            return date;
        },
        ToDateDefault: function(date) {
            var Sdate = date.replace(/\//g, '');
            var date = Sdate.substring(4, 8) + '-' + Sdate.substring(2, 4) + '-' + Sdate.substring(0, 2);
            return date;
        },
        write_error_log: function(url, status, statusText, responseText) {
            var status = status;
            var statusText = statusText;
            var responseText = responseText;
            var url = window.location.href;
            // $.ajax({
            //     type:"POST",
            //     data:{
            //         url:url
            //         ,status:status
            //         ,statusText:statusText
            //         ,responseText:responseText
            //     },
            //     url:site_url+'error_log/write'
            // });
        }
    }

}();